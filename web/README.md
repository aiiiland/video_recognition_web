# Video Recognition Frontend

created by Jason Leung

This directory is a self contained React source code.

The way this is integrated with the Python Flask app is that after the webpack react build, it copy the generated css and js file to the static directory in the Flask project.

## Developement
```
$ npm i
$ npm start
```
Open http://localhost:3000, code and refresh (auto refresh)


## Prepare to Release
the steps to release is written in 'gruntfile.js', use Grunt to execute

Install Grunt command line tool:
```
$ npm i -g grunt-cli
```

Then just run `grunt` in this directory
```
$ grunt
```
You can use Git to see what files has changed