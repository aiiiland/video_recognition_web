import React from 'react'

const defaultState = {
    isLoggedIn: false,
    user: null,
    onLogout: null
}

const AuthContext = React.createContext(defaultState);

export default AuthContext