import React, { Component } from 'react';

import { Router, Route, Link, Switch, Redirect } from "react-router-dom";
import history, { BackButton } from '~/lib/history'
import { makeApiMethod } from '~/lib/api'

import DefaultLayout from './layouts/DefaultLayout'
import Loading from '~/components/Loading'

import LoginPage from './pages/LoginPage'
import AdminSection from './pages/AdminSection'

import AuthContext from './AuthContext'

import './App.css';

const sessionApi = makeApiMethod({
  path: '/session',
  method: 'POST'
})
const logoutApi = makeApiMethod({
  path: '/logout',
  method: 'POST'
})



function NoRoute(){
  return (
    <p>Select the page to begin</p>
  )
}

class App extends Component 
{
  constructor(props){
    super(props)

    this.state = {
      loginStateReady: false,
      isLoggedIn: false,
      user: null
    }

    this.logout = this.logout.bind(this)
    this.onLogin = this.onLogin.bind(this)
  }

  componentDidMount(){
    this.checkLogin()
  }

  async checkLogin(){

    try{

      let response = await sessionApi({})
      this.setState({
          loginStateReady: true,
          isLoggedIn: true,
          user: response.username
      })

    }catch(e){

      this.setState({
        loginStateReady: true,
        isLoggedIn: false,
        user: null
    })

    }
  }

  onLogin(){
    // just check again
    this.checkLogin()
  }

  async logout(){
    let response = await logoutApi({})

    this.setState({
      isLoggedIn: false,
      user: null
    })
  }

  render() {

    return (
      <Router history={history}>
      <AuthContext.Provider value={{ user: this.state.user, isLoggedIn: this.state.isLoggedIn, onLogout: this.logout }}>
      <div className="App">
        <DefaultLayout  isLoggedIn={this.state.isLoggedIn}>
          {
            this.state.loginStateReady && 
            <Switch>
              <Redirect exact from="/" to="/login"/>
              <Route path="/" exact component={NoRoute} />
              <Route path="/login" exact render={(props) => {
                return <LoginPage {...props} onLogin={this.onLogin} />
              }} />
              <Route path="/admin" render={(props) => {
                return <AdminSection {...props} isLoggedIn={this.state.isLoggedIn} user={this.state.user} />
              }} />
            </Switch>
          }
          {
            !this.state.loginStateReady && 
            <Loading />
          }
        </DefaultLayout>
      </div>
      </AuthContext.Provider>
      </Router>
    );
  }
}

export default App;
