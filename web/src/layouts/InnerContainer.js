import React from 'react'

export default function InnerContainer({children, flex}){
    let mStyle = { padding:'0 15px 30px 15px' };
    if(flex){
        mStyle = {
            ...mStyle,
            display:'flex', flexDirection:'column', flex:1
        }
    }
    return (
        <div style={mStyle}>
        {children}
        </div>
    )
}