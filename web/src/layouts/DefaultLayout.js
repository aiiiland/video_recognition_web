import React, { Component } from 'react';
// import { Link } from 'react-router-dom'
import DocumentTitle from 'react-document-title';

// import 'bootstrap/dist/css/bootstrap.css'
import '../theme/css/dark.bootstrap.min.css'
// import 'bootstrap/dist/css/bootstrap-theme.css'
import './global.css'
// import './jebsen-theme.css'
// import './print.css'

// import logo from '../images/jebsen_logo.png'
// import logo from '../images/jebsen_logo.svg'

import Header from './Header'
// import SidebarMenu from './SidebarMenu'



// import { api as AuthAPI } from '../modules/auth'
// import { Loading } from '../components'
// import * as StorageUtils from '../utils/storage'
// import browserHistory from '../history'
// import ModuleManager from '../modules'

// import { loadPreferencesFromServer } from '~/utils/gridPreference'

// import { routes } from '../config/routes'


const APP_ENV = process.env.APP_ENV;

function Wrapper({children}){
    return (
        <div className="sideLayout">{children}</div>
    )
}


function Content({children}){
    return (
        <div style={{display:'flex', flex:1, flexDirection:'column'}} className="sideLayout-main">{children}</div>
    )
}


class DefaultLayout extends Component{

    state = { isOpen: true }

    constructor(props){
        super(props)
        this.state = {
            path: "",
            title: "",
            pageTitle: "",

            isShowSidebar: true
        }

        this.onMenuButtonClicked = this.onMenuButtonClicked.bind(this)
    }

    componentDidMount(){
        this.updateTitle(this.props);
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps.location !== this.props.location){
            this.updateTitle(nextProps)
        }
    }

    updateTitle(nextProps){
        const { location } = nextProps;
        // const route = routes.find(location.pathname);
        const route = null
    
        const displayTitle = (route && route.title)? 
                route.title + " - " + "Aiiiland"
                : "Aiiiland";
    
        this.setState({
        //   path: location.pathname,
          title: displayTitle,
          pageTitle: (route && route.title)? route.title :  ""
        })
    }

    onMenuButtonClicked(){
        // this.setState({ isShowSidebar: !this.state.isShowSidebar })
    }

    render(){

        return (
            <DocumentTitle title={this.state.title}>
            <div>
                {
                    this.props.isLoggedIn &&
                    <Header 
                        pageTitle={this.state.pageTitle}
                        onMenuButtonClicked={this.onMenuButtonClicked}
                    />
                }
                <div className="container-fluid" style={{paddingTop:60}}>
                {this.props.children}

                <footer>
                    <div className="text-center">
                    <small className="text-muted">Copyright © 2018 Aiiiland Co Ltd. All rights reserved.</small>
                    </div>
                </footer>
                </div>
            </div>
            </DocumentTitle>
        )
    }

}

export default DefaultLayout