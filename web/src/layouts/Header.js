import React, { Component } from 'react'
import history from '../lib/history'

import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Button } from 'react-bootstrap'
// import './Header.css'

// import ReactModal from 'react-modal';

import MenuIcon from "react-icons/lib/md/menu"
import UserIcon from "react-icons/lib/fa/user"
import CameraIcon from "react-icons/lib/fa/video-camera"
import DashboardIcon from "react-icons/lib/fa/dashboard"
import LogoutIcon from "react-icons/lib/fa/sign-out"
import SettingsIcon from "react-icons/lib/fa/cogs"
import EyeIcon from "react-icons/lib/fa/eye"

import AuthContext from '~/AuthContext'

const DEBUG = process.env.APP_ENV == 'development' || process.env.APP_ENV == 'dev-ci'


const Navs = {
  home: () => history.push('/'),
  login: () => history.push('/login'),
  dashboard: () => history.push('/admin/dashboard'),
  peopleAdmin: () => history.push('/admin/people-admin'),
  cameraAdmin: () => history.push('/admin/camera-admin'),
  settings: () => history.push('/admin/settings'),
  surveillanceLog: () => history.push('/admin/logs'),
}


function onLoginClicked(){
  history.push('/login')
}

function onLogoutClicked(){
  let result = window.confirm("Are you sure you want to logout?")
  if(result){
    // history.push('/logout')
    if(this.props.onLogout){
      this.props.onLogout()
    }
  }
}


function UserLabel({user}){
  return (
    <span><UserIcon /> {user.profile.full_name} ({user.username})</span>
  )
}



const custom3DHeader = {
  boxShadow:'0 0 10px rgba(0,0,0,1)',
  background: 'linear-gradient(#375a7f, hsla(211, 40%, 20%, 1))'
}

/**
 * Header component
 */
export default class Header extends Component
{
  constructor(props){
    super(props)

    this.state = {
      isShowModal: false
    }

    this.onSettingsClicked = this.onSettingsClicked.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)
    this.handleMenuButton = this.handleMenuButton.bind(this)
    this.onLogoutClicked = this.onLogoutClicked.bind(this)
  }

  
  onSettingsClicked(){
    this.setState({isShowModal:true});
  }

  handleCloseModal(){
    this.setState({isShowModal:false});
  }

  handleMenuButton(){
    if(this.props.onMenuButtonClicked){
      this.props.onMenuButtonClicked()
    }
  }

  onLogoutClicked(onLogout){
    return () => {
      let result = window.confirm("Are you sure you want to logout?")
      if(result){
        
        if(onLogout){
          onLogout()
        }

        history.push('/login')
      }
    }
  }

  render(){
    const { user } = this.props;

    return (
        <Navbar fluid collapseOnSelect fixedTop className="navbar-light bg-light" style={custom3DHeader}>
        <Navbar.Header>
          <Navbar.Brand onClick={Navs.home}>
            <span style={{marginLeft:2, padding:10, marginRight:10}}><img src={require('../aiiiland.png')} width={40} className="img-rounded" style={{boxShadow:'0 0 10px rgba(0,0,0,0.3)'}} title="Aiiiland" /></span>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Collapse>
        <Nav>
          {/* <NavItem onClick={Navs.home}>Home</NavItem> */}
          <NavItem onClick={Navs.dashboard}><DashboardIcon style={{verticalAlign:'baseline', fontSize:14}} /> Dashboard</NavItem>
          <NavItem onClick={Navs.cameraAdmin}><CameraIcon style={{verticalAlign:'baseline', fontSize:14}}/> Cameras</NavItem>
          <NavItem onClick={Navs.peopleAdmin}><UserIcon style={{verticalAlign:'baseline', fontSize:14}}/> People</NavItem>
          <NavItem onClick={Navs.settings}><SettingsIcon style={{verticalAlign:'baseline', fontSize:14}}/> Settings</NavItem>
          <NavItem onClick={Navs.surveillanceLog}><EyeIcon style={{verticalAlign:'baseline', fontSize:14}}/> Surveillance Log</NavItem>
        </Nav>
        <AuthContext.Consumer>
        {
          ({ user, isLoggedIn, onLogout }) => 
          <Nav pullRight>
            {
              isLoggedIn? 
              <NavItem href="javascript:void(0)" onClick={this.onLogoutClicked(onLogout)}><LogoutIcon /> Hi {user}, Logout</NavItem>
              : 
              <NavItem href="javascript:void(0)" onClick={Navs.login}>Login</NavItem>
            }
          </Nav>
        }
        </AuthContext.Consumer>
        </Navbar.Collapse>
      </Navbar>
    )
  }

}
