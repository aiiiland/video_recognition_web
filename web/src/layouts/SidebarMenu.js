import React, { Component } from 'react';
import _ from 'lodash'
// import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Nav, NavItem, Badge, Label } from 'react-bootstrap'

import MailIcon from "react-icons/lib/fa/envelope-o"

import browserHistory from '../history'
import { menu as MenuConfig } from '../config/menu'
import './sidebar.css'

const headerStyle = {
    padding:8,
    fontWeight: 'bold',
    color:'#000',
    textTransform: 'uppercase',
    backgroundColor:'#dcdcdc'
}

// for test
const goToBlank = () => browserHistory.push('/blank');

function SidebarSectionHeader({children}){
    return (
        <div className="section-header">{children}</div>
    )
}

function NavHeader({children}){
    return <li style={headerStyle}>{children}</li>
}


function NavItemPermission({ node }){
    return (
        <NavItem key={node.url} eventKey={node.url}>{node.title}</NavItem>
    )
}


class SidebarMenu extends Component{

    constructor(props){
        super(props)

        this.state = {
            activeKey: 1,
            isSelfInit: false,
            permissionFilteredMenu: [],
        }

        this.handleSelect = this.handleSelect.bind(this)
    }

    componentDidMount(){
        this.updateNavActiveItem(this.props)
        this.filterMenuWithUser(this.props)
    }

    componetWillReceiveProps(nextProps){
        if(this.props.pageState !== nextProps.pageState){
            // this.setState({ activeKey: nextProps.pageState.path })
            this.updateNavActiveItem(nextProps)
        }

        if(this.props.hasInit !== nextProps.hasInit || this.props.user_role !== nextProps.user_role){
            this.filterMenuWithUser(nextProps)
        }
    }

    updateNavActiveItem(props){
        if(props.pageState){
            this.setState({ activeKey: props.pageState.path })
        }
    }

    filterMenuWithUser(props){
        if(props.hasInit && !this.state.isSelfInit){
            const { user_role } = props;
            const user_permissions = user_role.permissions;

            // Sys admin = no filter
            if(user_role.system_role == 0){
                this.setState({
                    isSelfInit: true,
                    permissionFilteredMenu: _.cloneDeep(MenuConfig)
                })
                return;
            }

            // other user, need filter
            let filtered = _.cloneDeep(MenuConfig)
            filtered = filtered.map((node) => {

                if(node.access){
                    if(!_.includes(node.access, user_role.system_role)){
                        return false;
                    }
                }

                if(node.items){
                    let filteredItems = node.items.map((subitem) => {
                        if(subitem.requirePermission){
                            if(!_.includes(user_permissions, subitem.requirePermission)){
                                return false;
                            }
                        }
                        return subitem;
                    }).filter(Boolean)

                    if(filteredItems.length == 0){
                        // if no sub-menu to display, hide the section
                        return false
                    }

                    node.items = filteredItems;
                }

                return node;
            }).filter(Boolean)

            this.setState({
                isSelfInit: true,
                permissionFilteredMenu: filtered
            })

            
        }
    }

    handleSelect(key){
        // console.log(key)
        this.setState({activeKey: key})
        browserHistory.push(key)
    }

    render(){
        const { notification } = this.props;
        return (
            <Nav bsStyle="pills" stacked activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                {
                    this.props.hasInit &&
                    [
                        <NavHeader key="mainmenu">Main Menu</NavHeader>,
                        <NavItem key="inbox" eventKey="/cms">Inbox {notification && notification.newCount > 0 && <Label bsStyle="danger">{notification.newCount}</Label>}</NavItem>,
                        this.state.permissionFilteredMenu.map((node) => node.type && node.type == 'group'? 
                            this.renderNavGroup(node)
                            :
                            this.renderNavItem(node)
                        )
                    ]
                }
            </Nav>
        )
    }

    renderNavGroup(node){
        const  { user_role } = this.props;

        if(_.includes(node.access, user_role.system_role)){
            return [
                <NavHeader key={node.title}>{node.title}</NavHeader>,
                ... node.items.map((subitem) => this.renderNavItem(subitem))
            ];
        }else{
            return null;
        }
    }

    renderNavItem(node){
        return <NavItem key={node.url} eventKey={node.url}>{node.title}</NavItem>
    }

}

export default connect(
(state, ownProps) => {
    return {
        pageState: state.page,
        user: state.auth.user,
        user_role: state.auth.user.profile.role,
        // user_system_role: state.auth.user.profile.role.system_role,
        // user_permissions: state.auth.user.profile.role.permissions,
        hasInit: state.auth.hasInit && state.auth.user.profile.role,

        notification: state.notification,

    }
})(SidebarMenu)