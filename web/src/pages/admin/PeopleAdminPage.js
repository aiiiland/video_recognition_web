import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { makeApiMethod, makeFormDataApiMethod } from '~/lib/api'
import history from '~/lib/history'
import { BASE } from '~/lib/base'

import Loading from '~/components/Loading'
import { Well, Row, Col, Thumbnail, Image, Button, ProgressBar } from 'react-bootstrap'
import Modal from 'react-modal';

import UploadIcon from "react-icons/lib/fa/upload"
import GalleryIcon from "react-icons/lib/fa/image"
import DeleteIcon from "react-icons/lib/md/close"

import AddPeopleForm from './AddPeopleForm'

import Dropzone from 'react-dropzone'

import "react-toggle/style.css" // for ES6 modules
import Toggle from 'react-toggle'


const getPeople = makeApiMethod({
    path: "/people",
    method: "GET"
})

const getPeopleImages = makeApiMethod({
    path: "/people/images/{key}",
    method: "POST"
})

const getPeopleAlignedImages = makeApiMethod({
    path: "/people/alignedimages/{key}",
    method: "POST"
})

const addPeople = makeApiMethod({
    path: "/people/add",
    method: "POST"
})

const removePeople = makeApiMethod({
    path: "/people/remove/{id}",
    method: "POST"
})




const customStyles = {
    overlay:{
        backgroundColor: "rgba(0, 0, 0, 0.75)"
    },
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      width:600,
      background:"#333",
    }
};



const uploadPhoto = makeFormDataApiMethod({
    path: '/upload',
    method: 'POST'
})
  

class UploadPhotoModal extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            isUploading: false,

            isFilePicked: false,
            files: [],

            existingImages: []
        }

        this.onDrop = this.onDrop.bind(this)
        this.onUpload = this.onUpload.bind(this)
    }

    componentDidMount(){
        if(this.props.people !== null){
            this.loadPeopleImages(this.props)
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.people !== this.props.people){
            this.loadPeopleImages(nextProps)
        }
    }

    async loadPeopleImages(props){
        let result = await getPeopleImages({ key: props.people.key })
        this.setState({
            existingImages: result.items
        })

    }

    onDrop(files){
        // console.log(files)

        if(files && files.length > 0){
            this.setState({ 
                isFilePicked: true,
                files,
            })
        }

    }

    async onUpload()
    {
        const people = this.props.people;

        this.setState({ isUploading: true })

        for(let index in this.state.files){
            let file = this.state.files[index]
            console.log("start upload " + file.name)
            let result = await uploadPhoto({
                name: people.key,
                photo: file
            })
            console.log("result = ", result)
        }

        this.setState({ isUploading: false, isFilePicked: false, files:[] })

        alert("Photos uploaded")
        if(this.props.onRequestClose){
            this.props.onRequestClose()
        }

    }


    render(){
        const people = this.props.people
        return (
            <Modal
                isOpen={this.props.isOpen}
                onAfterOpen={this.props.onAfterOpen}
                onRequestClose={this.props.onRequestClose}
                style={customStyles}
                contentLabel="Upload Photos">
                {
                    people &&
                    <React.Fragment>
                        <h2 className="text-muted">Upload Photos for {people.name}</h2>
                        {
                            this.state.existingImages &&
                            <p>{people.name} has {this.state.existingImages.length} images in database</p>
                        }
                        <Dropzone onDrop={this.onDrop} style={{width:'100%', minHeight:'100px', border:'2px dashed #ddd'}}>
                            <div style={{padding:15}}>
                                <p><UploadIcon /> Drop files here (.jpg)</p>
                                {
                                    !this.state.isFilePicked&&
                                    <p className="text-muted">No Photos selected</p>
                                }
                                {
                                    this.state.isFilePicked&&
                                    <p>Selected Files{" (" + this.state.files.length + ")"}:</p>
                                }
                                {
                                    this.state.isFilePicked &&
                                    <div>
                                    {
                                        _.map(_.slice(this.state.files, 0, 5), (file, index) => {
                                            return <img key={index} src={file.preview} width="50" />
                                        })
                                    }
                                    <ul className="list-unstyled">
                                        {
                                            _.map(_.slice(this.state.files, 0, 10), (file, index) => {
                                                return <li key={index}>{file.name}</li>
                                            })
                                        }
                                    </ul>
                                    {this.state.files.length > 10 && <p className="text-muted">and {this.state.files.length-10} more ...</p>}
                                    </div>
                                }
                            </div>
                        </Dropzone>
                        <p style={{paddingTop:10}}>
                            {
                                !this.state.isUploading &&
                                <Button bsStyle="primary" block onClick={this.onUpload}>Start Upload</Button>
                            }
                            {
                                this.state.isUploading &&
                                <Button bsStyle="primary" disabled block>Uploading <Loading compact size="xsmall" /></Button>
                            }
                        </p>
                        <p style={{paddingTop:10}}>
                            <Button onClick={this.props.onRequestClose}>Close</Button>
                        </p>
                    </React.Fragment>
                }
                
            </Modal>
        )
    }
}


class PhotoGalleryModal extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            showAligned: false,
            photos: [],
        }

    }

    componentDidMount(){
        if(this.props.people !== null){
            this.loadPeopleImages(this.props)
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.people !== this.props.people){
            this.loadPeopleImages(nextProps)
        }
    }

    async loadPeopleImages(props){
        let result = null
        if(this.state.showAligned){
            result = await getPeopleAlignedImages({ key: props.people.key })
        }else{
            result = await getPeopleImages({ key: props.people.key })
        }
        
        this.setState({
            photos: result.items
        })

    }

    toggleShowAligned(){
        this.setState({ showAligned: !this.state.showAligned }, () => {
            this.loadPeopleImages(this.props)
        })
    }


    render(){
        const people = this.props.people
        return (
            <Modal
                isOpen={this.props.isOpen}
                onAfterOpen={this.props.onAfterOpen}
                onRequestClose={this.props.onRequestClose}
                style={customStyles}
                contentLabel="Gallery">
                {
                    people &&
                    <React.Fragment>
                        <h2 className="text-muted">Gallery of {people.name}</h2>
                        {
                            this.state.photos &&
                            <p>{people.name} has {this.state.photos.length} images in database</p>
                        }
                        <p>Show Aligned images <Toggle
                            checked={this.state.showAligned}
                            onChange={() => this.toggleShowAligned()}
                        /></p>
                        <div className="container-fluid" style={{height:500, overflowY:'scroll'}}>
                        <table className="table">
                        <tbody>
                        {
                            _.map(this.state.photos, (file, index) => {
                                return <tr key={index}>
                                    <td>{file}</td>
                                    <td>
                                        {
                                            this.state.showAligned?
                                            <Image src={BASE + `/people/alignimg/${people.key}/${file}`} responsive />
                                            :
                                            <Image src={BASE + `/people/img/${people.key}/${file}`} responsive />
                                        }
                                        
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                        </table>
                        </div>
                        <p style={{paddingTop:10}}>
                            <Button onClick={this.props.onRequestClose}>Close</Button>
                        </p>
                    </React.Fragment>
                }
                
            </Modal>
        )
    }
}



const getSimpleAlertStatus = makeApiMethod({
    path: "/get_simple_alert_status",
    method: "POST"
})

const createSimpleAlert = makeApiMethod({
    path: "/create_simple_alert",
    method: "POST"
})
const removeSimpleAlert = makeApiMethod({
    path: "/remove_simple_alert",
    method: "POST"
})


class AlertToggleButton extends React.PureComponent
{

    constructor(props){
        super(props)

        this.state = {
            checked: false
        }

        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount(){
        const person = this.props.person
        getSimpleAlertStatus({ person: person.key }).then((res) => {
            this.setState({
                checked: res.status !== false
            })
        })
    }

    handleChange(e){
        const person = this.props.person
        const isChecked = e.target.checked
        if(isChecked){
            createSimpleAlert({ person: person.key }).then((result) => {
                console.log("result = ", result)
                this.setState({checked: isChecked})
            })
            
        }else{
            removeSimpleAlert({ person: person.key }).then((result) => {
                console.log("result = ", result)
                this.setState({checked: isChecked})
            })
        }
    }

    render(){
        
        return (
            <Toggle
                checked={this.state.checked}
                onChange={this.handleChange}
            />
        )
    }

}



export default class PeopleAdminPage extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            isShowForm: false,

            isUploadPhotoModal: false,
            selectedPeople: null,

            isGalleryModal: false,

            people: []
        }

        this.onCreateBtnClicked = this.onCreateBtnClicked.bind(this)
        this.onRemoveBtnClicked = this.onRemoveBtnClicked.bind(this)
        this.onItemAdded = () => {
            this.setState({ isShowForm: false })
            this.fetchPeople()
        }
    }

    componentDidMount(){
        this.fetchPeople()
    }

    onCreateBtnClicked(){
        this.setState({ isShowForm: !this.state.isShowForm })
    }

    async fetchPeople(){
        let result = await getPeople()
        console.log(result)

        this.setState({
            people: result.items
        })
    }

    async onRemoveBtnClicked(id){
        if(!window.confirm("All photos of the person will be removed, continue?")){
            return   
        }
        let result = await removePeople({ id })
        this.fetchPeople()
    }

    onUploadPhotosClicked(people){
        this.setState({
            isUploadPhotoModal: true,
            selectedPeople: people
        })
    }
    onGalleryClicked(people){
        this.setState({
            isGalleryModal: true,
            selectedPeople: people
        })
    }

    render(){
        return (
            <div>
                <h2>People Management</h2>

                <p>
                    <Button bsStyle="primary" onClick={this.onCreateBtnClicked}>Add</Button>
                </p>
                {
                    this.state.isShowForm &&
                    <AddPeopleForm onItemAdded={this.onItemAdded} />
                }

                <div className="table-responsive">
                <table className="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Key</th>
                            <th>Added Date</th>
                            <th>Action</th>
                            <th>Alert</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                        this.state.people &&
                        _.map(this.state.people, (peopleData, index) => (
                            <tr key={index}>
                                <td>#{peopleData.id}</td>
                                <td><Image src={BASE + "/people/thumbnail/"+peopleData.key} width={96} /></td>
                                <td>{peopleData.name}</td>
                                <td>{peopleData.key}</td>
                                <td>{moment(peopleData.created_at*1000).format("LLL")}</td>
                                <td>
                                    <ul className="list-inline">
                                        <li><Button bsStyle="primary" onClick={() => this.onUploadPhotosClicked(peopleData)}><UploadIcon /></Button></li>
                                        <li><Button bsStyle="info" onClick={() => this.onGalleryClicked(peopleData)}><GalleryIcon /></Button></li>
                                    </ul>
                                </td>
                                <td>
                                    <AlertToggleButton person={peopleData} />
                                </td>
                                <td>
                                <Button bsStyle="danger" onClick={() => this.onRemoveBtnClicked(peopleData.id)}><DeleteIcon /></Button>
                                </td>
                            </tr>
                        ))
                        }
                    </tbody>
                </table>
                </div>

                <UploadPhotoModal 
                    isOpen={this.state.isUploadPhotoModal}
                    onAfterOpen={null}
                    onRequestClose={() => this.setState({ isUploadPhotoModal: false })}
                    people={this.state.selectedPeople}
                />

                <PhotoGalleryModal 
                    isOpen={this.state.isGalleryModal}
                    onAfterOpen={null}
                    onRequestClose={() => this.setState({ isGalleryModal: false })}
                    people={this.state.selectedPeople}
                />

            </div>
        )
    }
}