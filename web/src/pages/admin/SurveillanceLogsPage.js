import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { makeApiMethod } from '../../lib/api'
import history from '../../lib/history'
import { BASE } from '../../lib/base'

import Loading from '~/components/Loading'
import { Well, Row, Col, Thumbnail, Image, Button, ProgressBar } from 'react-bootstrap'
import Modal from 'react-modal';

import "react-toggle/style.css" // for ES6 modules
import Toggle from 'react-toggle'

import Select from 'react-select'
import 'react-select/dist/react-select.css'

import ReactPaginate from 'react-paginate';


import TagIcon from "react-icons/lib/fa/tag"

import AlertForm from './AlertForm'


const getAllLogs = makeApiMethod({
    path: "/surveillance_logs/get_all",
    method: "POST"
})


const getPeople = makeApiMethod({
    path: "/people",
    method: "GET"
})


const postTag = makeApiMethod({
    path: "/surveillance_logs/{log_id}/tag",
    method: "POST"
})

const postUnTag = makeApiMethod({
    path: "/surveillance_logs/{log_id}/untag",
    method: "POST"
})

const PER_PAGE = 100

export default class SurveillanceLogsPage extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            isTagPersonModal: false,
            selectedItem: null,

            items: {},
            total: 0,
            pageCount: 0,
            offset: 0,
        }

        this.loadData = this.loadData.bind(this)
        this.handlePageClick = this.handlePageClick.bind(this)
    }

    componentDidMount(){
        this.loadData({ offset: 0 })
    }

    async loadData(params){
        let offset = this.state.offset;
        if(params && params.offset != null)
            offset = params.offset

        let result = await getAllLogs({ offset: offset, limit: PER_PAGE })

        console.log("result = ", result)

        this.setState({
            items: result.items,
            total: result.total,
            pageCount: Math.ceil(result.total / PER_PAGE)
        })
    }

    onTagClicked(item){
        this.setState({
            isTagPersonModal: true,
            selectedItem: item
        })
    }

    async onSubmitUnTag(item){
        const log = item

        // this.setState({ isSubmitting: true })

        let result = await postUnTag({ log_id: log.id })

        console.log(result)

        // this.setState({ isSubmitting: false })

        // alert("Tagged")
        this.loadData()
    }

    handlePageClick(data){
        let selected = data.selected;
        let offset = Math.ceil(selected * PER_PAGE);
        this.loadData({ offset })
        this.setState({ offset })
    }

    render(){

        return (
            <div>
                <h2>Surveillance Logs</h2>
                <p>Total {this.state.total} entries</p>
                <ReactPaginate previousLabel={"previous"}
                       nextLabel={"next"}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={this.state.pageCount}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination"}
                       subContainerClassName={"pages pagination"}
                       activeClassName={"active"} />
                <table className="table table-bordered table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Time</th>
                            <th>Camera</th>
                            <th>identity</th>
                            <th>Confidence</th>
                            <th>Photo</th>
                            <th>Action</th>
                            <th>User Tag</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            _.map(this.state.items, (item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{item.id}</td>
                                        <td>{moment(item.log_time*1000).format("LLL")}</td>
                                        <td><span title={"#" + item.camera_id}>{item.camera_name || `Cam #${item.camera_id}` }</span></td>
                                        <td>{item.identity}</td>
                                        <td>{item.confidence}</td>
                                        <td><img src={BASE + "/logs_img/" + item.photo} width="48" /></td>
                                        <td>
                                            <ul className="list-inline">
                                                {
                                                    !item.user_flag &&
                                                    <li><Button bsSize="sm" bsStyle="info" onClick={() => this.onTagClicked(item)}><TagIcon /> Tag ...</Button></li>
                                                }
                                                {
                                                    item.user_flag &&
                                                    <li><Button bsSize="sm" bsStyle="danger" onClick={() => this.onSubmitUnTag(item)}><TagIcon /> UnTag</Button></li>
                                                }
                                            </ul>    
                                        </td>
                                        <td>{item.user_flag}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <TagPersonModal 
                    isOpen={this.state.isTagPersonModal}
                    onAfterOpen={null}
                    onRequestClose={() => this.setState({ isTagPersonModal: false })}
                    log={this.state.selectedItem}
                    onAnythingChanged={this.loadData}
                />
                
            </div>
        )
    }


}





const customStyles = {
    overlay:{
        backgroundColor: "rgba(0, 0, 0, 0.75)"
    },
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      width:600,
      background:"#333",
    }
};




const strValue = (value) => {
    if(value != null || typeof value === 'undefined'){
        return "" + value;
    }else{
        return null;
    }
}

class TagPersonModal extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            selectedPerson: null,

            isSubmitting: false
        }

        this.getOptions = this.getOptions.bind(this)
        this.onSelected = this.onSelected.bind(this)
        this.onSubmitTag = this.onSubmitTag.bind(this)
    }

    componentDidMount(){

    }

    getOptions(input){

        return getPeople().then((response) => {
            // console.log("response", response)
            
            let options = _.map(response.items, (person) => ({ label: person.name + ` (#${person.id})`, value: person.id }));
            // this.setState({ loadedOptions: options })
            
            return {
                options: options,
            }
        })
    }

    onSelected(value){
        console.log(value)
        this.setState({ selectedPerson: value })
    }

    async onSubmitTag(){
        const log = this.props.log

        this.setState({ isSubmitting: true })

        let result = await postTag({ log_id: log.id, person_id: this.state.selectedPerson })

        console.log(result)

        this.setState({ isSubmitting: false })

        if(this.props.onAnythingChanged){
            this.props.onAnythingChanged()
        }

        this.props.onRequestClose()

        // alert("Tagged")
    }


    render(){
        const log = this.props.log
        return (
            <Modal
                isOpen={this.props.isOpen}
                onAfterOpen={this.props.onAfterOpen}
                onRequestClose={this.props.onRequestClose}
                style={customStyles}
                contentLabel="Tag Person">
                {
                    log &&
                    <React.Fragment>
                        <h2 className="text-muted">Tag Person</h2>
                        
                        <div className="form">
                            <div style={{padding:10, backgroundColor:"#222", marginBottom:10}}>
                                <Image src={BASE + "/logs_img/" + log.photo} />
                            </div>
                            <div className="form-group">
                            <Select.Async
                                loadOptions={this.getOptions}
                                onChange={this.onSelected}
                                value={this.state.selectedPerson}
                                placeholder="Select Person ..."
                                simpleValue
                            />
                            </div>


                            <div className="form-group">
                            <Button bsStyle="info" onClick={this.onSubmitTag} disabled={this.state.isSubmitting}><TagIcon /> {this.state.isSubmitting? "Loading": "Submit Tag" }</Button>
                            </div>

                        </div>
                        
                        <p style={{paddingTop:10}}>
                            <Button onClick={this.props.onRequestClose}>Close</Button>
                        </p>
                    </React.Fragment>
                }
                
            </Modal>
        )
    }
}