import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { makeApiMethod } from '../../lib/api'
import history from '../../lib/history'
import { BASE } from '../../lib/base'

import Loading from '~/components/Loading'
import { Well, Row, Col, Thumbnail, Image, Button, ProgressBar } from 'react-bootstrap'

import RocketIcon from 'react-icons/lib/fa/rocket'

import AlertForm from './AlertForm'


const getAllSettings = makeApiMethod({
    path: "/settings/get_all",
    method: "POST"
})


const retrainClassifierApi = makeApiMethod({
    path: "/retrain_classifier",
    method: "POST"
})

const retrainEncodingsApi = makeApiMethod({
    path: "/retrain_encodings",
    method: "POST"
})


class RetrainButton extends React.PureComponent{
    constructor(props){
        super(props)

        this.state = {
            loading: false,
            // startTime: Date.now(),
        }

        this.onClick = this.onClick.bind(this)
    }

    async onClick(){
        let userConfirm = window.confirm("This could take a few minutes, continue?");
        if(!userConfirm) return;
        
        this.setState({ loading: true })
        await retrainClassifierApi()

        this.setState({ loading: false })

        alert("Retrain finished")
    }

    render(){
        if(this.state.loading){
            return (
                <Button bsStyle="info" disabled><Loading compact size="xsmall" /> Training ...</Button>
            )
        }else{
            return (
                <Button bsStyle="info" onClick={this.onClick}><RocketIcon /> Train Engine</Button>
            )
        }
    }


}



class RetrainEncodingsButton extends React.PureComponent{
    constructor(props){
        super(props)

        this.state = {
            loading: false,
            // startTime: Date.now(),
        }

        this.onClick = this.onClick.bind(this)
    }

    async onClick(){
        let userConfirm = window.confirm("This could take a few minutes, continue?");
        if(!userConfirm) return;
        
        this.setState({ loading: true })
        await retrainEncodingsApi()

        this.setState({ loading: false })

        alert("Retrain finished")
    }

    render(){
        if(this.state.loading){
            return (
                <Button bsStyle="danger" disabled><Loading compact size="xsmall" /> Training ...</Button>
            )
        }else{
            return (
                <Button bsStyle="danger" onClick={this.onClick}>Re-Train Encodings Classifiers</Button>
            )
        }
    }


}


export default class SettingsPage extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            settings: {}
        }
    }

    componentDidMount(){
        this.loadSettings()
    }

    async loadSettings(){
        let result = await getAllSettings()

        console.log("result = ", result)

        this.setState({
            settings: result.items
        })
    }

    render(){

        return (
            <div>
                <h2>Settings</h2>

                <Well>
                    <AlertForm 
                        settings={this.state.settings}
                    />
                    
                </Well>

                <Well>
                    <h3>AI Face Learning Engine</h3>
                    <p>Tell the AI Engine to learn the Faces after new person or new photos added</p>
                    <p><RetrainButton /></p>
                    {/* <p><RetrainEncodingsButton /></p> */}
                </Well>
                
            </div>
        )
    }


}
