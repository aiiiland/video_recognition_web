import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { Form as ReactForm, Field, Text, TextArea } from 'react-form';
import {
    FormGroup,
    Label,
    FormControl
} from 'react-bootstrap'

import Loading from '../../components/Loading'

import { Button } from 'react-bootstrap'

import SelectField from '../../components/SelectField'

import { makeApiMethod } from '~/lib/api'
import * as StorageUtils from '~/lib/storage'


const getAllSettings = makeApiMethod({
    path: "/settings/get_all",
    method: "POST"
})


const saveSettings = makeApiMethod({
    path: "/settings/save",
    method: "POST"
})


const ReactFormSelectField = (props) => (
    <Field field={props.field}>
    {
        fieldApi => {
            const { onChange, onBlur, field, ...rest } = props
            const { value, error, warning, success, setValue, setTouched } = fieldApi

            console.log("value = " + value)
            return (
                <SelectField
                    choice={props.choice}
                    onBlur={onBlur}
                    value={value}
                    onChange={value => {
                        setValue(value)
                        if (onChange) {
                          onChange(value)
                        }
                      }}
                />
            )
        }
    }
    </Field>
)

export default class AlertForm extends React.PureComponent
{

    constructor(props) {
        super(props);

        this.formDefault = {
            notification_email: props.notification_email || "",
        }

        this.state = {
            isSubmitting: false
        }

        this.onFormSubmit = this.onFormSubmit.bind(this)

        this.resolveFormReady = null
        this.isFormReady = new Promise((resolve) => {
            this.resolveFormReady = resolve
        })

    }

    componentDidMount(){
        // this.loadSettings()
    }

    componentWillReceiveProps(nextProps){
        if(this.props.settings !== nextProps.settings){
            this.updateFormFromProps(nextProps)
        }
    }

    updateFormFromProps(props){
        const settings = props.settings
        this.formApi.setValue("notification_email", settings.notification_email)
    }

    async onFormSubmit(formValue){
        console.log("onFormSubmit", formValue)

        this.setState({ isSubmitting: true })

        let result = await saveSettings({
            notification_email: formValue.notification_email,
        })

        console.log("result = ", result)

        if(this.props.onItemAdded){
            this.props.onItemAdded(result)
        }

        this.setState({ isSubmitting: false })
    }

    render(){

        return (
            <div className="well">
            <ReactForm onSubmit={this.onFormSubmit} defaultValues={this.formDefault}>
            {formApi => {
                this.formApi = formApi
                this.resolveFormReady(this.formApi)
                return (
                    <form onSubmit={formApi.submitForm} id="form1" className="form">
                        <legend>
                            <h4>Alert Notifications</h4>
                        </legend>
                        <FormGroup>
                            <label className="control-label" htmlFor="notification_email">Email</label>
                            <Text field="notification_email" id="notification_email" className="form-control input-block" placeholder="e.g. John Doe" />
                            <p className="help-block">The email address to receive alert notifications when the system recognized a face</p>
                        </FormGroup>
    
                        {
                            !this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit">Save</Button>
                        }
    
                        {
                            this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit" disabled>Submitting <Loading compact size="xsmall" /></Button>
                        }
                        
                    </form>
                )
            }}
            </ReactForm>
            </div>
        )
    }
}