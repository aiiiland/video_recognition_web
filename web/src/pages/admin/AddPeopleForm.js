import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { Form as ReactForm, Field, Text, TextArea } from 'react-form';
import {
    FormGroup,
    Label,
    FormControl
} from 'react-bootstrap'

import Loading from '../../components/Loading'

import { Button } from 'react-bootstrap'

import SelectField from '../../components/SelectField'

import { makeApiMethod } from '~/lib/api'
import * as StorageUtils from '~/lib/storage'


const addPeople = makeApiMethod({
    path: "/people/add",
    method: "POST"
})


const ReactFormSelectField = (props) => (
    <Field field={props.field}>
    {
        fieldApi => {
            const { onChange, onBlur, field, ...rest } = props
            const { value, error, warning, success, setValue, setTouched } = fieldApi

            console.log("value = " + value)
            return (
                <SelectField
                    choice={props.choice}
                    onBlur={onBlur}
                    value={value}
                    onChange={value => {
                        setValue(value)
                        if (onChange) {
                          onChange(value)
                        }
                      }}
                />
            )
        }
    }
    </Field>
)

export default class AddPeopleForm extends React.PureComponent
{

    constructor(props) {
        super(props);

        this.formDefault = {
            name: "",
            key: "",
        }

        this.state = {
            isSubmitting: false
        }

        this.onFormSubmit = this.onFormSubmit.bind(this)

    }

    componentDidMount(){

    }

    async onFormSubmit(formValue){
        console.log("onFormSubmit", formValue)

        this.setState({ isSubmitting: true })

        let result = await addPeople({
            name: formValue.name,
            key: formValue.key,
        })

        console.log("result = ", result)

        if(this.props.onItemAdded){
            this.props.onItemAdded(result)
        }

        this.setState({ isSubmitting: false })
    }

    render(){

        return (
            <div className="well">
            <ReactForm onSubmit={this.onFormSubmit} defaultValues={this.formDefault}>
            {formApi => {
                this.formApi = formApi
                // this.resolveFormReady(this.formApi)
                return (
                    <form onSubmit={formApi.submitForm} id="form1" className="form">
                        <legend>
                            <h4>Add People</h4>
                        </legend>
                        <FormGroup>
                            <label className="control-label" htmlFor="name">Name</label>
                            <Text field="name" id="name" className="form-control input-block" placeholder="e.g. John Doe" />
                        </FormGroup>
    
                        <FormGroup>
                            <label className="control-label" htmlFor="key">Key</label>
                            <Text field="key" id="key" className="form-control input-block" placeholder="e.g. JohnDoe" />
                        </FormGroup>

                        <p className="text-muted">(Photos can be added after the person record is created)</p>
    
                        {
                            !this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit">Submit</Button>
                        }
    
                        {
                            this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit" disabled>Submitting <Loading compact size="xsmall" /></Button>
                        }
                        
                    </form>
                )
            }}
            </ReactForm>
            </div>
        )
    }
}