import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

// import { makeApiMethod } from '../lib/api'
import history from '../../lib/history'
import { BASE } from '../../lib/base'

import { Jumbotron, Well, Row, Col, Thumbnail, Image, Button, ProgressBar } from 'react-bootstrap'
import Loading from '~/components/Loading'

import RefreshIcon from "react-icons/lib/md/refresh"


const smallPrint = {
    fontSize:'10px',whiteSpace:'nowrap'
}

function StubPeople()
{
    const fakeThumbnailBg = {
        display: 'block',
        padding: 2,
        marginBottom: 21,
        lineHeight: 1.42857143,
        backgroundColor: '#222222',
        border: '1px solid #222',
        borderRadius: 4,
        color:'#3a3a3a',
        paddingTop:9
    }
    return (
        <div className="dashboard-people-item" style={{display:'inline-block', width:120}}>
            <div width="120" style={fakeThumbnailBg}>
                <div style={{width:96, height:96, background:'#303030',margin:'0 auto'}}></div>
                <div style={{padding:9}}>
                    <div>___</div>
                    <div>___</div>
                    <ProgressBar now={0} />
                    <small>___<br/>___</small>
                </div>
            </div>
        </div>
    )
}

function People({ person, advanced })
{
    let img_string = BASE + "/get_faceimg/"+ person.uniqueKey +"_" + person.camera + "?q=" + person.timeD;
    const time = moment(person.time*1000);
    return (
        <div className="dashboard-people-item" style={{display:'inline-block', width:120}}>
            <Thumbnail src={img_string} width={120}>
                {
                    (person.identity == "unknown" && person.confidence > 50) ?
                    <div title={person.uniqueKey}><span>Look like {person.predictLabel}</span></div>
                    :
                    <div title={person.uniqueKey}><span>{person.identity}</span></div>
                }
                {
                    advanced &&
                    <div style={smallPrint}><span>ID: {person.uniqueKey}</span></div>
                }
                <ProgressBar now={person.confidence} label={`${person.confidence}%`} />
                {
                    advanced &&
                    <small className="text-muted" title={time.format("LLL")}>{time.format("LLL")}</small>
                }
                {
                    !advanced &&
                    <small className="text-muted" style={smallPrint} title={time.format("LLL")}>{time.fromNow()}</small>
                }
            </Thumbnail>
        </div>
    )
}


let staticTrySerialNum = 0

export class StreamPreview extends React.Component
{
    constructor(props){
        super(props)

        this.state = {
            ready: false,
        }

        this.reload = this.reload.bind(this)
    }

    componentDidMount(){
        this.loadImage(this.props.src)
    }

    loadImage(src){
        console.log("loading image")

        let timer = null
        
        const checkImgage = () => {
            // console.log(image.width)
            if(this.img && this.img.width > 0){
                console.log(this.img.width)
                this.setState({
                    ready: true
                })
                if(timer){
                    clearInterval(timer)
                }
            }
        }

        timer = setInterval(checkImgage, 1000)

        checkImgage()
    }

    reload(){
        this.setState({ ready: false })
        setTimeout(() => {
            staticTrySerialNum++
            this.setState({ ready: true})
        }, 300)
    }

    render(){
        const { camera, src, width, ...otherProps } = this.props


        let imageStyle = this.state.ready? {
            width: width? width: '100%'            
        } : {
            transform: "translate(-1000px, -1000px)"
        }

        const uncachedSrc = src + "?q="+camera.created_at + "&t="+staticTrySerialNum

        return (
            <div>
                {
                    !this.state.ready &&
                    <div style={{width:'100%', height: 200, background:'#111', display:'flex', alignItems:'center', justifyContent:'center'}}>
                        <Loading compact type="bubbles" />
                    </div>
                }
                {
                    !this.state.ready &&
                    <div style={{width:1, height:1, overflow:'hidden'}}>
                    <img ref={img => this.img = img} src={uncachedSrc} {...otherProps} style={imageStyle} />
                    </div>
                }
                {
                    this.state.ready &&
                    <img ref={img => this.img = img} src={uncachedSrc} {...otherProps} style={imageStyle} />
                }
                <div style={{marginTop:5}}>
                    <Button bsStyle="default" bsSize="sm" onClick={this.reload}><RefreshIcon /></Button>
                </div>
            </div>
        )
    }

}


class Camera extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            largeMode: false
        }

        this.toggleLargeMode = () => this.setState({ largeMode: !this.state.largeMode })
    }


    render(){
        const { camera, people, advanced } = this.props
        let camNum = camera.camNum;
        let streamURL = BASE + "/video_streamer/"+camNum

        const knownPeople = _.filter(people, (item) => item.identity != 'unknown')
        const unknownPeople = _.filter(people, (item) => item.identity == 'unknown')

        const sortedKnownPeople = _.sortBy(knownPeople, [item => -item.timeD, item=> item.confidence])
        const sortedUnKnownPeople = _.sortBy(unknownPeople, [item => -item.timeD, item=> item.confidence])

        return (
            <Well>
                <Row>
                    <Col sm={this.state.largeMode? 8 : 3}>
                        <p>{camera.label? camera.label : `Cam #${camera.camNum}` }</p>
                        <div style={{position:'relative'}}>
                            <StreamPreview camera={camera} src={streamURL} responsive onClick={this.toggleLargeMode} />
                            <div style={{color:"rgba(255,0,0,0.6)", position:'absolute', top:4, left:4, fontFamily:'arial', fontSize:12}}>• LIVE</div>
                        </div>
                        <p className="text-muted">
                            <small>Online since: {moment(camera.created_at*1000).format("LLL")}</small>
                        </p>
                    </Col>
                    <Col sm={this.state.largeMode? 4 : 9}>
                        <p>Face Detected:</p>
                        <div style={{verticalAlign:'top'}}>
                        {
                            _.map(sortedKnownPeople, (item, index) => {
                                return (
                                    <People key={index} person={item} advanced={advanced}  />
                                )
                            })
                        }
                        {
                            sortedUnKnownPeople.length > 0 &&
                            <React.Fragment>
                                <p>Unknowns:</p>
                                {
                                    _.map(sortedUnKnownPeople, (item, index) => {
                                        return (
                                            <People key={index} person={item} advanced={advanced}  />
                                        )
                                    })
                                }
                            </React.Fragment>
                        }
                        
                        {
                            (!people || people.length == 0) &&
                            <React.Fragment>
                                <StubPeople />
                                <StubPeople />
                                <StubPeople />
                                <StubPeople />
                            </React.Fragment>
                        }
                        </div>
                    </Col>
                </Row>
            </Well>
        )
    }
}



export default class DashboardPage extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            autoRefresh: true,
            advanced: false,
        }

        this.toggleAutoRefresh = this.toggleAutoRefresh.bind(this)

        this.toggleAdvanced = () => this.setState({ advanced: !this.state.advanced })
    }

    componentDidMount(){
        this.startPeriodicCheck()
    }

    componentWillUnmount(){
        clearInterval(this.timer)
        this.timer = null
    }

    startPeriodicCheck(){
        this.timer = setInterval(() => {
            this.props.requestUpdateFaces()
        }, 4000)

        return this.timer
    }

    toggleAutoRefresh(){
        if(this.state.autoRefresh){
            this.setState({ autoRefresh: false })
            //switch off
            clearInterval(this.timer)
            this.timer = null
        }else{
            this.setState({ autoRefresh: true })

            this.startPeriodicCheck()
        }
    }


    render(){

        return (
            <div>
                <h2>Dashboard</h2>
                <p>Current active cams: {this.props.camNum}</p>
                <p>
                    <ul className="list-inline">
                        <li><Button bsSize="sm" onClick={this.toggleAdvanced}>Detail: {this.state.advanced? "On" : "Off"}</Button></li>
                        <li><Button bsSize="sm" onClick={this.toggleAutoRefresh}>AutoRefresh: {this.state.autoRefresh? "On" : "Off"}</Button></li>
                        <li><Button bsStyle="primary" bsSize="sm" onClick={this.props.requestUpdateFaces}><RefreshIcon /></Button></li>
                    </ul>
                </p>
                {
                    this.props.cameras &&
                    _.map(this.props.cameras, (camera, index) => {
                        return (
                            <div key={index}>
                                <Camera 
                                    camera={camera} 
                                    people={this.props.people[camera.camNum]}
                                    advanced={this.state.advanced}
                                />
                            </div>
                        )
                    })
                }

                {
                    (!this.props.cameras || this.props.cameras.length == 0) &&
                    <Well>
                        <p className="text-muted">Go to <a href="#/admin/camera-admin">Camera Management</a> to add Cameras</p>
                    </Well>
                }

                {/* <Button disabled>Surveillance Log (Coming soon)</Button> */}
            </div>
        )
    }


}
