import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { makeApiMethod } from '../../lib/api'
import history from '../../lib/history'
import { BASE } from '../../lib/base'

import { Well, Row, Col, Thumbnail, Image, Button, ProgressBar } from 'react-bootstrap'

import StartIcon from "react-icons/lib/md/play-arrow"
import StopIcon from "react-icons/lib/md/pause"
import DeleteIcon from "react-icons/lib/md/close"

import AddCameraForm from './AddCameraForm'

import { StreamPreview } from './DashboardPage'


const ALGORITHMS = {
    "detect_motion": "Motion Detection",
    "detect_recognise": "Face Recognition",
    "motion_detect_recognise": "Motion Detection & Face Recognition",
    "segment_detect_recognise": "Motion Object Segmentation & Face Recognition",
    "detect_recognise_track": "Face Recognition & Tracking",
    "openface": "Openface",
    "face_capture": "Face capture",
    "face_detection_embedding": "cnn embedding face detectoin",
    "face_capture_embedding": "cnn embedding Face capture"
}

const DETECTION_METHOD = {
    opencv: "Opencv",
    dlib: "Dlib"
}

const addCameraApi = makeApiMethod({
    path: "/add_camera",
    method: "POST"
})

const removeCameraApi = makeApiMethod({
    path: "/remove_camera",
    method: "POST"
})

const startCameraApi = makeApiMethod({
    path: "/start_camera",
    method: "POST"
})

const stopCameraApi = makeApiMethod({
    path: "/stop_camera",
    method: "POST"
})



export default class CameraAdminPage extends React.PureComponent
{
    constructor(props){
        super(props)

        this.state = {
            isShowForm: false
        }

        this.onCreateBtnClicked = this.onCreateBtnClicked.bind(this)
        this.onRemoveBtnClicked = this.onRemoveBtnClicked.bind(this)
        this.onCameraAdded = () => this.setState({ isShowForm: false })


        this.onStartBtnClick = async (camID) => await startCameraApi({ camID })
        this.onStopBtnClick = async (camID) => await stopCameraApi({ camID })
    }

    onCreateBtnClicked(){
        this.setState({ isShowForm: !this.state.isShowForm })
    }

    async onRemoveBtnClicked(camNum){
        if(!window.confirm("Delete this camera?")){
            return
        }

        let result = await removeCameraApi({ camID: camNum })

        console.log("result = ", result)
    }

    render(){

        const CAMERA_LIMIT = 4

        const cameraLimitReached = this.props.cameras && this.props.cameras.length > (CAMERA_LIMIT - 1)

        return (
            <div>
                <h2>Camera Management</h2>

                <p>
                    <Button bsStyle="primary" disabled={cameraLimitReached} onClick={this.onCreateBtnClicked}>Add</Button>
                </p>
                {
                    !cameraLimitReached &&
                    this.state.isShowForm &&
                    <AddCameraForm onCameraAdded={this.onCameraAdded} />
                }

                <p>{this.props.camNum} active cameras</p>
                <div className="table-responsive">
                <table className="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Preview</th>
                            <th>Details</th>
                            <th>Action</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                        this.props.cameras &&
                        _.map(this.props.cameras, (cameraData, index) => (
                            <tr key={index}>
                                <td>#{cameraData.camNum}</td>
                                <td>
                                    <StreamPreview camera={cameraData} src={BASE + "/video_streamer/"+cameraData.camNum} width={120} />
                                </td>
                                <td>
                                {cameraData.label && <p><strong>{cameraData.label}</strong></p>}
                                <div>
                                    <div>{cameraData.url}</div>
                                    <div>{ALGORITHMS[cameraData.cameraFunction]}</div>
                                    <div>{DETECTION_METHOD[cameraData.dlibDetection]}</div>
                                    <div>Added: {moment(cameraData.created_at*1000).format("LLL")}</div>
                                    <div className={cameraData.activated? "text-success": "text-danger"}>Status: {cameraData.activated? "Running" : "Stopped"}</div>
                                </div>
                                </td>
                                <td>
                                    {
                                        cameraData.activated &&
                                        <Button bsStyle="info" onClick={() => this.onStopBtnClick(cameraData.camNum)}><StopIcon /></Button>
                                    }
                                    {
                                        !cameraData.activated &&
                                        <Button bsStyle="success" onClick={() => this.onStartBtnClick(cameraData.camNum)}><StartIcon /></Button>
                                    }
                                    
                                </td>
                                <td>
                                    <Button bsStyle="danger" onClick={() => this.onRemoveBtnClicked(cameraData.camNum)}><DeleteIcon /></Button>
                                </td>
                            </tr>
                        ))
                        }
                    </tbody>
                </table>
                </div>
                
            </div>
        )
    }


}
