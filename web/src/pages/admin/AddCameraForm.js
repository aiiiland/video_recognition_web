import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import { Form as ReactForm, Field, Text, TextArea } from 'react-form';
import {
    FormGroup,
    Label,
    FormControl
} from 'react-bootstrap'

import Loading from '../../components/Loading'

import { Button } from 'react-bootstrap'

import SelectField from '../../components/SelectField'

import { makeApiMethod } from '~/lib/api'
import * as StorageUtils from '~/lib/storage'


const addCameraApi = makeApiMethod({
    path: "/add_camera",
    method: "POST"
})


let ALGORITHMS = {
    //"detect_motion": "Motion Detection",
    "detect_recognise": "Face Recognition",
    //"motion_detect_recognise": "Motion Detection & Face Recognition",
    //"segment_detect_recognise": "Motion Object Segmentation & Face Recognition",
    //"detect_recognise_track": "Face Recognition & Tracking",
    // "openface": "Openface",
    // "face_capture": "Face capture",
    // "face_detection_embedding": "cnn embedding face detection",
    // "face_capture_embedding": "cnn embedding Face capture",
    "object_recognise_yolo": "Object Recognition",
}



const DETECTION_METHOD = {
    opencv: "Opencv",
    dlib: "Dlib"
}


const ReactFormSelectField = (props) => (
    <Field field={props.field}>
    {
        fieldApi => {
            const { onChange, onBlur, field, ...rest } = props
            const { value, error, warning, success, setValue, setTouched } = fieldApi

            console.log("value = " + value)
            return (
                <SelectField
                    choice={props.choice}
                    onBlur={onBlur}
                    value={value}
                    onChange={value => {
                        setValue(value)
                        if (onChange) {
                          onChange(value)
                        }
                      }}
                />
            )
        }
    }
    </Field>
)

export default class AddCameraForm extends React.PureComponent
{

    constructor(props) {
        super(props);

        this.formDefault = {
            url: "rtsp://192.168.1.101",
            application: "detect_recognise",
            detectionMethod: "dlib",
            fpstweak: false,
        }

        this.state = {
            isSubmitting: false
        }

        this.onFormSubmit = this.onFormSubmit.bind(this)

        this.resolveFormReady = null
        this.isFormReady = new Promise((resolve) => {
            this.resolveFormReady = resolve
        })
    }

    componentDidMount(){
        this.loadAutoSavedForm()
    }

    async loadAutoSavedForm(){

        let result = await StorageUtils.localGet("camera.url")
        console.log(result)

        if(result){
            await this.isFormReady
            this.formApi.setValue("url", result)
        }

    }

    async onFormSubmit(formValue){
        console.log("onFormSubmit", formValue)

        this.setState({ isSubmitting: true })

        let result = await addCameraApi({
            camURL: formValue.url,
            application: formValue.application,
            detectionMethod: formValue.detectionMethod,
            label: formValue.label,
            fpstweak: false,
        })

        console.log("result = ", result)

        StorageUtils.localSet("camera.url", formValue.url)

        if(this.props.onCameraAdded){
            this.props.onCameraAdded(result)
        }

        this.setState({ isSubmitting: false })
    }

    render(){

        return (
            <div className="well">
            <ReactForm onSubmit={this.onFormSubmit} defaultValues={this.formDefault}>
            {formApi => {
                this.formApi = formApi
                this.resolveFormReady(this.formApi)
                return (
                    <form onSubmit={formApi.submitForm} id="form1" className="form">
                        <legend>
                            <h4>Add Camera</h4>
                        </legend>
                        <FormGroup>
                            <label className="control-label" htmlFor="url">Camera URL</label>
                            <Text field="url" id="url" className="form-control input-block" placeholder="e.g. rtsp://192.168.1.101" />
                        </FormGroup>
    
                        <FormGroup>
                            <label className="control-label" htmlFor="application">Method</label>
                            <ReactFormSelectField field="application" id="application" choice={ALGORITHMS} />
                        </FormGroup>
    
                        {/* <FormGroup>
                            <label className="control-label" htmlFor="detectionMethod">Face Detection Method</label>
                            <ReactFormSelectField field="detectionMethod" id="detectionMethod" choice={DETECTION_METHOD} />
                        </FormGroup> */}
    
                        <FormGroup>
                            <label className="control-label" htmlFor="label">Label (optional)</label>
                            <Text field="label" id="label" className="form-control input-block" placeholder="e.g. Entrance" />
                        </FormGroup>
    
                        {
                            !this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit">Submit</Button>
                        }
    
                        {
                            this.state.isSubmitting &&
                            <Button bsStyle="danger" type="submit" disabled>Submitting <Loading compact size="xsmall" /></Button>
                        }
                        
                    </form>
                )
            }}
            </ReactForm>
            </div>
        )
    }
}