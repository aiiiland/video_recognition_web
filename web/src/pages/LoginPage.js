import React, { Component } from 'react'

// import axios from 'axios'
// import { fetch, fetchApi } from '../lib/base'
import { makeApiMethod } from '../lib/api'
import history from '../lib/history'

import { Jumbotron } from 'react-bootstrap'

import LoginBox from './LoginBox'


const loginApi = makeApiMethod({
    path: '/login',
    method: 'POST'
})

const sessionApi = makeApiMethod({
    path: '/session',
    method: 'POST'
})

export default class LoginPage extends Component
{
    constructor(props){
        super(props)

        this.performLogin = this.performLogin.bind(this)
        this.checkLogin = this.checkLogin.bind(this)
        this.onLoginSuccess = this.onLoginSuccess.bind(this)
    }

    async performLogin({ username, password }){
        console.log("username = ", username)
        console.log("password = ", password)
        // return false
        let response = await loginApi({ username: username, password: password })

        console.log("response = ", response)

        if(this.props.onLogin){
            this.props.onLogin()
        }

        return response
    }

    async checkLogin(){
        let response = await sessionApi({})

        return {
            isLoggedIn: true,
            username: response.username
        }
    }

    onLoginSuccess(){
        console.log("onLoginSuccess")

        // TODO redirect to dashboard
        history.push('/admin/dashboard')
    }

    render(){
        // const { defaultValues } = this.props;
        return (
            <div style={{paddingTop:20}}>
                <div style={{maxWidth:600, margin:'0 auto'}}>
                <Jumbotron>
                <h1><img src={require('../aiiiland.png')} width={100} className="img-rounded" style={{boxShadow:'0 0 10px rgba(0,0,0,0.3)', verticalAlign:'baseline'}} title="Aiiiland" /></h1>
                <p>Facial Recognition Security powered by Artificial Intelligence</p>
                </Jumbotron>
                <LoginBox
                    checkLogin={this.checkLogin}
                    performLogin={this.performLogin}
                    // performLoginWithToken={null}
                    onLoginSuccess={this.onLoginSuccess}
                    />
                </div>
            </div>
        )
    }
}
