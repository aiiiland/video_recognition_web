import React, { Component } from 'react'
import _ from 'lodash'

import Loading from '../components/Loading'
import { Alert } from 'react-bootstrap'
import history from '~/lib/history'

import { Route, Link, Switch, Redirect } from "react-router-dom";
import io from 'socket.io-client'

import DashboardPage from './admin/DashboardPage'
import CameraAdminPage from './admin/CameraAdminPage'
import PeopleAdminPage from './admin/PeopleAdminPage'
import SettingsPage from './admin/SettingsPage'
import SurveillanceLogsPage from './admin/SurveillanceLogsPage'

function SecurityReport(){
    return "SecurityReport"
}


const MAX_ATTEMPTS = 10

export default class AdminSection extends Component
{
    constructor(props){
        super(props)

        this.state = {

            connected: false,
            connecting: true,
            attempt: 0,
            connectFailed: false,
            errorMessage: null,

            camNum: 0,
            cameras: [],

            people: {},
        }

        this.requestUpdateFaces = this.requestUpdateFaces.bind(this)
    }

    componentDidMount(){
        const { document, location } = window

        // auth state check
        if(!this.props.isLoggedIn){
            history.replace("/")
            return
        }

        let socket = io.connect('http://' + document.domain + ':' + 5000 + '/surveillance', {
            reconnection: true,
            reconnectionAttempts: MAX_ATTEMPTS
        });
        
        this.socket = socket

        // setup error handling
        this.setupErrorHandling(socket)

        socket.on('people_detected', (dataStr) => {
            console.log("!!! people_detected !!!")
            let data = JSON.parse(dataStr);
            console.log(data)

            // let people = data.people

            let peopleGrouped = _.groupBy(data, "camera")

            console.log("peopleGrouped = ", peopleGrouped)

            this.setState({
                people: peopleGrouped
            })
        })

        socket.on('system_monitoring', () => {
            console.log("system_monitoring")
        });

        socket.on('system_data', (dataStr) => {
            console.log("system_data")
            // console.log(dataStr)

            let data = JSON.parse(dataStr);
            console.log(data)

            this.setState({
                camNum: data.camNum,
                cameras: data.cameras
            })
        });

        socket.on('cameras_updated', (data) => {
            console.log("cameras_updated")
            console.log(data)
            // let data = JSON.parse(dataStr);

            this.setState({
                camNum: data.cameras.length,
                cameras: data.cameras
            })
        });
    }

    componentWillReceiveProps(nextProps){
        // fail-safe auth state check
        if(!nextProps.isLoggedIn){
            history.replace("/")
            return
        }
    }

    setupErrorHandling(socket){
        socket.on('connect', () => {
            console.log("socket connected")
            this.setState({
                connected: true,
                connecting: false,
                connectFailed: false,
            })
        })
        socket.on('reconnecting', (attempt) => {
            this.setState({
                connected: false,
                connecting: true,
                attempt
            })
        })

        socket.on('connect_error', (error) => {
            console.log("socket not connected")
            this.setState({
                connected: false,
                connectFailed: true,
                errorMessage: "" + error
            })
        })
        socket.on('reconnect_failed', () => {
            // all reconnect failed
            this.setState({
                connected: false,
                connecting: false,
                connectFailed: true,
            })
        })
    }


    requestUpdateFaces(){
        this.socket.emit("request_update_faces")
    }


    render(){
        return (
            <div style={{paddingTop:20}}>
                {
                    this.state.connecting &&
                    <div className="well text-center">
                        <p><Loading compact size="xsmall" /> Reconnecting ...</p>
                    </div>
                }
                {
                    this.state.connectFailed &&
                    <div className="">
                        <Alert bsStyle="danger">
                            <h4>Connection failed</h4>
                            {
                                this.state.connecting &&
                                <p>Attempting reconnect ({this.state.attempt}/{MAX_ATTEMPTS})...</p>
                            }
                            {
                                !this.state.connecting &&
                                <p>Refresh this page to try again</p>
                            }
                            {
                                this.state.errorMessage &&
                                <p>{this.state.errorMessage}</p>
                            }
                        </Alert>
                    </div>
                }
                <Switch>
                    <Route path="/admin/dashboard" exact render={(props) =>{
                        return <DashboardPage 
                            {...props}
                            camNum={this.state.camNum}
                            cameras={this.state.cameras}
                            people={this.state.people}
                            requestUpdateFaces={this.requestUpdateFaces}
                        />
                    }} />
                    <Route path="/admin/people-admin" exact render={(props) => {
                        return <PeopleAdminPage
                            {...props}
                        />
                    }} />
                    <Route path="/admin/camera-admin" exact render={(props) => {
                        return <CameraAdminPage
                            {...props}
                            camNum={this.state.camNum}
                            cameras={this.state.cameras}
                        />
                    }} />
                    <Route path="/admin/settings" exact render={(props) => {
                        return <SettingsPage
                            {...props}
                        />
                    }} />

                    <Route path="/admin/logs" exact render={(props) => {
                        return <SurveillanceLogsPage
                            {...props}
                        />
                    }} />
                </Switch>
            </div>
        )
    }

}