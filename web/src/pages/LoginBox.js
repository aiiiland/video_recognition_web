import React, { Component } from 'react'
import * as StorageUtils from '../lib/storage'

import TextField from '../components/TextField';
import PasswordField from '../components/PasswordField';
import ReactLoading from 'react-loading';

import LockIcon from 'react-icons/lib/fa/lock'
import SuccessIcon from 'react-icons/lib/fa/check-circle'

function Loading(){
    return (
        <div style={{display:'inline-block'}}>
        <ReactLoading delay={0} color="#fff" type="spinningBubbles" width={24} height={24} />
        </div>
    )
}


const styles = 
{
    page: {
        // backgroundColor:"#dedede", 
        flex:1,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '0 100%',
        minHeight:568,
    },
    contentArea: {
        marginTop: 50
    },
    box: {
        padding: 0,
        // paddingTop:20,
        paddingBottom:20,
        // backgroundColor:'rgba(255,255,255,0.6)',
        margin:'0 auto',
        marginTop:20,
        // boxShadow:'0 0 10px rgba(0,0,0,0.15)'
    }
};
  
const defaultValues = {
    username: "demo",
    password: "demo"
}

export default class LoginBox extends Component
{

    constructor(props){
        super(props)
        this.state = {
            username: StorageUtils.localGet("username") || "",
            password: "",
            error: false,

            isLoading: false,
            isSuccess: false,
        }

        this.signInClicked = this.signInClicked.bind(this);
        this.onUsernameChanged = this.onUsernameChanged.bind(this);
        this.onPasswordChanged = this.onPasswordChanged.bind(this);

        this.keyCapture = this.keyCapture.bind(this)
        this.onLoginSuccess = this.onLoginSuccess.bind(this)
    }

    componentDidMount(){
        this.props.checkLogin().then(result => {
            this.setState({ isLoading: false, isSuccess: true })
            StorageUtils.localSet("username", result.username)
            this.props.onLoginSuccess(result.username);
        })
    }


    performLoginWithUserInput(){

        this.setState({ isLoading: true })

      this.props.performLogin({ username: this.state.username, password: this.state.password })
      .then((response) => {
        // console.log("success")
        // console.log(response)
        if(!response || response.error){
          StorageUtils.localSet("username", "")
          this.setState({error: true});
        }else{
            //hack: login again to get /me full profile
            StorageUtils.localSet("username", this.state.username)
            // StorageUtils.sessionSet("api_token", response.item.api_token)
            // return this.props.performLoginWithToken(response.item.api_token)
            // .then((result) => {
            //     this.onLoginSuccess(result.item)
            // })
            this.onLoginSuccess({ username: this.state.username })
        }

        this.setState({ isLoading: false })
      })
      .catch((e) => {
        console.log("some error")
        console.log(e)
        this.setState({error: true, isLoading: false});
      })
    }

    onLoginSuccess(user){
        this.setState({ isLoading: false, isSuccess: true }, () => {
            this.props.onLoginSuccess(user);
        })
    }

    signInClicked(){
        if(!this.state.isSuccess && !this.state.isLoading){
            this.performLoginWithUserInput();
        }
    }

    onUsernameChanged(newVal){
        // console.log(newVal)
        this.setState({username: newVal.value});
    }

    onPasswordChanged(newVal){
        // console.log(newVal)
        this.setState({password: newVal.value});
    }

    keyCapture(e){
        // console.log(e)
        if(e.keyCode == 13){
            // console.log("here")
            e.preventDefault();
            this.signInClicked()
        }
    }


    render(){
        const { defaultValues } = this.props;

        const boxStyle = this.state.error? "animated headShake" : "animated fadeIn";
        return (
        <div style={styles.contentArea}>
            <h3>Portal Login</h3>
            <div className={boxStyle} id="loginBox" style={styles.box}>
            
                <React.Fragment>
                    <div className="form-group has-feedback">
                        <TextField name="username" placeholder="Username" autoFocus={!this.state.username} onChange={this.onUsernameChanged} onKeyDown={this.keyCapture} value={this.state.username} disabled={this.state.isLoading} />
                        <span className="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div className="form-group has-feedback">
                        <PasswordField name="password" placeholder="Password" onChange={this.onPasswordChanged} onKeyDown={this.keyCapture} value={this.state.password} disabled={this.state.isLoading} />
                        <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    {
                        (this.state.error)?
                        <div className="row">
                            <div className="col-xs-12">
                            <p className="text-danger">Incorrect username or password</p>
                            </div>
                        </div>
                        :
                        null
                    }
                </React.Fragment>
            
            <div className="row">
                <div className="col-xs-12">
                {!this.state.isSuccess && 
                <button disabled={this.state.isLoading} type="button" onClick={this.signInClicked} className="btn btn-primary btn-block">
                    {this.state.isLoading && <Loading /> }
                    {!this.state.isLoading && !this.state.isSuccess && <React.Fragment><LockIcon /> Login</React.Fragment>}
                </button>
                }
                {this.state.isSuccess && 
                    <button disabled type="button" className="btn btn-primary btn-block">
                        <React.Fragment><SuccessIcon size={24} /> Login Success</React.Fragment>
                    </button>
                }
                </div>
            </div>
            
            </div>
        </div>
        )
    }
}

LoginBox.defaultProps = {
    devMode: false
}