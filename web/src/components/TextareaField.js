import React from 'react'

export default class TextareaField extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      value: props.value,
    }
    this.defaultProps = {
      id:null,
      placeholder:null,
      rows: 3,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.value !== this.props.value){
      this.setState({value: nextProps.value});
    }
  }

  handleChange(evt){
    const { name, onChange } = this.props;
    this.setState({value: evt.target.value})
    
    if(onChange) onChange({ name, value: evt.target.value });

  }

  render(){
    const {id, name, rows, onChange, placeholder} = this.props;

    return (
      <textarea 
        id={id} 
        className="form-control" 
        name={name} 
        rows={rows}
        value={this.state.value} 
        placeholder={placeholder}
        onChange={this.handleChange} 
        />
    )
  }
}