import React, { Component, PureComponent } from 'react'
import _ from 'lodash'

import Select from 'react-select'
import 'react-select/dist/react-select.css'

const convertLabelValuePair = (object) => {
    return _.map(object, (label, value) => ({ label, value }))
}

const strValue = (value) => {
    if(value != null || typeof value === 'undefined'){
        return "" + value;
    }else{
        return null;
    }
}

const displayValue = (choice, value) => {
    if(!value || !choice) return "";
    return choice[value] || "";
}


export default class SelectField extends PureComponent
{
    constructor(props){
        super(props)
        const { choice } = props;
        if(choice){
            this.options = convertLabelValuePair(choice)
        }else{
            this.options = null;
        }
    }

    render(){
        const { name, value, options, onChange, onBlur, disabled, clearable } = this.props;
        return (
            <Select
                name={name}
                options={this.options? this.options : options}
                onChange={onChange}
                value={strValue(value)}
                simpleValue
                disabled={disabled}
                clearable={clearable}
                onBlur={() => onBlur && onBlur(value)} // just pass the current value (updated on change) on blur
            />
        )
    }
}