import React, {Component} from 'react';

function Col({className, children, ...rest})
{
    return (
        <div {...rest} className={className}>{children}</div>
    )
}

export default Col;