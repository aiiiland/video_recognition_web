import React from 'react'


const strValue = (value) => {
  if(value != null){
      return "" + value;
  }else{
      return "";
  }
}


export default class TextField extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      value: props.value
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.value !== this.props.value){
      this.setState({value: nextProps.value});
    }
  }

  handleChange(evt){
    const { name, onChange, simpleValue } = this.props;
    this.setState({value: evt.target.value})
    
    if(onChange){
      if(simpleValue){
        onChange(evt.target.value);
      }else{
        onChange({ name, value: evt.target.value });
      }
    }

  }

  render(){
    const {id, name, onChange, placeholder, simpleValue, value, ...props} = this.props;

    return (
      <input 
        id={id} 
        className="form-control" 
        type="text" 
        name={name} 
        value={strValue(this.state.value)} 
        placeholder={placeholder}
        onChange={this.handleChange} 
        {...props}
        />
    )
  }
}


TextField.defaultProps = {
  id:null,
  placeholder:null,
}