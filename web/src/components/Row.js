import React, {Component} from 'react';

function Row({children, className, ...rest}){
    return (
        <div {...rest} className={"row " + className}>{children}</div>
    )
}

Row.defaultProps = {
    className: ""
}

export default Row;