import React from 'react'

export default class PasswordField extends React.Component
{

  constructor(props){
    super(props)
    this.state = {
      value: props.value
    }
    this.defaultProps = {
      id:null,
      placeholder:null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.value !== this.props.value){
      this.setState({value: nextProps.value});
    }
  }

  handleChange(evt){
    const { name, onChange } = this.props;
    this.setState({value: evt.target.value})
    
    if(onChange) onChange({ name, value: evt.target.value });

  }

  render(){
    const {id, name, onChange, placeholder, ...props} = this.props;

    return (
      <input 
        id={id} 
        className="form-control" 
        type="password" 
        name={name} 
        value={this.state.value} 
        placeholder={placeholder}
        onChange={this.handleChange} 
        {...props}
        />
    )
  }
}