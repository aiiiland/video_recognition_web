import React from 'react';
import ReactLoading from 'react-loading';

const Loading = ({ className, size, compact, color, type }) => {

    let style = {display:'inline-block'};

    let dimen = 100;
    if(size == 'small'){
        dimen = 50;
    }else if(size == 'xsmall'){
        dimen = 14;
    }
    style = {
        ...style,
        height: dimen, width: dimen
    }
    if(!compact){
        style.marginTop = dimen;
        style.marginBottom = dimen;
    }

    const displayColor = color? color : "#ddd"

    const displayType = type? type: "spin"

    return (
        <div style={style}>
             <ReactLoading delay={0} color={displayColor} className={className} type={displayType} height={dimen} width={dimen} /> 
        </div>
    )
};

Loading.defaultProps = {
    className: "",
    size: 'medium'
}

export default Loading;