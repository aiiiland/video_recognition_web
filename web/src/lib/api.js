import urlTemplate from 'url-template'
import _ from 'lodash'

import { fetchApi, fetchApiCustomBody } from './base' 


function getPath(config, params){
    if(params){
      var template = urlTemplate.parse(config.path);
      var path = template.expand(params);
      return path;
    }else{
      return config.path;
    }  
}

/**
 * HOF for making api call that perform actions
 * @param {*} config 
 */
export function makeApiMethod(config)
{
    return async (params) => {
        // console.log("Calling API with param", params)
        //   const path = getPath(config);
        const path = getPath(config, params);

        //make http request
        const response = await fetchApi( path, config.method, params);
        // const json = await response.json();

        if(response.error){
            throw response.error
        }

        // return result for convenience
        return response;
    }
}


export function makeFormDataApiMethod(config)
{
    return async (params) => {
        // console.log("Calling API with param", params)
        //   const path = getPath(config);
        const path = getPath(config, params);

        var formData = new FormData()
        for(let key in params){
            formData.append(key, params[key])
        }

        //make http request
        const response = await fetchApiCustomBody( path, config.method, formData);

        if(response.error){
            throw response.error
        }

        // return result for convenience
        return response;
    }
}