import React from 'react'
import { Button } from 'react-bootstrap';
// import createBrowserHistory from 'history/createBrowserHistory'
import createHashHistory from 'history/createHashHistory'
import createMemoryHistory from 'history/createMemoryHistory'

// const history = createBrowserHistory()
const history = createHashHistory()
// const history = createMemoryHistory()

history.listen((location, action) => {
// location is an object like window.location
    console.log(action, location.pathname, location.state)
})

export function goBack(){
    history.goBack()
} 

export function goHome(){
    history.push("/")
} 

export function BackButton(){
    return (
        <Button onClick={goBack}>返回</Button>
    )
}


export default history