import axios from 'axios'
import { isString, isObject } from 'lodash'

// let BASE = process.env.API_BASE_URL;
const location = window.location

export const BASE = process.env.NODE_ENV == 'development'? 
                      "http://localhost:5000" :
                      location.protocol + "//" + location.host;
const APIBASE = ""

let TOKEN = null;


export function buildQueryString(params) {
  return Object.keys(params)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
      .join('&');
}

export function fetch(url, options){
  const { method, body, ...otherOptions } = options;
  // alert("here")

  let config = null;
  if(method == 'get' || method == 'GET'){
    config = {
      url,
      method,
      params: body,
      ...otherOptions,
      responseType: 'json',
      withCredentials: true,
    }
  }else{
    config = {
      url,
      method,
      data: body,
      ...otherOptions,
      responseType: 'json',
      withCredentials: true,
    }
  }
  // console.log(config);
  return axios(config)
}

export function makeErrorHandler(resolve, reject){
  return (e) => {
    console.log("in Error handler")

    if(e.response && e.response.data){
      const response = e.response;
      const data = e.response.data;

      let error = null;
      if(data.ErrorMessage && typeof data.ErrorMessage !== 'undefined'){
        error = new Error(data.ErrorMessage); 
      }else{
        error = new Error(e.response.statusText)
      }

      if(data.ErrorPayload && typeof data.ErrorPayload !== 'undefined'){
        error.payload = data.ErrorPayload;
      }

      error.response = response;
      reject(error);

    }else{
      // if(e.response){
      //   error = new Error(e.response.statusText);
      // }
      // //attach the fetch/ajax response
      // error.response = e.response;
      reject(e);
    }

  }
}






const dataIfOk = (response) => {
  // if(response.status == 200){
  //   return response.data
  // }else{
  //   return response
  // }
  return response.data
}


export function fetchApi(api, method, params)
{
	let options = {
		method: method? method : "GET",
	}

  if (options.headers == null) {
    options.headers = {
      'Cache-Control' : 'no-cache'
    };
  }
  if (options.headers['Content-Type'] == null) {
    options.headers['Content-Type'] = 'application/json';
  }

  if (options.credentials == null) {
    options.credentials = 'include';
  }

  if(method == 'POST' || method == 'PUT'){
    options.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };

    if(params){
		  options.body = JSON.stringify(params);
    }
    
  }else if(method == 'GET'){
    if(params) {
        api += (api.indexOf('?') === -1 ? '?' : '&') + buildQueryString(params);
    }
  }

  options.mode = 'cors';
  options.cache = 'no-cache';

  return fetch(BASE+APIBASE+api, options)
    .then(dataIfOk)
}



export function fetchApiCustomBody(api, method, body)
{
	let options = {
		method: method? method : "POST",
	}

  if(options.credentials == null) {
    options.credentials = 'include';
  }

  if(method == 'POST' || method == 'PUT'){
    options.headers = {
      'Accept': 'application/json',
      // 'Content-Type': 'application/json'
    };
  }

  options.body = body

  options.mode = 'cors';
  options.cache = 'no-cache';

  return fetch(BASE+APIBASE+api, options)
    .then(dataIfOk)
}