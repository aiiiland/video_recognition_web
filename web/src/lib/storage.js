import _isObject from 'lodash/isObject'
import _map from 'lodash/map'

import * as localForage from "localforage";

var localForageStore = localForage.createInstance({
    name: "app"
});

function makeStorageMethods(storageEngine){

    function getItem(key){
        return storageEngine.getItem(key);
    }

    function setItem(key, value){
        return storageEngine.setItem(key, value);
    }
    function removeItem(key){
        return storageEngine.removeItem(key);
    }

    function clear(){
        storageEngine.clear();
    }
    
    return {
        setItem,
        getItem,
        removeItem,
        clear,
        
    }
}



export const { getItem: localGet, setItem: localSet, clear: localClear } = makeStorageMethods(localStorage);
export const { getItem: sessionGet, setItem: sessionSet, clear: sessionClear } = makeStorageMethods(sessionStorage);

const storage = makeStorageMethods(localForageStore)
export default storage