module.exports = function(grunt) {

	require("matchdep").filter("grunt-*").forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		copy: {

			react: {
				files: [
					{expand: true, flatten: true, src: ['build/static/css/*'], dest: '../system/static/css/', filter: 'isFile'},
					{expand: true, flatten: true, src: ['build/static/js/*'], dest: '../system/static/js/', filter: 'isFile'},
					{expand: true, flatten: true, src: ['build/static/media/*'], dest: '../system/static/media/', filter: 'isFile'}
				]
			}
		},



	    watch: {
		  configFiles: {
			files: [ 'gruntfile.js', 'config/*.js' ],
			options: {
				reload: true
			}
		  },

		  reactApp:{
			files: [
				'src/**/*.js',
				'src/**/*.css',
			],
			tasks: ['reactBuild', 'clean:npmDebugLogs'],
			options:{
				interrupt: true,
				debounceDelay: 250,
			} 
		  }
		},

		shell: {
	        hello: {
	            command: 'echo Hello'
			},
			
			reactBuild:{
				command: [
					'npm run build',
				].join('&&')
			},

			reactProdBuild:{
				command: [
					'npm run build',
				].join('&&')
			},
	    },

	    clean: {
			
			npmDebugLogs: [
				'npm-debug.log*',
				'npm-debug.log.*'
			]
        }
        

    });


	grunt.registerTask('reactBuild', ['shell:reactBuild', 'copy:react']);
    grunt.registerTask('reactProdBuild', ['shell:reactProdBuild', 'copy:react']);
    
    grunt.registerTask('default', ['reactBuild'])

	

};
