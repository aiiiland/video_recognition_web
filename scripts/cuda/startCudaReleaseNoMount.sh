#!/bin/bash
# For using GPU accelerated image

REPO_NAME="aiiiland/video_recognition_web"

nvidia-docker run -it \
    --name aiiiland \
    -p 5000:5000 \
    $REPO_NAME:app-cuda \
    $@