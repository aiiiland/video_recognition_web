#!/bin/bash
# For using GPU accelerated image

REPO_NAME="aiiiland/video_recognition_web"

nvidia-docker run --rm --name aiiiland \
    -p 5000:5000 \
    -v $(pwd):/host \
    -v $(pwd)/system/training-images:/host/system/training-images \
    -v $(pwd)/system/generated-embeddings:/host/system/generated-embeddings \
    --env PYTHONUNBUFFERED=0 \
    $REPO_NAME:app-cuda \
    $@