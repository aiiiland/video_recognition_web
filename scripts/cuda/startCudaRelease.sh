#!/bin/bash
# For using GPU accelerated image

REPO_NAME="aiiiland/video_recognition_web"

nvidia-docker run -it \
    --name aiiiland \
    -p 5000:5000 \
    -v storage:/host/storage \
    -v trainingImages:/host/system/training-images \
    -v generatedEmbeddings:/host/system/generated-embeddings \
    $REPO_NAME:app-cuda \
    python3 WebApp.py --cuda