#!/bin/bash

REPO_NAME="aiiiland/video_recognition_web"

nvidia-docker build \
    -t $REPO_NAME:base-cuda \
    ./aiiiland-cuda-base/