#!/bin/bash

set -e

REPO_NAME="aiiiland/video_recognition_web"

echo "Building release build of the app with CUDA support..."

nvidia-docker build \
    -t $REPO_NAME:app-cuda \
    -f CudaRelease.Dockerfile .


echo "Build completed with tag $REPO_NAME:app-cuda"
echo "To publish, run:"
echo "    docker push $REPO_NAME:app-cuda"