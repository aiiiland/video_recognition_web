#!/bin/bash
# For using GPU accelerated image

REPO_NAME="aiiiland/video_recognition_web"

nvidia-docker run --rm --name aiiiland \
    -p 5000:5000 \
    -v $(pwd):/host \
    -v $(pwd)/storage:/host/storage \
    --env PYTHONUNBUFFERED=0 \
    $REPO_NAME:app-cuda \
    python3 WebApp.py --cuda