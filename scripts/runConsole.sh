#!/bin/bash

echo "Mounting " $(pwd) " to /host"

docker run -it --rm \
    -v $(pwd):/host \
    -v $(pwd)/storage:/host/storage \
    -p 5000:5000 \
    aiiiland/video_recognition_web:app \
    $@