#!/bin/bash
# Install brew taps that support relay rtsp source to remote server

brew install gstreamer gst-plugins-base gst-plugins-good ffmpeg
brew install gst-plugins-bad --with-rtmpdump
brew install gst-plugins-ugly --with-x264