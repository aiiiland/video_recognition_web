#!/bin/bash
# Push a local RTSP source to the cloud server

gst-launch-1.0 rtspsrc location=$1 ! decodebin ! \
    x264enc bitrate=256 tune=zerolatency  ! h264parse ! flvmux name=mux streamable=true ! \
    queue ! rtmpsink location='rtmp://172.105.223.92/live/office'