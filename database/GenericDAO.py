import logging
import time

# import sqlite3
from pypika import Query, Table, Field, Order, functions as fn
import RootDB


logger = logging.getLogger(__name__)


class GenericDAO(object):
    """ Data access object Generic """
    
    def __init__(self, tableName = ""):
        self.rootDB = RootDB.RootDB()
        self.TABLE = Table(tableName)
        self.selectFields = ['id', 'created_at']
        self.insertFields = ['id', 'created_at']

    def provideSeedData(self):
        return []

    def seed(self):
        """ Seeding function """
        dataArray = self.provideSeedData()
        for data in dataArray:
            self.rootDB.insertSql(Query.into(self.TABLE).insert(data).get_sql())

    def countAll(self):
        query = Query.from_(self.TABLE).select(fn.Count("*"))

        result = self.rootDB.selectOneSql(query.get_sql())
        
        return result[0]

    def getAll(self, limit = False, offset = False, orderby = False):
        
        query = Query.from_(self.TABLE).select(*self.selectFields)

        if offset:
            query = query.offset(offset)

        if limit:
            query = query.limit(limit)
        
        if orderby:
            field, direction = orderby
            orderDirection = None
            if direction == "desc":
                orderDirection = Order.desc
            else:
                orderDirection = Order.asc
            query = query.orderby(field, order=orderDirection)

        result = self.rootDB.selectSql(query.get_sql())
        transformer = lambda item : dict(zip(self.selectFields, item))
        return list(map(transformer, result))


    def get(self, id):
        query = Query.from_(self.TABLE).select(*self.selectFields).where(self.TABLE.id == id)

        result = self.rootDB.selectOneSql(query.get_sql())
        return dict(zip(self.selectFields, result))


    def add(self, dataDict):
        now = time.time()

        values = []
        for key in self.insertFields:
            if(key == 'created_at' or key == 'updated_at'):
                values.append(now)
            else:
                values.append(dataDict[key])

        query = Query.into(self.TABLE).columns(*self.insertFields).insert(*values)
        insertId = self.rootDB.insertSql(query.get_sql())
        return self.get(insertId)


    def update(self, id, dataDict):
        now = time.time()
        query = Query.update(self.TABLE).where(self.TABLE.id == id)

        for key, value in dataDict.items():
            query = query.set(key, value)

        query = query.set('updated_at', now)

        self.rootDB.executeSql(query.get_sql())
        return True


    def remove(self, id):
        query = Query.from_(self.TABLE).where(self.TABLE.id == id).delete()
        self.rootDB.executeSql(query.get_sql())
        return True
