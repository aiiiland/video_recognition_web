import os
import sqlite3
from sqlite3 import OperationalError
import logging

mypath = os.path.realpath(__file__)
projectRoot = os.path.dirname(os.path.dirname(mypath))
DATABASE_PATH = projectRoot + "/database"
MIGRATIONS_PATH = os.path.join(DATABASE_PATH, "migrations")
STORAGE_PATH = projectRoot + "/storage"


logger = logging.getLogger(__name__)


#### Migration function
def initAndMigrateDB(conn):
    """ This function needs to be maintained everytime the DB changes """
    if not isDatabaseExist(conn):
        print("Database is new, initializing ...")
        runMigrationFile(conn, "0_create.sql")
        # print("Tables created ...")
    elif isFirstMigration(conn):
        runMigrationFile(conn, "1_add_surveillance_logs.sql")
    else:
        version = getMigrationVersion(conn)
        # if version < 2:
            # future migration add here
            # runMigrationFile(conn, "2_xxxx.sql")



#### Migrations test functions
def isDatabaseExist(conn):
    init = False

    cur = conn.cursor()
    try:
        cur.execute("SELECT * FROM settings")
        init = True
    except OperationalError as msg:
        init = False

    return init
        

def isFirstMigration(conn):
    cur = conn.cursor()
    resultSet = cur.execute("SELECT count(*) FROM settings where skey = 'migration_version' ")
    test = resultSet.fetchone()[0] == 0
    cur.close()

    return test

def getMigrationVersion(conn):
    cur = conn.cursor()
    resultSet = cur.execute("SELECT svalue FROM settings where skey = 'migration_version' ")
    version = resultSet.fetchone()[0]
    cur.close()
    return version


##### Other helper functions
def open_db(dbname = STORAGE_PATH + '/database.db'):
    conn = sqlite3.connect(dbname)
    return conn

def readSQLFromFile(filename):
    # Open and read the file as a single buffer
    fd = open(filename, 'r')
    sqlFile = fd.read()
    fd.close()
    return sqlFile


def runMigrationFile(conn, file):
    cursor = conn.cursor()

    logger.info("executing {} ...".format(file))

    path = os.path.join(MIGRATIONS_PATH, file)
    createSQL = readSQLFromFile(path)

    sqlCommands = createSQL.split(';')
    # Execute every command from the input file
    for command in sqlCommands:
        try:
            cursor.execute(command)
        except OperationalError as msg:
            print(command)
            print("Command skipped: ", msg)
    
    conn.commit()
    cursor.close()



#### Main Class
class RootDB(object):

    def __init__(self, checkInit = False):
        # nothing
        self.conn = conn = open_db()
        if checkInit:
            initAndMigrateDB(conn)
    
    def insertSql(self, command):
        cur = self.conn.cursor()
        try:
            cur.execute(command)

            insertId = cur.lastrowid
            
            self.conn.commit()
            cur.close()

            return insertId
            
        except OperationalError as msg:
            print(command)
            print("Error: ", msg)

        return False

    def executeSql(self, command):
        cur = self.conn.cursor()
        try:
            cur.execute(command)
            
            self.conn.commit()
            cur.close()
            return True
        except OperationalError as msg:
            print(command)
            print("Error: ", msg)

        return False

    def selectOneSql(self, command):
        cur = self.conn.cursor()
        try:
            resultSet = cur.execute(command)
            result = resultSet.fetchone()
            cur.close()
            return result
        except OperationalError as msg:
            print(command)
            print("Error: ", msg)

        return False

    def selectSql(self, command):
        cur = self.conn.cursor()
        try:
            resultSet = cur.execute(command)
            result = resultSet.fetchall()
            cur.close()
            return result
        except OperationalError as msg:
            print(command)
            print("Error: ", msg)

        return False



if __name__ == '__main__':
    print(STORAGE_PATH)
    print(DATABASE_PATH)

    conn = open_db()
    print("Opened database successfully")

    if isDatabaseExist(conn):
        print("Database already initialized")
    else:
        print("Database is new, initializing ...")
        runMigrationFile(conn, "0_create.sql")
        print("Tables created ...")
