
CREATE TABLE IF NOT EXISTS settings (
 skey text PRIMARY KEY,
 svalue text NULL
);


CREATE TABLE IF NOT EXISTS cameras (
 id integer PRIMARY KEY,
 label text NOT NULL,
 method text NOT NULL,
 url text NOT NULL,
 created_at timestamp,
 updated_at timestamp
);

CREATE TABLE IF NOT EXISTS people (
 id integer PRIMARY KEY,
 name text NOT NULL,
 key text NOT NULL,
 created_at timestamp,
 updated_at timestamp
);


CREATE TABLE IF NOT EXISTS events (
 id integer PRIMARY KEY,
 level text NOT NULL,
 name text NOT NULL,
 data text NOT NULL,
 created_at timestamp,
 updated_at timestamp
);