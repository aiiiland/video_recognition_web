INSERT INTO settings VALUES ('migration_version', 1);

CREATE TABLE IF NOT EXISTS surveillance_logs (
 id integer PRIMARY KEY,
 identity text NOT NULL,
 photo text NOT NULL,
 confidence integer NOT NULL,
 log_time timestamp,
 
 camera_id integer NOT NULL,
 camera_name text NOT NULL,

 user_flag text NULL,

 created_at timestamp,
 updated_at timestamp
);