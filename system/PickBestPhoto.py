import os
import sys

import logging
import numpy as np
import math
import face_recognition
import AppEnv


logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


args = AppEnv.getArgs()

def getOneFaceEncoding(image):

    face_locations = []
    if args.cuda:
        face_locations = face_recognition.face_locations(image, number_of_times_to_upsample=1, model="cnn")
    else:
        face_locations = face_recognition.face_locations(image)

    faceEncoding = face_recognition.face_encodings(image, face_locations)
    if faceEncoding is not None and len(faceEncoding) > 0:
        return faceEncoding[0]
    else:
        return None

def getPos(landmarks):

    nose_points = np.array(landmarks["nose_tip"])
    leftEye_points = np.array(landmarks["left_eye"])
    rightEye_points = np.array(landmarks["right_eye"])

    #print(nose_points[2])
    #print(leftEye_points[0])
    #print(rightEye_points[3])

    leftSide = math.sqrt((leftEye_points[0][0] - nose_points[2][0])**2 + (leftEye_points[0][1] - nose_points[2][1])**2)
    rightSide = math.sqrt((rightEye_points[3][0] - nose_points[2][0])**2 + (rightEye_points[3][1] - nose_points[2][1])**2)

    print(leftSide)
    print(rightSide)
    
    if (leftSide / rightSide) > 1.3:
        return "Right"
    elif (rightSide / leftSide) > 1.3:
        return "Left"
    return "Center"

def pickBestPhotoIn(dir, position):

    logger.info(f"Finding best photo from {dir}")

    photos = os.listdir(dir)
    photos = list(filter(lambda item : item != ".DS_Store", photos))

    # quick return
    # if len(photos) == 1:
    #     return (photos[0], 0, getOneFaceEncoding(photos[0]))

    logger.info(f"Reading faces from the photos ...")
    faceEncodingCache = dict()
    for photoPath in photos:
        
        image = face_recognition.load_image_file(os.path.join(dir, photoPath))
        face_landmarks_list = face_recognition.face_landmarks(image)
        if face_landmarks_list is not None and len(face_landmarks_list) > 0:
            face_landmarks = face_landmarks_list[0]
        else:
            continue

        #print(face_landmarks)
        print(getPos(face_landmarks))
        if getPos(face_landmarks) != position:
            continue

        faceEncoding = getOneFaceEncoding(image)

        if faceEncoding is None:
            continue

        faceEncodingCache[photoPath] = faceEncoding

    logger.info(f"Face encoding cache loaded")

    # resultList = []
    thePhoto = None
    theFaceEncoding = None
    minAvgDistance = 9999
    for photoPath in photos:
        
        faceEncoding = faceEncodingCache.get(photoPath)
        if faceEncoding is None:
            continue

        distanceTotal = 0
        num = 0
        for otherPhotoPath in photos:
            if otherPhotoPath != photoPath:
                otherFaceEncoding = faceEncodingCache.get(otherPhotoPath)

                if otherFaceEncoding is None:
                    distanceTotal = distanceTotal + 1
                    num = num + 1
                    continue

                distances = face_recognition.face_distance([faceEncoding], otherFaceEncoding)
                # logger.debug(f"distances {distances}")
                if distances is not None and len(distances) > 0:
                    d = distances[0]
                    distanceTotal = distanceTotal + d
                    num = num + 1
                else:
                    distanceTotal = distanceTotal + 1
                    num = num + 1

        # logger.debug(f"distanceTotal {distanceTotal}")
        # logger.debug(f"num {num}")

        avgDistance = 0
        if num > 0:
            avgDistance = distanceTotal / num
        else:
            avgDistance = 0
        if avgDistance < minAvgDistance:
            minAvgDistance = avgDistance
            thePhoto = photoPath
            theFaceEncoding = faceEncoding
        # resultList.append((photoPath, avgDistance))

    return (thePhoto, minAvgDistance, theFaceEncoding)





def main():
    print("")
    print("##############################################################")
    print(" PickBestPhoto.py ")
    print("#############################################################")
    print("")

    logger.setLevel(logging.DEBUG)

    staffsDir = os.path.join(AppEnv.storageDir, "test-images/staffs")
    staffs = os.listdir(staffsDir)
    staffs = filter(lambda item : item != ".DS_Store", staffs)

    for staff in staffs:
        (photo, avgDistance, encoding) = pickBestPhotoIn(os.path.join(staffsDir, staff))
        print(f"Best Photo of {staff} is {photo} with avg distance of {avgDistance}")





if __name__ == "__main__":
    main()

