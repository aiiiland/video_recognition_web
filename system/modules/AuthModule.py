from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import json

from MyUtils import json_encode



def checkLogin(form):
    if form['username'] != 'admin' or form['password'] != 'admin':
        return False
    else:
        return True


def initRoutes(app):

    @app.route('/login', methods=['POST'])
    def login():
        if request.method == 'POST':
            app.logger.info("In Login post")
            form = request.get_json()
            app.logger.info(form)

            session.pop('user',None) # Drops session everytime user tries to login
            if not checkLogin(form):
                app.logger.info("Invalid username or password")
            else:
                session['user'] = form['username']
                return Response(
                    json_encode({'msg': 'OK'}),
                    status = 200,
                    mimetype='application/json'
                )
        # default error
        return Response(
            json_encode({'msg': 'Invalid username or password', 'error': True}),
            status = 400,
            mimetype='application/json'
        )

    @app.route('/logout', methods=['POST'])
    def logout():
        if g.user is not None:
            session.pop('user',None)
            g.user = None

        return Response(
                json_encode({
                    'msg': 'OK',
                }),
                status = 200,
                mimetype='application/json'
            )


    @app.route('/session', methods=['POST'])
    def getSession():
        if g.user is not None:
            return Response(
                    json_encode({
                        'msg': 'OK',
                        'username': g.user
                    }),
                    status = 200,
                    mimetype='application/json'
                )
        else:
            return Response(
                json_encode({'msg': 'Not login', 'error': True}),
                status = 400,
                mimetype='application/json'
            )