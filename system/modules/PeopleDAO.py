import logging
import time

import RootDB
import GenericDAO

logger = logging.getLogger(__name__)


class PeopleDAO(GenericDAO.GenericDAO):
    """ Data access object for "People" object """

    pass

    def __init__(self):
        super().__init__("people")

        self.selectFields = ['id', 'name', 'key', 'created_at', 'updated_at']
        self.insertFields = ['name', 'key', 'created_at']


    def provideSeedData(self):
        now = time.time()

        data = []
        data.append([1, 'Andy Lau', 'AndyLau', now, now])
        data.append([2, 'Aaron Kwok', 'AaronKwok', now, now])

        return data
