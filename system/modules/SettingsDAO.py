import logging
import time

import RootDB
# import sqlite3
from pypika import Query, Table, Field
from pypika.terms import Values

logger = logging.getLogger(__name__)


class SettingsDAO(object):
    """ Data access object for "Settings" """

    pass

    def __init__(self):

        self.rootDB = RootDB.RootDB()
        self.TABLE = Table("settings")

        self.keyField = "skey"
        self.valueField = "svalue"
        self.selectFields = ["skey", "svalue"]
        self.insertFields = ["skey", "svalue"]


    def provideSeedData(self):
        data = []
        data.append(['notification_email', 'videorecoga+example@gmail.com'])
        return data

    def getAll(self):
        
        query = Query.from_(self.TABLE).select(*self.selectFields)

        result = self.rootDB.selectSql(query.get_sql())
        dataDict = {}
        for key, value in result:
            dataDict[key] = value

        return dataDict

    def get(self, key, defaultValue = None):
        query = Query.from_(self.TABLE).select(*self.selectFields).where(self.TABLE.skey == key)

        result = self.rootDB.selectOneSql(query.get_sql())

        if result is None:
            return None
        else:
            logger.info("result = {}".format(result))

            resultDict = dict(zip(self.selectFields, result))

            return resultDict.get(self.valueField, defaultValue)


    def put(self, key, value):

        search = self.get(key)

        if search is None:
            query = Query.into(self.TABLE) \
                    .columns(*self.insertFields) \
                    .insert(key, value)
            self.rootDB.insertSql(query.get_sql())
            return True
        else:
            query = Query.update(self.TABLE).where(self.TABLE.skey == key)
            query = query.set(self.valueField, value)

            self.rootDB.executeSql(query.get_sql())
            return True