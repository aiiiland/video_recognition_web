import json

# Custom JSON encoder
class MyEncoder(json.JSONEncoder):  
    def default(self, obj):  
        if isinstance(obj, bytes):  
            return str(obj, encoding='utf-8');  
        return json.JSONEncoder.default(self, obj)


def json_encode(data):
    return json.dumps(data, cls=MyEncoder),