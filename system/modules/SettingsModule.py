from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import json
import time

import SettingsDAO

def init(app, socketio, HomeSurveillance):
    initRoutes(app)


def initRoutes(app):

    @app.route('/settings/save', methods = ['POST'])
    def endpoint_settings_save():
        form = request.get_json()

        dao = SettingsDAO.SettingsDAO()

        for key, value in form.items():
            dao.put(key, value)

        return jsonify({
            'msg': 'OK'
        })


    @app.route('/settings/get_all', methods = ['POST'])
    def endpoint_settings_get_all():
        form = request.get_json()

        dao = SettingsDAO.SettingsDAO()
        items = dao.getAll()

        return jsonify({
            'items': items
        })


    @app.route('/settings/get', methods = ['POST'])
    def endpoint_settings_get():
        form = request.get_json()

        dao = SettingsDAO.SettingsDAO()
        value = dao.get(form.get("key"))

        return jsonify({
            'key': form.get("key"),
            'value': value
        })

    @app.route('/settings/algorithms', methods = ['GET'])
    def get_algorithms_list():
        return jsonify({
            "algorithms": {
                "detect_motion": "Motion Detection",
                "detect_recognise": "Face Recognition",
                "motion_detect_recognise": "Motion Detection & Face Recognition",
                "segment_detect_recognise": "Motion Object Segmentation & Face Recognition",
                "detect_recognise_track": "Face Recognition & Tracking",
                "openface": "Openface",
                "face_capture": "Face capture",
                "face_detection_embedding": "cnn embedding face detection",
                "face_capture_embedding": "cnn embedding Face capture"
            }
        })