from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import os
import logging
import shutil
import cv2

import PeopleDAO
import AppEnv

logger = logging.getLogger(__name__)


_, jpegUnknownPerson = cv2.imencode('.jpg', cv2.imread(AppEnv.pathJoin(AppEnv.systemDir, "static/img/speaker-placeholder.png")))
UNKNOW_PERSON_IMG = jpegUnknownPerson.tostring()



def init(app, socketio, HomeSurveillance):
    checkPeople()

    initRoutes(app)

def initRoutes(app):

    @app.route('/people', methods = ['GET'])
    def get_people_list():
        dao = PeopleDAO.PeopleDAO()
        people = dao.getAll()

        return jsonify({
            'items': people
        })

    @app.route('/people/thumbnail/<key>', methods = ['GET'])
    def get_people_thumbnail(key):
        path = getPeopleAlignedImageDir(key)
        images = getAlignedImagesByKey(key)

        image = None
        
        if len(images) > 0:
            imagePath = os.path.join(path, images[0])
            image = cv2.imread(imagePath)
            _, jpeg = cv2.imencode('.jpg', image)
            image = jpeg.tostring()
        else:
            image = UNKNOW_PERSON_IMG

        resp = Response(image, mimetype="image/jpeg")
        return resp
    
    @app.route('/people/alignedimages/<key>', methods = ['GET','POST'])
    def get_people_aligned_images_list(key):
        images = getAlignedImagesByKey(key)

        return jsonify({
            'items': images
        })

    @app.route('/people/images/<key>', methods = ['GET','POST'])
    def get_people_images_list(key):
        images = getTrainingImagesByKey(key)

        return jsonify({
            'items': images
        })
    
    @app.route('/people/img/<key>/<filename>', methods = ['GET'])
    def get_people_image(key, filename):
        path = getPeopleTrainingImageDir(key)
        imagePath = os.path.join(path, filename)

        image = None
        if os.path.exists(imagePath):
            image = cv2.imread(imagePath)
            _, jpeg = cv2.imencode('.jpg', image)
            image = jpeg.tostring()
        else:
            image = UNKNOW_PERSON_IMG

        resp = Response(image, mimetype="image/jpeg")
        return resp

    @app.route('/people/alignimg/<key>/<filename>', methods = ['GET'])
    def get_people_align_image(key, filename):
        path = getPeopleAlignedImageDir(key)
        imagePath = os.path.join(path, filename)

        image = None
        if os.path.exists(imagePath):
            image = cv2.imread(imagePath)
            _, jpeg = cv2.imencode('.jpg', image)
            image = jpeg.tostring()
        else:
            image = UNKNOW_PERSON_IMG

        resp = Response(image, mimetype="image/jpeg")
        return resp

    @app.route('/people/add', methods = ['POST'])
    def people_add():
        form = request.get_json()

        dao = PeopleDAO.PeopleDAO()
        people = dao.add(form)

        return jsonify({
            'item': people
        })

    @app.route('/people/remove/<id>', methods = ['POST'])
    def people_remove(id):

        dao = PeopleDAO.PeopleDAO()
        person = dao.get(id)
        dao.remove(id)

        
        trainingDir = getPeopleTrainingImageDir(person['key'])
        alignDir = getPeopleAlignedImageDir(person['key'])
        shutil.rmtree(trainingDir)
        shutil.rmtree(alignDir)
        # os.unlink(trainingDir)
        # os.unlink(alignDir)

        return jsonify({
            'msg': 'OK'
        })


    @app.route('/people/seed', methods = ['GET'])
    def seed_people():
        dao = PeopleDAO.PeopleDAO()
        dao.seed()

        return jsonify({
            'msg': 'OK'
        })



def checkPeople():
    dao = PeopleDAO.PeopleDAO()
    people = dao.getAll()

    for ppl in people:
        images = getTrainingImagesByKey(ppl.get('key'), True)
        logger.info("person = {}, images= {}".format(ppl.get('key'), images))



def getPeopleTrainingImageDir(key):
    # path = AppEnv.trainingImagesDir
    # logger.info( "path = {}".format(path+key) )
    
    return AppEnv.pathJoin(AppEnv.trainingImagesDir, key)

def getPeopleAlignedImageDir(key):
    # path = AppEnv.alignedImagesDir
    # logger.info( "path = {}".format(path+key) )
    # return path + key
    return AppEnv.pathJoin(AppEnv.alignedImagesDir, key)


def getTrainingImagesByKey(key, autoInit=False):
    path = getPeopleTrainingImageDir(key)
    
    images = []
    if not os.path.exists(path):
        if autoInit:
            os.makedirs(path)
        images = []
    else:
        images = os.listdir(path)

    # logger.info("getTrainingImagesByKey, images= {}".format(images))

    return images

def getAlignedImagesByKey(key, autoInit=False):
    path = getPeopleAlignedImageDir(key)
    
    images = []
    if not os.path.exists(path):
        if autoInit:
            os.makedirs(path)
        images = []
    else:
        images = os.listdir(path)

    return images
    