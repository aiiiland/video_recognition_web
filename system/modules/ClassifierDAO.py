import os
import pickle

from operator import itemgetter
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder

import AppEnv


def createDummyClassifierIfNotFound():
    if not os.path.exists(AppEnv.classifierPath):
        createDummyClassifier()

def createDummyClassifier():
    labels = "1:aligned-images/dummy/1.png"
    labels = list(map(itemgetter(1),list(map(os.path.split,list(map(os.path.dirname, labels))))))
    labelsObject = LabelEncoder().fit(labels)

    classifier = SVC(C=1, kernel='linear', probability=True)

    with open(AppEnv.classifierPath, 'wb') as f:
        pickle.dump((labelsObject, classifier), f)