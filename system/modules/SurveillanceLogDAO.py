import logging
import time

import RootDB
import GenericDAO

logger = logging.getLogger(__name__)


class SurveillanceLogDAO(GenericDAO.GenericDAO):
    """ Data access object for "SurveillanceLog" object """

    pass

    def __init__(self):
        super().__init__("surveillance_logs")

        self.selectFields = ['id', 'identity', 'photo', 'confidence', 'log_time', 'camera_id', 'camera_name', 'user_flag', 'created_at', 'updated_at']
        self.insertFields = ['identity', 'photo', 'confidence', 'log_time', 'camera_id', 'camera_name', 'user_flag', 'created_at']


    def provideSeedData(self):
        # now = time.time()
        data = []
        return data




def insert_log(identity, photo, confidence, log_time, camera_id, camera_name, user_flag = None):
    """ Convenience method to insert log """
    
    instance = SurveillanceLogDAO()
    instance.add({
        'identity': identity,
        'photo': photo,
        'confidence': confidence,
        'log_time': log_time,
        'camera_id': camera_id,
        'camera_name': camera_name,
        'user_flag': user_flag,
    })