from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import json
import time
import cv2
# from shutil import copyfile
import shutil
import os

import SurveillanceLogDAO
import PeopleDAO
import AppEnv

from PeopleModule import getPeopleTrainingImageDir


def init(app, socketio, HomeSurveillance):
    initRoutes(app)


def initRoutes(app):


    @app.route('/surveillance_logs/get_all', methods = ['GET','POST'])
    def endpoint_surveillance_logs_get_all():
        form = request.get_json()
        if form is None:
            form = dict()

        offset = form.get("offset", 0)
        limit = form.get("limit", 100)

        dao = SurveillanceLogDAO.SurveillanceLogDAO()
        items = dao.getAll(offset = offset, limit = limit, orderby= ("id", "desc"))

        total = dao.countAll()

        return jsonify({
            'items': items,
            'total': total
        })
    
    @app.route('/logs_img/<name>', methods = ['GET'])
    def get_log_image(name):
        # form = request.get_json()

        path = AppEnv.pathJoin(AppEnv.logImagesDir, name)
        _, jpeg = cv2.imencode('.jpg', cv2.imread(path))

        resp = Response(jpeg.tostring(), mimetype="image/jpeg")
        return resp


    @app.route('/surveillance_logs/<log_id>/tag', methods = ['GET', 'POST'])
    def endpoint_surveillance_logs_tag_people(log_id):
        """ Tag the log item to a certain people """

        form = request.get_json()
    
        dao = SurveillanceLogDAO.SurveillanceLogDAO()
        logItem = dao.get(log_id)

        # find the image
        filename = logItem.get("photo")
        imagePath = AppEnv.pathJoin(AppEnv.logImagesDir, filename)
        # _, jpeg = cv2.imencode('.jpg', cv2.imread(path))

        # find the person
        personDAO = PeopleDAO.PeopleDAO()
        person = personDAO.get(form.get("person_id"))

        personTrainingImageDir = getPeopleTrainingImageDir(person.get("key"))
        if not os.path.exists(personTrainingImageDir):
            os.makedirs(personTrainingImageDir)

        # copy to the training dir
        shutil.copy(imagePath, personTrainingImageDir)

        # update logItem user_flag
        dao.update(log_id, {
            'user_flag': person.get("key")
        })

        return jsonify({
            'msg': 'OK'
        })


    @app.route('/surveillance_logs/<log_id>/untag', methods = ['GET', 'POST'])
    def endpoint_surveillance_logs_untag_people(log_id):
        """ UnTag  """

        form = request.get_json()
    
        dao = SurveillanceLogDAO.SurveillanceLogDAO()
        logItem = dao.get(log_id)

        # find the image
        filename = logItem.get("photo")
        # imagePath = AppEnv.pathJoin(AppEnv.logImagesDir, filename)

        personTrainingImageDir = getPeopleTrainingImageDir(logItem.get("user_flag"))
        if os.path.exists(personTrainingImageDir):
            theImagePath = os.path.join(personTrainingImageDir, filename)
            if os.path.exists(theImagePath):
                os.remove(theImagePath)

            # update logItem user_flag
            dao.update(log_id, {
                'user_flag': ""
            })

            return jsonify({
                'msg': 'OK'
            })
        else:

            # update logItem user_flag
            dao.update(log_id, {
                'user_flag': ""
            })

            return jsonify({
                'msg': 'OK, but Tagged person not found'
            })

