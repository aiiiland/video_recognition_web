from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import json
import os
import time
import psutil
import multiprocessing



# if not monitoringThread.isAlive():
    #     #print "Starting monitoringThread"
    #     app.logger.info("Starting monitoringThread")
    #     monitoringThread = threading.Thread(name='monitoring_process_thread_',target= system_monitoring, args=())
    #     monitoringThread.start()

def init(app, socketio, HomeSurveillance):

    def system_monitoring():
        """Pushes system monitoring data to client"""
        while True:
            cameraProcessingFPS = []
            cameraStreamingFPS = []
            for camera in HomeSurveillance.cameras:
                cameraStreamingFPS.append("{0:.2f}".format(camera.streamingFPS))
                cameraProcessingFPS.append("{0:.2f}".format(camera.processingFPS))
                # print("FPS: " + str(camera.processingFPS) + " " + str(camera.streamingFPS))
                # app.logger.info("FPS: " +str(camera.processingFPS) + " " + str(camera.streamingFPS))
            systemState = {
                'cpu':cpu_usage(),
                'memory':memory_usage(),
                'processingFPS': cameraProcessingFPS,
                'streamingFPS': cameraStreamingFPS
            }
            socketio.emit('system_monitoring', json.dumps(systemState) ,namespace='/surveillance')
            time.sleep(3)

    def cpu_usage():
        psutil.cpu_percent(interval=1, percpu=False) #ignore first call - often returns 0
        time.sleep(0.12)
        cpu_load = psutil.cpu_percent(interval=1, percpu=False)
        #print "CPU Load: " + str(cpu_load)
        app.logger.info("CPU Load: " + str(cpu_load))
        return cpu_load  

    def memory_usage():
        mem_usage = psutil.virtual_memory().percent
        #print "System Memory Usage: " + str( mem_usage)
        app.logger.info("System Memory Usage: " + str( mem_usage))
        return mem_usage 

    return {
        system_monitoring,
        cpu_usage,
        memory_usage
    }


def initRoutes(app):
    # do nothing
    return 0




def system_info(app):
    print("CPU Count: " + str( multiprocessing.cpu_count() ) )
    print("CPU Core Count: " + str( psutil.cpu_count(logical=True) ) )
    
    # app.logger.info("CPU Cores: " + str( len(os.sched_getaffinity(0)) ))