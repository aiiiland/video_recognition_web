from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g

# import SurveillanceSystem
import Camera

import CameraDAO

def init(app, socketio, HomeSurveillance):
    initRoutes(app)
    restoreCameras(HomeSurveillance)


def restoreCameras(HomeSurveillance):
    dao = CameraDAO.CameraDAO()
    cameras = dao.getAll()

    # if len(cameras) > 0:
    #     with HomeSurveillance.camerasLock :
    #         for camera in cameras:
    #             HomeSurveillance.add_camera(
    #                 Camera.IPCamera(
    #                     camera.get("id"),
    #                     camera.get("url"),
    #                     camera.get("method"),
    #                     False, #opencv
    #                     False,
    #                     camera.get("label")
    #                 ))




def initRoutes(app):

    @app.route('/camera', methods = ['GET'])
    def get_camera_list():
        dao = CameraDAO.CameraDAO()
        items = dao.getAll()

        return jsonify({
            'items': items
        })

    @app.route('/camera/add', methods = ['POST'])
    def camera_add():
        form = request.get_json()

        dao = CameraDAO.CameraDAO()
        item = dao.add(form)

        return jsonify({
            'item': item
        })

    @app.route('/camera/remove/<id>', methods = ['POST'])
    def camera_remove(id):

        dao = CameraDAO.CameraDAO()
        dao.remove(id)

        return jsonify({
            'msg': 'OK'
        })


    @app.route('/camera/seed', methods = ['GET'])
    def seed_camera():
        dao = CameraDAO.CameraDAO()
        dao.seed()

        return jsonify({
            'msg': 'OK'
        })
