import logging
import time
import string
import random
import cv2
import numpy as np

import AppEnv
import SurveillanceLogDAO

# from multiprocessing import Queue
import queue 

logger = logging.getLogger(__name__)


def getRandomString(N):
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(N))


def saveImageToLogDir(jpeg):
    # gen name
    rName = getRandomString(16) + ".jpg"
    path = AppEnv.pathJoin(AppEnv.logImagesDir, rName)

    nparr = np.fromstring(jpeg, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    cv2.imwrite(path, image)

    return rName


class SurveillanceLogger():
    """ Data access object for "SurveillanceLog" object """

    def __init__(self, queue):
        self.dao = SurveillanceLogDAO.SurveillanceLogDAO()
        self.queue = queue


    def loop(self):

        while True:
            try:

                payloadDict = self.queue.get(True, 5)

                logger.info("!!! surveillance log item found !!!")

                # save thumbnail to disk
                savedImagePath = saveImageToLogDir(payloadDict.get("thumbnail"))

                SurveillanceLogDAO.insert_log(payloadDict.get('identity'), savedImagePath, payloadDict.get('confidence'), payloadDict.get('time'), payloadDict.get('camera_id'), payloadDict.get('camera_name'))

            except queue.Empty:
                logger.debug("Logger get queue Timeout")
                time.sleep(1)
            except Exception as e:
                logger.error("Error Caught in SurveillanceLogger : {}".format(e))
                time.sleep(1)



def logger_process(queue):

    logger.info("Starting logger process ...")

    instance = SurveillanceLogger(queue)
    instance.loop()

    logger.info("Surveillance Logger process terminated")

# def insert_log(identity, photo, confidence, log_time, camera_id, camera_name, user_flag = None):
#     """ Convenience method to insert log """
    
#     instance = SurveillanceLogDAO()
#     instance.add({
#         'identity': identity,
#         'photo': photo,
#         'confidence': confidence,
#         'log_time': log_time,
#         'camera_id': camera_id,
#         'camera_name': camera_name,
#         'user_flag': user_flag,
#     })