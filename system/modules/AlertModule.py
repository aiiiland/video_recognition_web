from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
import json

import time
import psutil
import logging

import SurveillanceSystem


import CameraDAO
import SettingsDAO

from DataModels import Alert

# def alarm_state():
    #  """Used to push alarm state to client"""
    #  while True:
    #         alarmstatus = {'state': HomeSurveillance.alarmState , 'triggered': HomeSurveillance.alarmTriggerd }
    #         socketio.emit('alarm_status', json.dumps(alarmstatus) ,namespace='/surveillance')
    #         time.sleep(3)


# @socketio.on('alarm_state_change', namespace='/surveillance') 
# def alarm_state_change():   
#     HomeSurveillance.change_alarm_state()

logger = logging.getLogger(__name__)

class AlertModule(object):
    def __init__(self):
        self.personAlertStatus = dict({})

def init(app, socketio, HomeSurveillance):
    initSockets(socketio, HomeSurveillance)
    initRoutes(app, socketio, HomeSurveillance)


def initSockets(socketio, HomeSurveillance):
    @socketio.on('panic', namespace='/surveillance') 
    def panic(): 
        HomeSurveillance.trigger_alarm()

def initRoutes(app, socketio, HomeSurveillance):

    moduleInstance = AlertModule()

    @app.route('/create_alert', methods = ['GET','POST'])
    def endpoint_create_alert():
        if request.method == 'POST':

            form = request.get_json()
            if form is None:
                form = request.form

            create_alert(HomeSurveillance, request.form)

            data = {
                "alert_id": HomeSurveillance.alerts[-1].id,
                "alert_message": "Alert if " + HomeSurveillance.alerts[-1].alertString
            }
            return jsonify(data)

        return render_template('index.html')

    @app.route('/get_simple_alert_status', methods = ['POST'])
    def endpoint_get_simple_alert_status():
        form = request.get_json()
        if form is None:
            form = request.form

        person = form.get('person', None)

        if person is None:
            return jsonify({ 'msg': 'Error' })

        data = {
            'status': moduleInstance.personAlertStatus.get(person, False)
        }
        return jsonify(data)


    @app.route('/create_simple_alert', methods = ['POST'])
    def endpoint_create_simple_alert():
        if request.method == 'POST':

            form = request.get_json()
            if form is None:
                form = request.form

            person = form.get('person', None)

            if person is None:
                return jsonify({ 'msg': 'Error' })

            if moduleInstance.personAlertStatus.get(person, False) == True:
                return jsonify({ 'msg': 'Already enabled' })


            # cameraDao = CameraDAO.CameraDAO()
            # cameras = cameraDao.getAll()

            settingsDao = SettingsDAO.SettingsDAO()
            email = settingsDao.get("notification_email", None)

            if email is not None:

                create_simple_alert(HomeSurveillance, str(person), email)
                
                moduleInstance.personAlertStatus[person] = True

            data = {
                'msg': 'OK'
            }
            return jsonify(data)

    @app.route('/remove_simple_alert', methods = ['POST'])
    def endpoint_remove_simple_alert():
        form = request.get_json()
        if form is None:
            form = request.form

        person = form.get('person', None)

        if person is None:
            return jsonify({ 'msg': 'Error', 'error': True })

        remove_simple_alert(HomeSurveillance, str(person))

        moduleInstance.personAlertStatus[person] = False

        return jsonify({
                'msg': 'OK'
            })

    @app.route('/remove_alert', methods = ['GET','POST'])
    def remove_alert():
        if request.method == 'POST':
            alertID = request.form.get('alert_id')
            with HomeSurveillance.alertsLock:
                for i, alert in enumerate(HomeSurveillance.alerts):
                    if alert.id == alertID:
                        del HomeSurveillance.alerts[i]
                        break
            
            data = {"alert_status": "removed"}
            return jsonify(data)
        return render_template('index.html')



def create_simple_alert(HomeSurveillance, person, emailAddress):

    data = {
        'camera': 'All',
        'emailAddress': emailAddress,
        'eventdetail': 'Recognition',
        'alarmstate': 'All',
        'person': person,
        'push_alert': 'false',
        'email_alert': 'true',
        'trigger_alarm': 'false',
        'notify_police': 'false',
        'confidence': 85,
    }

    logger.info("Creating simple alert {} notify {}".format(person, emailAddress))

    return create_alert(HomeSurveillance, data)


def remove_simple_alert(HomeSurveillance, person):
    with HomeSurveillance.alertsLock:
        for i, alert in enumerate(HomeSurveillance.alerts):
            if str(alert.person) == str(person):
                del HomeSurveillance.alerts[i]

    return True


def create_alert(HomeSurveillance, params):

    camera = params.get('camera')
    emailAddress = params.get('emailAddress')
    event = params.get('eventdetail')
    alarmstate = params.get('alarmstate')
    person = params.get('person')
    push_alert = params.get('push_alert')
    email_alert = params.get('email_alert')
    trigger_alarm = params.get('trigger_alarm')
    notify_police = params.get('notify_police')
    confidence = params.get('confidence')

    actions = {'push_alert': push_alert , 'email_alert':email_alert , 'trigger_alarm':trigger_alarm , 'notify_police':notify_police}
    with HomeSurveillance.alertsLock:
        alertInstance = Alert(alarmstate,camera, event, person, actions, emailAddress, int(confidence))
        HomeSurveillance.alerts.append(alertInstance)

    return HomeSurveillance.alerts[-1].id