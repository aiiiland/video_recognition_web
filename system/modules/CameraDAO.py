import logging
import time

import RootDB
import GenericDAO

logger = logging.getLogger(__name__)


class CameraDAO(GenericDAO.GenericDAO):
    """ Data access object for "Camera" object """

    pass

    def __init__(self):
        super().__init__("cameras")

        self.selectFields = ['id', 'label', 'method', 'url', 'created_at', 'updated_at']
        self.insertFields = ['label', 'method', 'url', 'created_at']


    def provideSeedData(self):
        now = time.time()

        data = []
        data.append([1, 'Entrance', 'detect_recognise', 'rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov', now, now])

        return data
