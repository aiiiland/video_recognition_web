# FaceRecogniser.
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Code used in this project included opensource software (openface)
# developed by Brandon Amos
# Copyright 2015-2016 Carnegie Mellon University

import cv2
import numpy as np
import os
import glob
import dlib
import sys
import argparse
from PIL import Image
import pickle
import math
import datetime
import threading
import logging
from sklearn.pipeline import Pipeline
# from sklearn.grid_search import GridSearchCV
from sklearn.manifold import TSNE
from sklearn.svm import SVC
from sklearn.mixture import GMM
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import time
from operator import itemgetter
from datetime import datetime, timedelta
from sklearn.preprocessing import LabelEncoder
import atexit
from subprocess import Popen, PIPE
import os.path
import numpy as np
import pandas as pd
import aligndlib
import openface
import face_recognition
from imutils import paths

from kerasopenface.model import create_model
from keras import backend as K
from keras.models import Model
import tensorflow as tf

import AppEnv

logger = logging.getLogger(__name__)

start = time.time()
np.set_printoptions(precision=2)

systemDir = AppEnv.systemDir
luaDir = AppEnv.luaDir
modelDir = AppEnv.modelDir
dlibModelDir = AppEnv.dlibModelDir
openfaceModelDir = AppEnv.openfaceModelDir



class FaceRecogniserKeras(object):
    """This class implements face recognition using Openface's
    pretrained neural network and a Linear SVM classifier. Functions
    below allow a user to retrain the classifier and make predictions
    on detected faces"""

    def __init__(self):
        self.args = args = AppEnv.getArgs()
        self.align = openface.AlignDlib(args.dlibFacePredictor)
        self.predictor = dlib.shape_predictor(args.dlibFacePredictor)

        global nn4_small2_pretrained 
        K.clear_session()
        nn4_small2_pretrained = create_model()
        nn4_small2_pretrained.load_weights(args.networkModelTF)
        global graph
        graph = tf.get_default_graph()

        logger.info("Opening classifier.pkl to load existing known faces db")
        if os.path.isfile(AppEnv.classifierKerasPath):
            with open(AppEnv.classifierKerasPath, 'rb') as f: # le = labels, clf = classifier
                (self.labels, self.classifier) = pickle.load(f, encoding='latin1') # Loads labels and classifier SVM or GMM

    def make_prediction(self,rgbFrame,bb):
        """The function uses the location of a face
        to detect facial landmarks and perform an affine transform
        to align the eyes and nose to the correct positiion.
        The aligned face is passed through the neural net which
        generates 128 measurements which uniquly identify that face.
        These measurements are known as an embedding, and are used
        by the classifier to predict the identity of the person"""

        landmarks = self.align.findLandmarks(rgbFrame, bb)
        if landmarks == None:
            logger.debug("///  FACE LANDMARKS COULD NOT BE FOUND  ///")
            return None
        alignedFace = self.align.align(self.args.imgDim, rgbFrame, bb,landmarks=landmarks,landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if alignedFace is None:
            logger.debug("///  FACE COULD NOT BE ALIGNED  ///")
            return None

        logger.debug("////  FACE ALIGNED  // ")
        persondict = self.recognize_face(alignedFace)

        if persondict is None:
            logger.debug("/////  FACE COULD NOT BE RECOGNIZED  //")
            return persondict, alignedFace
        else:
            logger.debug("/////  FACE RECOGNIZED  /// ")
            return persondict, alignedFace

    def recognize_face(self, alignedFaceImg):
        """ Main flow for tagging a image with a predicated name """

        if self.getRep(alignedFaceImg) is None:
            return None
        rep1 = self.getRep(alignedFaceImg) # Gets embedding representation of image
        logger.debug("Embedding returned. Reshaping the image and flatting it out in a 1 dimension array.")
        rep = rep1.reshape(1, -1)   #take the image and  reshape the image array to a single line instead of 2 dimensionals
        start = time.time()
        logger.debug("Submitting array for prediction.")
        predictions = self.classifier.predict_proba(rep).ravel() # Computes probabilities of possible outcomes for samples in classifier(clf).
        #logger.info("We need to dig here to know why the probability are not right.")
        maxI = np.argmax(predictions)
        person1 = self.labels.inverse_transform(maxI)
        #person1 = person1.decode('utf-8') #only when python3 opening a python2 object
        confidence1 = int(math.ceil(predictions[maxI]*100))

        logger.info("Recognition took {} seconds.".format(time.time() - start))
        # logger.info("Recognized {} with {:.2f} confidence.".format(person1, confidence1))

        persondict = {'name': person1, 'confidence': confidence1, 'rep':rep1}
        print ("Recognized P: " + str(person1) + " C: " + str(confidence1))
        return persondict

    def getRep(self,alignedFace):
        rep = None
        bgrImg = alignedFace
        if bgrImg is None:
            logger.error("unable to load image")
            return None

        logger.debug("Tweaking the face color ")
        alignedFace = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
        # scale RGB values to interval [0,1]
        Img = (alignedFace / 255.).astype(np.float32)
        start = time.time()
        logger.debug("Getting embedding for the face")
            # scale RGB values to interval [0,1]
        with graph.as_default():
            rep = nn4_small2_pretrained.predict(np.expand_dims(Img, axis=0))[0]
        return rep

    def getBBs(self,bgrImg):
        if bgrImg is None:
            raise Exception("Unable to load image/frame")

        rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
        start = time.time()

        # Get the largest face bounding box
        # bb = align.getLargestFaceBoundingBox(rgbImg) #Bounding box

        # Get all bounding boxes
        bb = self.align.getAllFaceBoundingBoxes(rgbImg)

        if bb is None:
            # raise Exception("Unable to find a face: {}".format(imgPath))
            return None
        logger.info("Faces detection took {} seconds.".format(time.time() - start))

        return bb

    def reloadClassifier(self):
        with open(AppEnv.classifierKerasPath, 'rb') as f: # Reloads character stream from pickle file
            (self.labels, self.classifier) = pickle.load(f, encoding='latin1') # Loads labels and classifier SVM or GMM
        logger.info("reloadClassifier called")
        return True

    def trainClassifier(self):
        """Trainng the classifier begins by aligning any images in the
        training-images directory and putting them into the aligned images
        directory. Each of the aligned face images are passed through the
        neural net and the resultant embeddings along with their
        labels (names of the people) are used to train the classifier
        which is saved to a pickle file as a character stream"""

        logger.info("trainClassifier called")

        cachePath = AppEnv.pathJoin(AppEnv.alignedImagesDir, "cache.t7")
        try:
            os.remove(cachePath) # Remove cache from aligned images folder
        except:
            logger.info("Failed to remove cache.t7. Could be that it did not existed in the first place.")
            pass

        start = time.time()
        aligndlib.alignMain(AppEnv.trainingImagesDir, AppEnv.alignedImagesDir, "outerEyesAndNose", self.args.dlibFacePredictor, self.args.imgDim)
        logger.info("Aligning images for training took {} seconds.".format(time.time() - start))
        #done = False
        start = time.time()

        # grab the paths to the input images in our dataset
        logger.info("[INFO] quantifying faces...")
        imagePaths = list(paths.list_images(AppEnv.alignedImagesDir))
        # initialize the list of known encodings and known names
        embeddings = []
        labels = []        

        for (i, imagePath) in enumerate(imagePaths):
            # extract the person name from the image path
            logger.info("[INFO] processing image {}/{}".format(i + 1,
                len(imagePaths)))
            name = imagePath.split(os.path.sep)[-2]
        
            # load the input image and convert it from RGB (OpenCV ordering)
            # to dlib ordering (RGB)
            image = cv2.imread(imagePath)
            rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)        

            # scale RGB values to interval [0,1]
            rgb = (rgb / 255.).astype(np.float32)
            # obtain embedding vector for image
            with graph.as_default():
                encoding = nn4_small2_pretrained.predict(np.expand_dims(rgb, axis=0))[0]
            
            embeddings.append(encoding)
            labels.append(name)

        logger.info("Representation Generation (Classification Model) took {} seconds.".format(time.time() - start))
        start = time.time()

        self.labels = LabelEncoder().fit(labels) # LabelEncoder is a utility class to help normalize labels such that they contain only values between 0 and n_classes-1
        # Fits labels to model
        labelsNum = self.labels.transform(labels)
        nClasses = len(self.labels.classes_)
        logger.info("Training for {} classes.".format(nClasses))

        self.classifier = SVC(C=1, kernel='linear', probability=True)
        self.classifier.fit(embeddings, labelsNum) #link embeddings to labels

        fName = AppEnv.classifierKerasPath
        logger.info("Saving classifier to '{}'".format(fName))
        with open(fName, 'wb') as f:
            pickle.dump((self.labels,  self.classifier), f) # Creates character stream and writes to file to use for recognition

        logger.info("Training took {} seconds.".format(time.time() - start))


    def getSquaredl2Distance(self,rep1,rep2):
        """Returns number between 0-4, Openface calculated the mean between
        similar faces is 0.99 i.e. returns less than 0.99 if reps both belong
        to the same person"""
        
        d = rep1 - rep2
        #print("Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))        
        return float(np.dot(d, d))