import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import ImageUtils

import FaceDetector
from FaceDetector import DLIB_DETECTOR, OPENCV_DETECTOR
import CNNFaceDetector
import MTCNNFaceDetector
import MTCNN2FaceDetector
import DNNFaceDetector


from MethodCommon import update_camera_person_with_prediction
import AppEnv

import face_recognition

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

##################################################################################################################################################
#<#####################################################> FACE DETECTION AND RECOGNTIION <#########################################################>
##################################################################################################################################################
class MethodDetectRecogniseNew(object):

    def __init__(self, camera, faceRecogniserBestPhoto, confidenceThreshold, isDrawing):
        self.camera = camera
        self.faceRecogniserBestPhoto = faceRecogniserBestPhoto
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing

        self.args = args = AppEnv.getArgs()

        if self.args.detector == "CNN":
        #if args.cuda:
            self.faceDetector = CNNFaceDetector.CNNFaceDetector()
        elif self.args.detector== "dlib":
            self.faceDetector = FaceDetector.FaceDetector()
        elif self.args.detector== "MTCNN":
            self.faceDetector = MTCNNFaceDetector.MTCNNFaceDetector()
        elif self.args.detector== "MTCNN2":
            self.faceDetector = MTCNN2FaceDetector.MTCNN2FaceDetector()
        elif self.args.detector== "DNN":
            self.faceDetector = DNNFaceDetector.DNNFaceDetector()

        # use DLIB only
        self.faceDetectorMethod = DLIB_DETECTOR


    def run(self, frame):

        camera = self.camera

        DetectFaceStart = time.time()

        # height, width, channels = frame.shape
        # if width > 640:
        frame = ImageUtils.resize(frame)

        face_locations = []
        # face_locations = face_recognition.face_locations(frame)
        if self.args.detector== "MTCNN":
            faceBoxes, landmarks = self.faceDetector.detect_faces(frame, self.faceDetectorMethod)
            face_locations = [[faceBox.top(), faceBox.right(), faceBox.bottom(), faceBox.left()] for faceBox in faceBoxes]
        elif self.args.detector== "MTCNN2":
            faceBoxes, landmarks = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
            face_locations = [[faceBox.top(), faceBox.right(), faceBox.bottom(), faceBox.left()] for faceBox in faceBoxes]
        elif self.args.detector== "DNN":
            faceBoxes = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
            face_locations = [[faceBox.top(), faceBox.right(), faceBox.bottom(), faceBox.left()] for faceBox in faceBoxes]
        else:
            #faceBoxes = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
            if self.args.cuda:
                face_locations = face_recognition.face_locations(frame, number_of_times_to_upsample=1, model="cnn")
            else:
                face_locations = face_recognition.face_locations(frame)


        DetectFaceEnd = time.time()

        # logger.debug(f"face_locations = {face_locations}")

        if face_locations is not None and len(face_locations) > 0:

            if self.isDrawing == True:
                drawedFrame = ImageUtils.draw_boxes_standard(frame, face_locations)
                camera.State['processing_frame'] = drawedFrame
            else:
                camera.State['processing_frame'] = frame


            faceDetected = len(face_locations)
            if(faceDetected > 0):
                logger.info('FACES DETECTED: '+ str(faceDetected))
                logger.debug("Detect Face took {} seconds.".format( DetectFaceEnd - DetectFaceStart ) )

        
            # returns list of tuple that contains name, confidence and representation and an alignedFace (numpy array)
            predictions = self.faceRecogniserBestPhoto.make_prediction(frame, face_locations)

            if predictions is not None:
                for prediction in predictions:
                    # destruct the result
                    ( detectedLabel, confidence, face_bb, rep ) = prediction

                    # crop the face here, resize to standard 96px for better frontend presentation
                    faceimg = ImageUtils.crop_standard(frame, face_bb)
                    faceimg = ImageUtils.resize_standard96(faceimg)

                    persondict = { 'name': detectedLabel, 'confidence': confidence, 'rep': rep }
                    update_camera_person_with_prediction(camera, persondict, self.confidenceThreshold, faceimg)

        else:
            camera.State['processing_frame'] = frame
