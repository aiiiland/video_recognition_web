import sys
from os import path
import logging
import time
import datetime
import queue as Queue

sys.path.append(path.dirname(__file__))

import DataModels
import SurveillanceSystem

logger = logging.getLogger(__name__)


def update_camera_person_with_prediction(camera, predictionObj, confidenceThreshold, faceImg):
    """ Check if the confidence is enough, update detected person list and thumbnail """
    pName = predictionObj['name']
    pConfidence = predictionObj['confidence']
    isConfident = pConfidence > confidenceThreshold
    
    #reset the name if not match to any
    # if not isConfident:
    #     pName = "unknown"
    
    person = None
    with camera.peopleDictLock:

        # 1. Check the confidence level of the prediction first
        if isConfident:

            # 2. Check if the person already in runtime db
            if (pName in camera.people):
                person = camera.people[pName]
                # If current detected confidence is higher, update the person
                isMoreConfident = pConfidence > person.confidence
                isMoreRecent = time.time() - person.time > 1
                if isMoreConfident or isMoreRecent:
                    logger.info("!!! UPDATING person info !!!")
                    person.confidence = pConfidence
                    # if person.confidence > confidenceThreshold:
                    # person.identity = pName
                    person.set_thumbnail(faceImg) 
                    person.add_to_thumbnails(faceImg)  
                    person.set_time()
                    # Not sure this is necessary
                    camera.people[pName] = person
            else:
                logger.info("!!! NEW PERSON FOUND !!!")
                personKey = pName
                person = DataModels.Person(predictionObj['rep'], pName, pConfidence, face = faceImg)
                camera.people[personKey] = person

        else:
            # The prediction is not very confident
            # Don't count as the person

            personKey = "person" + datetime.datetime.now().strftime('%d%H%M')
            #show all the 'unknown' even it is the same 'unknown' person, people dict key + timestamp
            person = DataModels.Person(predictionObj['rep'], pName, pConfidence, face = faceImg, identity = "unknown", uniqueKey = personKey)
            camera.people[personKey] = person

    # TODO insert surv log
    try:
        payloadDict = person.toDict()
        payloadDict["camera_id"] = camera.camID
        payloadDict["camera_name"] = camera.label
        SurveillanceSystem.getInstance().surveillanceLogsQueue.put_nowait(payloadDict)
    except Queue.Full:
        logger.error("Queue is full, is the logger process running?")
    except Exception as e:
        logger.error("Error during logging : {}".format(e))
