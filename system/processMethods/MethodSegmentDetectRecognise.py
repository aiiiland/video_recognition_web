import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2


from MethodCommon import update_camera_person_with_prediction
import MotionDetector

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



###################################################################################################################################################################
#<#####################################>  MOTION DETECTION OBJECT SEGMENTAION FOLLOWED BY FACE DETECTION AND RECOGNITION <#####################################>
####################################################################################################################################################################
class MethodSegmentDetectRecognise(object):
    """ This approach uses background subtraction to segement a region of
        interest that is likely to contain a person. The region is cropped from
        the frame and face detection is performed on a much smaller image. This 
        improves proccessing performance but is highly dependent upon the accuracy of 
        the background model generated by the MotionDetector object. """

    def __init__(self, camera, state, faceRecogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.state = state
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing

        self.faceDetector = FaceDetector.FaceDetector()
        self.motionDetector = MotionDetector.MotionDetector()


    def run(self, frame):
        camera = self.camera

        camera.motion, peopleRects  = self.motionDetector.detect_movement(frame, get_rects = True)   

        if camera.motion == False:
            camera.State['processing_frame'] = frame
            logger.debug('////-- NO MOTION DETECTED --////')
            return

        logger.debug('///// MOTION DETECTED /////')
        if self.isDrawing == True:
            frame = ImageUtils.draw_boxes(frame, peopleRects, False)

        for x, y, w, h in peopleRects:
            
            logger.debug('//// Proccessing People Segmented Areas ///')
            bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h)) 
            personimg = ImageUtils.crop(frame, bb, dlibRect = True)
            
            personimg = cv2.flip(personimg, 1)
            camera.faceBoxes = self.faceDetector.detect_faces(personimg,camera.dlibDetection)
            if self.isDrawing == True:
                camera.State['processing_frame'] = ImageUtils.draw_boxes(frame, peopleRects, False)

            for face_bb in camera.faceBoxes: 

                if camera.dlibDetection == False:
                    x, y, w, h = face_bb
                    face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                    faceimg = ImageUtils.crop(personimg, face_bb, dlibRect = True)
                    if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                        continue
                logger.info('/// Proccessing Detected faces ///')

                predictions, alignedFace = self.faceRecogniser.make_prediction(personimg,face_bb)

                update_camera_person_with_prediction(camera, predictions, self.confidenceThreshold, alignedFace)