import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2

import FaceRecogniserEmbedding

from MethodCommon import update_camera_person_with_prediction

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class MethodFaceDetectionEmbedding(object):
    """ This approach peroforms basic face detection and 
        recognition using Openface Only  """

    def __init__(self, camera, state, recogniserEmbedding, confidenceThreshold, isDrawing):
        self.camera = camera
        self.state = state
        self.recogniserEmbedding = FaceRecogniserEmbedding.FaceRecogniserEmbedding()
        self.confidenceThreshold = 90 #confidenceThreshold
        self.isDrawing = isDrawing

        # self.faceDetector = FaceDetector.FaceDetector()


    def run(self, frame):
        camera = self.camera

        # frame = cv2.flip(frame, 1) # converts frame from BGR (OpenCV format) to RGB (Dlib format)
        frame = ImageUtils.resize(frame)
        camera.faceBoxes = self.recogniserEmbedding.getBBs(frame) 
        if self.isDrawing == True:
            drawedFrame = ImageUtils.draw_rects_face_recognition(frame, camera.faceBoxes, color=(0, 255, 0))
            camera.State['processing_frame'] = drawedFrame
        else:
            camera.State['processing_frame'] = frame
            # for (top, right, bottom, left) in camera.faceBoxes:
                
            #     camera.State['processing_frame'] = drawedFrame
            #     # draw the predicted face name on the image
            #     cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 2)
        # camera.State['processing_frame'] = frame

        if(len(camera.faceBoxes) > 0):
            logger.info('////  FACES DETECTED: '+ str(len(camera.faceBoxes)) +' //')

            predictions = self.recogniserEmbedding.make_prediction(frame, camera.faceBoxes)

            index = 0
            for face_bb in camera.faceBoxes: 
                
                # returns a dictionary that contains name, confidence and representation and an alignedFace (numpy array)
                alignedFace =  frame[face_bb[0]:face_bb[2], face_bb[3]:face_bb[1]]
                alignedFace = ImageUtils.resize_standard96(alignedFace)

                update_camera_person_with_prediction(camera, predictions[index], self.confidenceThreshold, alignedFace)
                index = index + 1
        
        # camera.State['processing_frame'] = frame # Used for streaming proccesed frames to client and email alerts, but mainly used for testing purposes