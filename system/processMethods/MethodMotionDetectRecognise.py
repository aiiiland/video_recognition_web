import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2


from MethodCommon import update_camera_person_with_prediction
import MotionDetector

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


##################################################################################################################################################
#<#####################################> MOTION DETECTION EVENT FOLLOWED BY FACE DETECTION AND RECOGNITION <#####################################>
##################################################################################################################################################
class MethodMotionDetectRecognise(object):
    """ When motion is detected, consecutive frames are proccessed for faces.
    If no faces are detected for longer than 30 seconds the thread goes back to
    looking for motion """

    def __init__(self, camera, state, faceRecogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.state = state
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing

        self.faceDetector = FaceDetector.FaceDetector()
        self.motionDetector = MotionDetector.MotionDetector()

        self.state['motion_state'] = 1
        self.state['motion_frame_count'] = 0


    def run(self, frame):
        camera = self.camera

        if self.state['motion_state'] == 1: # If no faces have been found or there has been no movement

            camera.motion, mframe = self.motionDetector.detect_movement(frame, get_rects = False)   

            if camera.motion == True:
                logger.debug('////////////////////// MOTION DETECTED //////////////////////')
                self.state['motion_state'] = 2
                camera.State['processing_frame'] = mframe
            else:
                logger.debug('////////////////////// NO MOTION DETECTED //////////////////////')
                return

        elif self.state['motion_state'] == 2: # If motion has been detected
            if self.state['motion_frame_count'] == 0:
                self.state['motion_start_time'] = time.time()
                self.state['motion_frame_count'] += 1

            # frame = cv2.flip(frame, 1)
            camera.faceBoxes = self.faceDetector.detect_faces(frame, camera.dlibDetection)
            if self.isDrawing == True:
                frame = ImageUtils.draw_boxes(frame, camera.faceBoxes, camera.dlibDetection)
        
            camera.State['processing_frame'] = frame

            if len(camera.faceBoxes) == 0:
                if (time.time() - self.state['motion_start_time']) > 30.0:
                    logger.info('//  No faces found for ' + str(time.time() - self.state['motion_start_time']) + ' seconds - Going back to Motion Detection Mode')
                    self.state['motion_state'] = 1
                    self.state['motion_frame_count'] = 0
            else:
                if(len(camera.faceBoxes) > 0):
                    logger.info('////  FACES DETECTED: '+ str(len(camera.faceBoxes)) +' ////')
                # frame = cv2.flip(frame, 1)
                for face_bb in camera.faceBoxes: 
                
                    if camera.dlibDetection == False:
                        x, y, w, h = face_bb
                        face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                        faceimg = ImageUtils.crop(frame, face_bb, dlibRect = True)
                        if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                            continue

                    predictions, alignedFace = self.faceRecogniser.make_prediction(frame,face_bb)

                    update_camera_person_with_prediction(camera, predictions, self.confidenceThreshold, alignedFace)

                self.state['motion_start_time'] = time.time() # Used to go back to motion detection state of 30s of not finding a face
                camera.State['processing_frame'] = frame
        