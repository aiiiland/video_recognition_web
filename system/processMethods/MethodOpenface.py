import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2

import random
from datetime import datetime, timedelta

from MethodCommon import update_camera_person_with_prediction

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class MethodOpenface(object):
    """ This approach peroforms basic face detection and 
        recognition using Openface Only  """

    def __init__(self, camera, state, faceRecogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.state = state
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing

        self.faceDetector = FaceDetector.FaceDetector()


    def run(self, frame):
        camera = self.camera

        frame = cv2.flip(frame, 1) # converts frame from BGR (OpenCV format) to RGB (Dlib format)
            
        faceBoxes = self.faceRecogniser.getBBs(frame) 
        if self.isDrawing == True:
            frame = ImageUtils.draw_boxes(frame, faceBoxes, camera.dlibDetection)
        camera.State['processing_frame'] = frame

        if(len(faceBoxes) > 0):
            logger.info('////  FACES DETECTED: '+ str(len(faceBoxes)) +' //')
        for face_bb in faceBoxes: 
            
            # Used to reduce false positives from opencv haar cascade detector.
            # If face isn't detected using more rigorous paramters in the detectMultiscale() function read the next frame               
            if camera.dlibDetection == False:
                x, y, w, h = face_bb
                face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                faceimg = ImageUtils.crop(frame, face_bb, dlibRect = True)
                if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                    continue

            # returns a dictionary that contains name, confidence and representation and an alignedFace (numpy array)
            predictions, alignedFace = self.faceRecogniser.make_prediction(frame,face_bb) 

            update_camera_person_with_prediction(camera, predictions, self.confidenceThreshold, alignedFace)
        
        camera.State['processing_frame'] = frame # Used for streaming proccesed frames to client and email alerts, but mainly used for testing purposes