import sys
import os
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2
import shutil

import FaceRecogniserEmbedding

from multiprocessing import Lock

from MethodCommon import update_camera_person_with_prediction
import AppEnv

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class MethodFaceCaptureEmbedding(object):
    """ This will be used to capture faces for training in the surveillance environment 
        not fully implmented - was mainly used for face capture during testing """

    def __init__(self, mainApp, camera, state, recogniserEmbedding, confidenceThreshold, isDrawing, trainingEvent):
        self.camera = camera
        self.state = state
        self.recogniserEmbedding = FaceRecogniserEmbedding.FaceRecogniserEmbedding()
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing
        self.trainingEvent = trainingEvent
        self.mainApp = mainApp

        self.faceDetector = FaceDetector.FaceDetector()

        self.testingResultsLock = Lock()
        self.detetectionsCount = 0
        self.trueDetections = 0
        self.counter = 0


    def run(self, frame):
        camera = self.camera

        # tempframe = frame
        if not os.path.exists(AppEnv.capturedImagesDir):
            os.makedirs(AppEnv.capturedImagesDir)
        frame = cv2.flip(frame, 1)

        camera.faceBoxes = self.recogniserEmbedding.getBBs(frame) 

        if(len(camera.faceBoxes) > 0):
            logger.debug('//  FACES DETECTED: '+ str(len(camera.faceBoxes)) +' ///')

        for face_bb in camera.faceBoxes: 
            with self.testingResultsLock:
                self.detetectionsCount += 1 
                
                predictions, alignedFace = self.recogniserEmbedding.make_prediction(frame,face_bb)
                destPath = os.path.join(AppEnv.capturedImagesDir, 'capture-'+ str(self.detetectionsCount) +'.png')
                cv2.imwrite(destPath, alignedFace)

        if self.isDrawing == True:
            frame = ImageUtils.draw_boxes(frame, camera.faceBoxes, camera.dlibDetection)
        camera.State['processing_frame'] = frame

        logger.debug("used time: " + str(time.time() - camera.FPSstart))
        if (time.time() - camera.FPSstart) > 30: #capture 2 minutes
            self.mainApp.remove_camera(camera.camID) #camID to be defined
            newPersonDir = os.path.join(AppEnv.trainingImagesDir, camera.personLabel)
            if not os.path.exists(newPersonDir):
                os.makedirs(newPersonDir)
            sourceDir = os.listdir(AppEnv.capturedImagesDir)
            for files in sourceDir:
                if files.endswith('.png'):
                    filepath = os.path.join(AppEnv.capturedImagesDir, files)
                    logger.debug(filepath)
                    shutil.move(filepath,newPersonDir)
            logger.debug("copy the captured photos to training-image")
            #start training    
            self.trainingEvent.clear() # Block processing threads
            self.recogniserEmbedding.trainClassifier()#calling the module in FaceRecogniser to start training
            self.trainingEvent.set() # Release processing threads       
            logger.debug("finished face capture training")