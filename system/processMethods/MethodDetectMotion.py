import sys
from os import path
import time
import logging

import MotionDetector

sys.path.append(path.dirname(__file__))


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class MethodDetectMotion(object):

    def __init__(self, camera):
        self.camera = camera

        self.motionDetector = MotionDetector.MotionDetector()


    def run(self, frame):
        camera = self.camera
        camera.motion, mframe = self.motionDetector.detect_movement(frame, get_rects = False) 
        camera.State['processing_frame'] = frame
        if camera.motion == False:
            logger.debug('//// NO MOTION DETECTED /////')
            return
        else:
            logger.debug('/// MOTION DETECTED ///')
        