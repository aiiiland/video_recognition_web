import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import ImageUtils

import FaceDetector
from FaceDetector import DLIB_DETECTOR, OPENCV_DETECTOR
import CNNFaceDetector
import MTCNNFaceDetector
import MTCNN2FaceDetector
import DNNFaceDetector

from MethodCommon import update_camera_person_with_prediction
import AppEnv

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

##################################################################################################################################################
#<#####################################################> FACE DETECTION AND RECOGNTIION <#########################################################>
##################################################################################################################################################
class MethodDetectRecognise(object):

    def __init__(self, camera, faceRecogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = 90
        self.isDrawing = isDrawing


        self.args = args = AppEnv.getArgs()

        if self.args.detector == "CNN":
        #if args.cuda:
            self.faceDetector = CNNFaceDetector.CNNFaceDetector()
        elif self.args.detector== "dlib":
            self.faceDetector = FaceDetector.FaceDetector()
        elif self.args.detector== "MTCNN":
            self.faceDetector = MTCNNFaceDetector.MTCNNFaceDetector()
        elif self.args.detector== "MTCNN2":
            self.faceDetector = MTCNN2FaceDetector.MTCNN2FaceDetector()
        elif self.args.detector== "DNN":
            self.faceDetector = DNNFaceDetector.DNNFaceDetector()

        # use DLIB only
        self.faceDetectorMethod = DLIB_DETECTOR





    def run(self, frame):

        camera = self.camera

        DetectFaceStart = time.time()

        # height, width, channels = frame.shape
        # if width > 640:
        frame = ImageUtils.resize(frame)
        if self.args.detector== "MTCNN":
            faceBoxes, landmarks = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
        elif self.args.detector== "MTCNN2":
            faceBoxes, landmarks = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
        elif self.args.detector== "DNN":
            faceBoxes = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
        else:
            faceBoxes = self.faceDetector.detect_faces(frame, self.faceDetectorMethod) 
        DetectFaceEnd = time.time()

        if self.isDrawing == True:
            drawedFrame = ImageUtils.draw_boxes(frame, faceBoxes, self.faceDetectorMethod)
            camera.State['processing_frame'] = drawedFrame
        else:
            camera.State['processing_frame'] = frame
        

        faceDetected = len(faceBoxes)
        if(faceDetected > 0):
            logger.info('FACES DETECTED: '+ str(faceDetected))
            logger.debug("Detect Face took {} seconds.".format( DetectFaceEnd - DetectFaceStart ) )

        for face_bb in faceBoxes: 
            
            # Used to reduce false positives from opencv haar cascade detector.
            # If face isn't detected using more rigorous paramters in the detectMultiscale() function read the next frame               
            # if self.faceDetectorMethod == OPENCV_DETECTOR:
            #     x, y, w, h = face_bb
            #     face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
            #     faceimg = ImageUtils.crop(frame, face_bb, dlibRect = True)
            #     if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
            #         continue

            #if self.args.cuda:
            if self.args.detector == "CNN" or self.args.detector== "MTCNN" or self.args.detector== "MTCNN2":
                faceimg = ImageUtils.crop(frame, face_bb, dlibRect = True)
                if not self.faceDetector.is_front_face(faceimg):
                    logger.debug("Not front face")
                    continue

            # returns a dictionary that contains name, confidence and representation and an alignedFace (numpy array)
            predictions, alignedFace = self.faceRecogniser.make_prediction(frame, face_bb)

            update_camera_person_with_prediction(camera, predictions, self.confidenceThreshold, alignedFace)
            



    