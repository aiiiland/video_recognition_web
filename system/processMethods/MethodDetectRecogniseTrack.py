import sys
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import DataModels
import FaceDetector
import ImageUtils
import cv2

import random
from datetime import datetime, timedelta

from MethodCommon import update_camera_person_with_prediction

import MotionDetector


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)




############################################################################################################################################################################
#<#####################################>  MOTION DETECTION OBJECT SEGMENTAION FOLLOWED BY FACE DETECTION, RECOGNITION AND TRACKING <#####################################>
#############################################################################################################################################################################
class MethodDetectRecogniseTrack(object):
    """ This approach incorporates background subtraction to perform person tracking 
        and is the most efficient out of the all proccesing funcions above. When
        a face is detected in a region a Tracker object it generated, and is updated
        every frame by comparing the last known region of the person, to new regions
        produced by the motionDetector object. Every update of the tracker a detected 
        face is compared to the person's face of whom is being tracked to ensure the tracker
        is still tracking the correct person. This is acheived by comparing the prediction
        and the the l2 distance between their embeddings (128 measurements that represent the face).
        If a tracker does not overlap with any of the regions produced by the motionDetector object
        for some time the Tracker is deleted.  """

    def __init__(self, camera, state, faceRecogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.state = state
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing

        self.faceDetector = FaceDetector.FaceDetector()
        self.motionDetector = MotionDetector.MotionDetector()


    def run(self, frame):
        camera = self.camera

        logger.debug('//// detect_recognise_track 1 ////')
        peopleFound = False
        camera.motion, peopleRects  = self.motionDetector.detect_movement(frame, get_rects = True)   
        logger.debug('//// detect_recognise_track  2 /////')
    
        if camera.motion == False:
            camera.State['processing_frame'] = frame
            logger.debug('///// NO MOTION DETECTED /////')
            return

        if self.isDrawing == True:
            camera.State['processing_frame'] = ImageUtils.draw_boxes(frame, peopleRects, False)

        logger.debug('//// MOTION DETECTED //////')

        
        for x, y, w, h in peopleRects:

            peopleFound = True
            person_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h)) 
            personimg = ImageUtils.crop(frame, person_bb, dlibRect = True)   # Crop regions of interest 

            personimg = cv2.flip(personimg, 1) 

            tracked = False
            # Iterate through each tracker and compare there current psotiion
            for i in range(len(camera.trackers) - 1, -1, -1): 
                
                if camera.trackers[i].overlap(person_bb):
                    logger.debug("=> Updating Tracker <=")
                    camera.trackers[i].update_tracker(person_bb)
                    # personimg = cv2.flip(personimg, 1)
                    camera.faceBoxes = self.faceDetector.detect_faces(personimg,camera.dlibDetection)  
                    if(len(camera.faceBoxes) > 0):
                        logger.debug('//////  FACES DETECTED: '+ str(len(camera.faceBoxes)) +' /////')
                    if len(camera.faceBoxes) > 0:
                        logger.info("Found " + str(len(camera.faceBoxes)) + " faces.")
                    for face_bb in camera.faceBoxes: 

                        if camera.dlibDetection == False:
                            x, y, w, h = face_bb
                            face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                            faceimg = ImageUtils.crop(personimg, face_bb, dlibRect = True)
                            if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                                continue

                        predictions, alignedFace =  self.faceRecogniser.make_prediction(personimg,face_bb)
                
                        if predictions['confidence'] > self.confidenceThreshold:
                            predictedName = predictions['name']
                        else:
                            predictedName = "unknown"
                            
                        # If only one face is detected
                        if len(camera.faceBoxes) == 1:
                            # if not the same person check to see if tracked person is unknown and update or change tracker accordingly
                            # l2Distance is between 0-4 Openface found that 0.99 was the average cutoff between the same and different faces
                            # the same face having a distance less than 0.99 
                            if self.faceRecogniser.getSquaredl2Distance(camera.trackers[i].person.rep ,predictions['rep']) > 0.99 and (camera.trackers[i].person.identity != predictedName): 
                                
                                    alreadyBeenDetected = False
                                    with camera.peopleDictLock:
                                            for ID, person in camera.people.items():  # iterate through all detected people in camera
                                                # if the person has already been detected continue to track that person - use same person ID
                                                if person.identity == predictedName or self.faceRecogniser.getSquaredl2Distance(person.rep ,predictions['rep']) < 0.8:
                                                        
                                                        # person = DataModels.Person(predictions['rep'], predictions['confidence'], alignedFace, predictedName)
                                                        person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace, identity = predictedName, uniqueKey = predictedName)
                                                        logger.info( "====> New Tracker for " +person.identity + " <===")
                                                        # Remove current tracker and create new one with the ID of the original person
                                                        del camera.trackers[i]
                                                        camera.trackers.append(DataModels.Tracker(frame, person_bb, person,ID))
                                                        alreadyBeenDetected = True
                                                        break

                                    if not alreadyBeenDetected:
                                            num = random.randrange(1, 1000, 1)    
                                            strID = "person" +  datetime.now().strftime("%Y%m%d%H%M%S") + str(num) # Create a new person ID
                                            # Is the new person detected with a low confidence? If yes, classify them as unknown
                                            if predictions['confidence'] > self.confidenceThreshold:
                                                    # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, predictions['name'])
                                                    person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace, identity = predictedName, uniqueKey = predictedName)
                                            else:   
                                                    # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, "unknown")
                                                    person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace, identity = predictedName, uniqueKey = predictedName)
                                            #add person to detected people      
                                            with camera.peopleDictLock:
                                                    camera.people[strID] = person
                                                    logger.info( "=====> New Tracker for new person <====")
                                            del camera.trackers[i]
                                            camera.trackers.append(DataModels.Tracker(frame, person_bb, person,strID))
                            # if it is the same person update confidence if it is higher and change prediction from unknown to identified person
                            # if the new detected face has a lower confidence and can be classified as unknown, when the person being tracked isn't unknown - change tracker
                            else:
                                logger.info( "====> update person name and confidence <==")
                                if camera.trackers[i].person.confidence < predictions['confidence']:
                                    camera.trackers[i].person.confidence = predictions['confidence']
                                    if camera.trackers[i].person.confidence > self.confidenceThreshold:
                                        camera.trackers[i].person.identity = predictions['name']

                            
                        # If more than one face is detected in the region compare faces to the people being tracked and update tracker accordingly
                        else:
                            logger.info( "==> More Than One Face Detected <==")
                            # if tracker is already tracking the identified face make an update 
                            if self.faceRecogniser.getSquaredl2Distance(camera.trackers[i].person.rep ,predictions['rep']) < 0.99 and camera.trackers[i].person.identity == predictions['name']: 
                                if camera.trackers[i].person.confidence < predictions['confidence']:
                                    camera.trackers[i].person.confidence = predictions['confidence']
                                    if camera.trackers[i].person.confidence > self.confidenceThreshold:
                                        camera.trackers[i].person.identity = predictions['name']
                            else:
                                # if tracker isn't tracking this face check the next tracker
                                break
                        
                        camera.trackers[i].person.set_thumbnail(alignedFace)  
                        camera.trackers[i].person.add_to_thumbnails(alignedFace)
                        camera.trackers[i].person.set_rep(predictions['rep'])
                        camera.trackers[i].person.set_time()
                        camera.trackers[i].reset_face_pinger()
                        with camera.peopleDictLock:
                                camera.people[camera.trackers[i].id] = camera.trackers[i].person
                    camera.trackers[i].reset_pinger()
                    tracked = True
                    break

            # If the region is not being tracked
            if not tracked:

                # Look for faces in the cropped image of the region
                camera.faceBoxes = self.faceDetector.detect_faces(personimg,camera.dlibDetection)
                
                for face_bb in camera.faceBoxes:

                    if camera.dlibDetection == False:
                        x, y, w, h = face_bb
                        face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                        faceimg = ImageUtils.crop(personimg, face_bb, dlibRect = True)
                        if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                            continue

                    predictions, alignedFace =  self.faceRecogniser.make_prediction(personimg,face_bb)
        
                    alreadyBeenDetected = False
                    with camera.peopleDictLock:
                            for ID, person in camera.people.items():  # iterate through all detected people in camera, to see if the person has already been detected
                                if person.identity == predictions['name'] or self.faceRecogniser.getSquaredl2Distance(person.rep ,predictions['rep']) < 0.8: 
                                        if predictions['confidence'] > self.confidenceThreshold and person.confidence > self.confidenceThreshold:
                                                # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, predictions['name'])
                                                person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace)
                                        else:   
                                                # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, "unknown")
                                                person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace, identity = "unknown", uniqueKey = "unknown")
                                        logger.info( "==> New Tracker for " + person.identity + " <====")
                            
                                        camera.trackers.append(DataModels.Tracker(frame, person_bb, person,ID))
                                        alreadyBeenDetected = True
                                        break
                                    
                    if not alreadyBeenDetected:
                            num = random.randrange(1, 1000, 1)    # Create new person ID if they have not been detected
                            strID = "person" +  datetime.now().strftime("%Y%m%d%H%M%S") + str(num)
                            if predictions['confidence'] > self.confidenceThreshold:
                                    # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, predictions['name'])
                                    person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace)
                            else:   
                                    # person = DataModels.Person(predictions['rep'],predictions['confidence'], alignedFace, "unknown")
                                    person = DataModels.Person(predictions['rep'], predictions['name'], predictions['confidence'], alignedFace, identity = "unknown", uniqueKey = "unknown")
                            #add person to detected people      
                            with camera.peopleDictLock:
                                    camera.people[strID] = person
                            logger.info( "====> New Tracker for new person <=")
                            camera.trackers.append(DataModels.Tracker(frame, person_bb, person,strID))


        for i in range(len(camera.trackers) - 1, -1, -1): # starts with the most recently initiated tracker
            if self.isDrawing == True:
                    bl = (camera.trackers[i].bb.left(), camera.trackers[i].bb.bottom()) # (x, y)
                    tr = (camera.trackers[i].bb.right(), camera.trackers[i].bb.top()) # (x+w,y+h)
                    cv2.rectangle(frame, bl, tr, color=(0, 255, 255), thickness=2)
                    cv2.putText(frame,  camera.trackers[i].person.identity + " " + str(camera.trackers[i].person.confidence)+ "%", (camera.trackers[i].bb.left(), camera.trackers[i].bb.top() - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.3,
                            color=(0, 255, 255), thickness=1)
            camera.State['processing_frame'] = frame
            # Used to check if tracker hasn't been updated
            camera.trackers[i].ping()
            camera.trackers[i].faceping()

            # If the tracker hasn't been updated for more than 10 pings delete it
            if camera.trackers[i].pings > 10: 
                del camera.trackers[i]
                return