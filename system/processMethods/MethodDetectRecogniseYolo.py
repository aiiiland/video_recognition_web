import sys
from os import path
import time
import logging
import numpy as np
from centroidtracker import CentroidTracker

sys.path.append(path.dirname(__file__))

import ImageUtils

from MethodCommon import update_camera_person_with_prediction
import AppEnv

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

##################################################################################################################################################
#<#####################################################> FACE DETECTION AND RECOGNTIION <#########################################################>
##################################################################################################################################################
class MethodDetectRecogniseYolo(object):

    def __init__(self, camera, Recogniser, confidenceThreshold, isDrawing):
        self.camera = camera
        self.Recogniser = Recogniser
        self.confidenceThreshold = 0.5
        self.isDrawing = isDrawing
        self.ct = CentroidTracker()

        self.args = args = AppEnv.getArgs()


    def run(self, frame):

        camera = self.camera

        logger.info("run YOLO")
        DetectStart = time.time()

        # height, width, channels = frame.shape
        # if width > 640:
        frame = ImageUtils.resize(frame)

        predictions = self.Recogniser.make_prediction(frame)
        # [name, confidence, box] 

        if predictions is None:
            return
 
        DetectEnd = time.time()
        #preditions = np.array(predictions)

        if predictions is not None and len(predictions) > 0:

            #4locations = predictions[:,2]
            locations = [pred[2] for pred in predictions]
	        #tracking the object
            rects = [[l[3], l[0], l[1], l[2]] for l in locations]
            objects = self.ct.update(rects)
            
            logger.debug(f"locations = {locations}")
            if self.isDrawing == True:
                drawedFrame = ImageUtils.draw_boxes_standard(frame, locations)
                camera.State['processing_frame'] = drawedFrame
            else:
                camera.State['processing_frame'] = frame

            objectDetected = len(locations)
            if(objectDetected > 0):
                logger.info('OBJECT DETECTED: '+ str(objectDetected))
                logger.debug("Detect Object took {} seconds.".format( DetectEnd - DetectStart ) )

            for prediction, objectItem in zip(predictions, objects.items()):

                ( detectedLabel, confidence, bb ) = prediction
                ( objectID, centroid ) = objectItem

                # crop the face here, resize to standard 96px for better frontend presentation
                img = ImageUtils.crop_standard(frame, bb)
                #img = ImageUtils.resize_standard96(img)

                persondict = { 'name': str(detectedLabel, 'utf-8') + str(objectID), 'confidence': confidence, 'rep': None }
                update_camera_person_with_prediction(camera, persondict, self.confidenceThreshold, img)

        else:
            camera.State['processing_frame'] = frame
