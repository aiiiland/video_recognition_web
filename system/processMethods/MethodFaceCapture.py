import sys
import os
from os import path
import time
import logging

sys.path.append(path.dirname(__file__))


import dlib
import SurveillanceSystem
import DataModels
import FaceDetector
import ImageUtils
import cv2
import shutil

from multiprocessing import Lock

from MethodCommon import update_camera_person_with_prediction
import AppEnv

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class MethodFaceCapture(object):
    """ This will be used to capture faces for training in the surveillance environment 
        not fully implmented - was mainly used for face capture during testing """

    def __init__(self, mainApp, camera, state, faceRecogniser, confidenceThreshold, isDrawing, trainingEvent):
        self.camera = camera
        self.state = state
        self.faceRecogniser = faceRecogniser
        self.confidenceThreshold = confidenceThreshold
        self.isDrawing = isDrawing
        self.trainingEvent = trainingEvent
        self.mainApp = mainApp

        self.faceDetector = FaceDetector.FaceDetector()


        self.testingResultsLock = Lock()
        self.detetectionsCount = 0
        self.trueDetections = 0
        self.counter = 0


    def run(self, frame):
        camera = self.camera


        if not os.path.exists(AppEnv.capturedImagesDir):
            os.makedirs(AppEnv.capturedImagesDir)
        frame = cv2.flip(frame, 1)

        camera.faceBoxes = self.faceDetector.detect_faces(frame,camera.dlibDetection)
        #camera.faceBoxes = self.faceRecogniser.getBBs(frame) 

        if(len(camera.faceBoxes) > 0):
            logger.debug('//  FACES DETECTED: '+ str(len(camera.faceBoxes)) +' ///')

        for face_bb in camera.faceBoxes: 
            result = ""
            # used to reduce false positives from opencv haar cascade detector
            if camera.dlibDetection == False:
                x, y, w, h = face_bb
                face_bb = dlib.rectangle(int(x), int(y), int(x+w), int(y+h))
                faceimg = ImageUtils.crop(frame, face_bb, dlibRect = True)
                if len(self.faceDetector.detect_cascadeface_accurate(faceimg)) == 0:
                    continue
                        
            with self.testingResultsLock:
                self.detetectionsCount += 1 
                
                predictions, alignedFace = self.faceRecogniser.make_prediction(frame,face_bb)
                destPath = os.path.join(AppEnv.capturedImagesDir, 'capture-'+ str(self.detetectionsCount) +'.png')
                cv2.imwrite(destPath, alignedFace)

        if self.isDrawing == True:
            frame = ImageUtils.draw_boxes(frame, camera.faceBoxes, camera.dlibDetection)
        camera.State['processing_frame'] = frame

        print("used time: " + str(time.time() - camera.FPSstart))
        if (time.time() - camera.FPSstart) > 30: #capture 2 minutes
            self.mainApp.remove_camera(camera.camID) #camID to be defined
            newPersonDir = os.path.join(AppEnv.trainingImagesDir, camera.personLabel)
            if not os.path.exists(newPersonDir):
                os.makedirs(newPersonDir)
            sourceDir = os.listdir(AppEnv.capturedImagesDir)
            for files in sourceDir:
                if files.endswith('.png'):
                    filepath = os.path.join(AppEnv.capturedImagesDir, files)
                    print(filepath)
                    shutil.move(filepath,newPersonDir)
            print("copy the captured photos to training-image")
            #start training    
            self.trainingEvent.clear() # Block processing threads
            self.faceRecogniser.trainClassifier()#calling the module in FaceRecogniser to start training
            self.trainingEvent.set() # Release processing threads       
            print("finished face capture training")