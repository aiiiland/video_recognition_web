
# Surveillance System Controller.
# Brandon Joffe
# 2016
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Code used in this project included opensource software (Openface)
# developed by Brandon Amos
# Copyright 2015-2016 Carnegie Mellon University


import time
import argparse
import cv2
import os
import pickle
from operator import itemgetter
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.mixture import GMM
import dlib
import atexit
from subprocess import Popen, PIPE
import os.path
import sys
import logging
from logging.handlers import RotatingFileHandler

import time
from datetime import datetime, timedelta
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import requests
import json
from openface.data import iterImgs
import Camera
import FaceRecogniser
import FaceRecogniserEmbedding
import FaceRecogniserKeras
import FaceRecogniserBestPhotoSimilarity
import ObjectRecogniserYOLO
import openface
import aligndlib
import ImageUtils
import random
import psutil
import math
import shutil
import queue as Queue

import threading
# from threading import Event, Lock

import multiprocessing
from multiprocessing import Process, Event, Lock, Manager, Queue as MPQueue


import MethodDetectMotion
import MethodDetectRecognise
import MethodDetectRecogniseNew
#import MethodMotionDetectRecognise
#import MethodSegmentDetectRecognise
#import MethodDetectRecogniseTrack
#import MethodOpenface
#import MethodFaceCapture
#import MethodFaceDetectionEmbedding
#import MethodFaceCaptureEmbedding
import MethodDetectRecogniseYolo

import AppEnv

import SurveillanceLogger

# Get paths for models
# //////////////////////////////////////////////////////////////////////////////////////////////

systemDir = AppEnv.systemDir


start = time.time()
np.set_printoptions(precision=2)

try:
    os.makedirs('logs', exist_ok=True)  # Python>3.2
except TypeError:
    try:
        os.makedirs('logs')
    except OSError as exc:  # Python >2.5
        print("logging directory already exist")

logger = logging.getLogger(__name__)
# formatter = logging.Formatter("(%(threadName)-10s) %(asctime)s - %(name)s - %(levelname)s - %(message)s")
# handler = RotatingFileHandler("logs/surveillance.log", maxBytes=10000000, backupCount=10)
# handler.setLevel(logging.DEBUG)
# handler.setFormatter(formatter)
# logger.addHandler(handler)
logger.setLevel(logging.DEBUG)





def frame_processor(mainApp, camera, camID, cameraFunction, queue, state, faceRecogniser, objectRecogniser, runningFlag, trainingEvent):

    args = AppEnv.getArgs()
    # Runtime constants
    confidenceThreshold = state['confidenceThreshold']
    drawing = state['drawing']

    method = None
    #if cameraFunction == "detect_motion":
    #    method = MethodDetectMotion.MethodDetectMotion(camera)
    if cameraFunction == "detect_recognise":
        if args.recogniser == "face_recognition_compare":
            method = MethodDetectRecogniseNew.MethodDetectRecogniseNew(camera, faceRecogniser, confidenceThreshold, drawing)
        else:
            method = MethodDetectRecognise.MethodDetectRecognise(camera, faceRecogniser, confidenceThreshold, drawing)
    #elif cameraFunction == "motion_detect_recognise":
    #    method = MethodMotionDetectRecognise.MethodMotionDetectRecognise(camera, state, faceRecogniser, confidenceThreshold, drawing)
    #elif cameraFunction == "segment_detect_recognise":
    #    method = MethodSegmentDetectRecognise.MethodSegmentDetectRecognise(camera, state, faceRecogniser, confidenceThreshold, drawing)
    #elif cameraFunction == "detect_recognise_track":
    #    method = MethodDetectRecogniseTrack.MethodDetectRecogniseTrack(camera, state, faceRecogniser, confidenceThreshold, drawing)
    elif cameraFunction == "object_recognise_yolo":
        method = MethodDetectRecogniseYolo.MethodDetectRecogniseYolo(camera, objectRecogniser, confidenceThreshold, drawing)

    while True:

        # Init some vars for current loop
        # FPSstart = time.time()

        if runningFlag.value != 1:
            logger.info("Process thread terminate ...")
            break

        try:

            frame = queue.get(True, 1)

            if frame is None:
                logger.info("No frame")
                time.sleep(1)
                continue

            if (np.shape(frame) == ()): #or (np.array_equal(frame, camera.tempFrame))):  # Checks to see if the new frame is the same as the previous frame
                logger.debug("Frame skipped")
                continue

            # height, width, channels = frame.shape

            # Wait for any training
            trainingEvent.wait()

            # run the method
            method.run(frame)

        except Queue.Empty:
            # timeout
            time.sleep(0.04)
        except Exception as e:
            logger.error("Caught exception in processing process: {}".format(e))
            # throttle a bit so will not busy load
            time.sleep(0.04)


    logger.info("End of the frame_processor function, thread will terminate ...")
    return -1


singleton = None



class SurveillanceSystem(object):
    """ The SurveillanceSystem object is the heart of this application.
    It provides all the central proccessing and ties everything
    together. It generates camera frame proccessing threads as 
    well as an alert monitoring thread. A camera frame proccessing 
    thread can process a camera using 5 different processing methods.
    These methods aim to allow the user to adapt the system to their 
    needs and can be found in the process_frame() function. The alert 
    monitoring thread continually checks the system state and takes 
    action if a particular event occurs. """ 

    def __init__(self):

        global singleton
        if singleton is not None:
            print("Do not create more than one instance of SurveillanceSystem")
            logger.error("Do not create more than one instance of SurveillanceSystem")
            exit()

        # THIS SHOULD BE THE ONLY MANAGER IN THIS PROGRAM
        self.processManager = Manager()

        self.args = args = AppEnv.getArgs()
        self.recogniser = None
        self.objectRecogniser = None

        if self.args.recogniser == "openface":
            self.recogniser = FaceRecogniser.FaceRecogniser()
        elif self.args.recogniser == "face_recognition":
            self.recogniser = FaceRecogniserEmbedding.FaceRecogniserEmbedding()
        elif self.args.recogniser == "keras":
            self.recogniser = FaceRecogniserKeras.FaceRecogniserKeras()
        elif self.args.recogniser == "face_recognition_compare":
            self.recogniser = FaceRecogniserBestPhotoSimilarity.FaceRecogniserBestPhotoSimilarity()
        
        if self.args.objectRecogniser == "object_recognition_yolo":
            self.objectRecogniser = ObjectRecogniserYOLO.ObjectRecogniserYOLO()

        # self.recogniserYOLO = ObjectRecogniserYOLO.FaceRecogniserYOLO()
        self.trainingEvent = Event() # Used to holt processing while training the classifier 
        self.trainingEvent.set() 
        self.drawing = True 
        self.alarmState = 'Disarmed' # Alarm states - Disarmed, Armed, Triggered
        self.alarmTriggerd = False
        self.alerts = [] # Holds all system alerts
        self.cameras = {} # Holds all system cameras
        self.camerasLock  = Lock() # Used to block concurrent access of cameras []
        self.cameraProcessingThreads = {}
        self.peopleDB = []
        self.confidenceThreshold = self.args.confidenceThreshold # Used as a threshold to classify a person as unknown


        self.surveillanceLogsQueue = MPQueue(128)

        # Initialization of alert processing thread 
        self.alertsLock = Lock()

        # Used for testing purposes
        ###################################
        self.testingResultsLock = Lock()
        self.detetectionsCount = 0
        self.trueDetections = 0
        self.counter = 0
        ####################################

        self.load_names_to_memory() # Gets people in database for web client

        # DEPRECATED
        #//////////////////////////////////////////////////// Camera Examples ////////////////////////////////////////////////////
        #self.cameras.append(Camera.IPCamera("testing/iphoneVideos/singleTest.m4v","detect_recognise_track",False)) # Video Example - uncomment and run code
        # self.cameras.append(Camera.IPCamera("http://192.168.1.33/video.mjpg","detect_recognise_track",False))
        
        # processing frame threads 
        # for i, cam in enumerate(self.cameras):       
        #   self.start_processing_process(cam)

    def getManager(self):
        return self.processManager

    def trainClassifier(self):
        if self.recogniser is not None:
            self.recogniser.trainClassifier()
            self.recogniser.reloadClassifier()

    def start_processing_thread(self, camera):
        
        self.processStateStore = dict()
        self.processStateStore['confidenceThreshold'] = self.confidenceThreshold
        self.processStateStore['drawing'] = self.drawing


        thread = threading.Thread(name='frame_processor',target=frame_processor, args=(self, camera, camera.camID, camera.cameraFunction, camera.Q, self.processStateStore, self.recogniser, self.objectRecogniser, camera.runningFlag, self.trainingEvent))
        # process = Process(target=frame_processor, args=(self, camera, camera.camID, camera.cameraFunction, camera.Q, self.processStateStore, self.recogniser, camera.runningFlag, self.trainingEvent))
        thread.daemon = True
        self.cameraProcessingThreads[str(camera.camID)] = thread
        thread.start()


    def start_processing_process(self, camera):
        
        self.processStateStore = self.processManager.dict()
        self.processStateStore['confidenceThreshold'] = self.confidenceThreshold
        self.processStateStore['drawing'] = self.drawing

        # self.runningFlag = multiprocessing.Value("i")
        # self.runningFlag.value = 1

        process = Process(target=frame_processor, args=(self, camera, camera.camID, camera.cameraFunction, camera.Q, self.processStateStore, self.recogniser, self.objectRecogniser, camera.runningFlag, self.trainingEvent))
        process.daemon = False
        self.cameraProcessingThreads[str(camera.camID)] = process
        process.start()


    def add_camera(self, camera):
        """Adds new camera to the System and generates a 
        frame processing thread"""

        self.cameras[str(camera.camID)] = camera
        self.start_processing_thread(camera)
        # self.start_processing_process(camera)
        

    def remove_camera(self, camID):
        """remove a camera to the System and kill its processing thread"""
        
        camera = self.cameras[str(camID)]
        self.cameras[str(camID)] = None
        self.cameras.pop(str(camID), None)

        camera.makeItStop()

        processKey = str(camera.camID)
        if processKey in self.cameraProcessingThreads:
            process = self.cameraProcessingThreads[str(camera.camID)]
            # process.terminate()

        del camera
        del process
    

    def start_logger_process(self):
        """ start the process that consume the log items in queue """
        process = Process(target=SurveillanceLogger.logger_process, args=(self.surveillanceLogsQueue,))
        process.daemon = True
        process.start()


    def start_alert_threads(self):
        """ call this once to start a thread for generating alerts """
        self.alertThread = threading.Thread(name='alerts_process_thread_',target=self.alert_engine,args=())
        self.alertThread.daemon = False
        self.alertThread.start()


    def alert_engine(self):  
        """check alarm state -> check camera -> check event -> 
        either look for motion or look for detected faces -> take action"""

        logger.debug('Alert engine starting')
        while True:
           with self.alertsLock:
              for alert in self.alerts:
                logger.debug('checking alert')
                if alert.action_taken == False: # If action hasn't been taken for event 
                    if alert.alarmState != 'All':  # Check states
                        if  alert.alarmState == self.alarmState: 
                            logger.debug('checking alarm state')
                            alert.event_occurred = self.check_camera_events(alert)
                        else:
                          continue # Alarm not in correct state check next alert
                    else:
                        alert.event_occurred = self.check_camera_events(alert)
                else:
                    if (time.time() - alert.eventTime) > 300: # Reinitialize event 5 min after event accured
                        logger.info( "reinitiallising alert: " + alert.id)
                        alert.reinitialise()
                    continue 

           time.sleep(2) # Put this thread to sleep - let websocket update alerts if need be (i.e delete or add)
  
    def check_camera_events(self,alert):   
        """Used to check state of cameras
        to determine whether an event has occurred"""

        notifcationImagePath = os.path.join(AppEnv.notificationDir, "image.jpg")

        if alert.camera != 'All':  # Check cameras   
            logger.info( "alertTest" + alert.camera)
            camera = self.cameras[str(alert.camera)]
            
            if alert.event == 'Recognition': #Check events
                logger.info(  "checkingalertconf "+ str(alert.confidence) + " : " + alert.person)
                for person in list(camera.people.values()):
                    logger.info( "checkingalertconf "+ str(alert.confidence )+ " : " + alert.person + " : " + person.identity)
                    if alert.person == person.identity: # Has person been detected
                       
                        if alert.person == "unknown" and (100 - person.confidence) >= alert.confidence:
                            logger.info( "alertTest2" + alert.camera)
                            cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                            self.take_action(alert)
                            return True
                        elif person.confidence >= alert.confidence:
                            logger.info( "alertTest3" + alert.camera)
                            cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                            self.take_action(alert)
                            return True     
                return False # Person has not been detected check next alert       

            else:
                logger.info( "alertTest4" + alert.camera)
                if camera.motion == True: # Has motion been detected
                       logger.info( "alertTest5" + alert.camera)
                       cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                       self.take_action(alert)
                       return True
                else:
                  return False # Motion was not detected check next alert
        else:
            if alert.event == 'Recognition': # Check events
                with self.camerasLock :
                    for camID, camera in self.cameras.items(): # Look through all cameras
                        for person in list(camera.people.values()):
                            if alert.person == person.identity: # Has person been detected
                                if alert.person == "unknown" and (100 - person.confidence) >= alert.confidence:
                                    cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                                    self.take_action(alert)
                                    return True
                                elif person.confidence >= alert.confidence:
                                    cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                                    self.take_action(alert)
                                    return True
               
                return False # Person has not been detected check next alert   

            else:
                with self.camerasLock :
                    for camID, camera in self.cameras.items(): # Look through all cameras
                        if camera.motion == True: # Has motion been detected
                            cv2.imwrite(notifcationImagePath, camera.State['processing_frame'])#
                            self.take_action(alert)
                            return True

                return False # Motion was not detected check next camera

    def take_action(self, alert): 
        """Sends email alert and/or triggers the alarm"""

        logger.info( "!!! Taking action !!! == {}".format(alert.actions) )
        if alert.action_taken == False: # Only take action if alert hasn't accured - Alerts reinitialise every 5 min for now
            alert.eventTime = time.time()  
            if alert.actions['email_alert'] == 'true':
                logger.info( "email notification being sent")
                self.send_email_notification_alert(alert)
            if alert.actions['trigger_alarm'] == 'true':
                logger.info( "triggering alarm1")
                self.trigger_alarm()
                logger.info( "alarm1 triggered")
            alert.action_taken = True

    def send_email_notification_alert(self, alert):
        """ Code produced in this tutorial - http://naelshiab.com/tutorial-send-email-python/"""

        fromaddr = "videorecoga@gmail.com"
        toaddr = alert.emailAddress

        msg = MIMEMultipart()

        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "Aiiiland Video recognition"

        body = "NOTIFICATION ALERT:" +  alert.alertString + ""

        msg.attach(MIMEText(body, 'plain'))

        filename = "image.jpg"
        notifcationImagePath = os.path.join(AppEnv.notificationDir, filename)
        attachment = open(notifcationImagePath, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        msg.attach(part)

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(fromaddr, "Admi!23456")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()


    def add_face(self,name,image, upload):
      """Adds face to directory used for training the classifier"""

      if upload == False:
          path = AppEnv.alignedImagesDir
      else:
          path = AppEnv.trainingImagesDir
      num = 0
    
      peopleDir = AppEnv.pathJoin(path, name)
      if not os.path.exists(peopleDir):
        try:
          logger.info( "Creating New Face Dircectory: " + name)
          os.makedirs(peopleDir)
        except OSError:
          logger.info( OSError)
          return False
          pass
      else:
         num = len([nam for nam in os.listdir(peopleDir) if os.path.isfile(os.path.join(peopleDir, nam))])

      logger.info( "Writing Image To Directory: " + name)
      cv2.imwrite(peopleDir+"/"+ name + "_"+str(num) + ".png", image)
      self.load_names_to_memory()

      return True


    def load_names_to_memory(self):
      """Gets all the names that were most recently 
      used to train the classifier"""

      
      path = AppEnv.alignedImagesDir
      self.peopleDB = []
      for name in os.listdir(path):
        if (name == 'cache.t7' or name == '.DS_Store' or name[0:7] == 'unknown'):
          continue
        self.peopleDB.append(name)
        logger.info("Known faces in our db for: " + name + " ")
      self.peopleDB.append('unknown')

    def change_alarm_state(self):
      """Sends Raspberry PI a resquest to change the alarm state.
      192.168.1.35 is the RPI's static IP address port 5000 is used 
      to access the flask application."""

      r = requests.post('http://192.168.1.35:5000/change_state', data={"password": "admin"})
      alarm_states = json.loads(r.text)

      logger.info(alarm_states)
      if alarm_states['state'] == 1:
          self.alarmState = 'Armed' 
      else:
          self.alarmState = 'Disarmed'       
      self.alarmTriggerd = alarm_states['triggered']

    def trigger_alarm(self):
       """Sends Raspberry PI a resquest to change to trigger the alarm.
      192.168.1.35 is the RPI's static IP address port 5000 is used 
      to access the flask application."""

       r = requests.post('http://192.168.1.35:5000/trigger', data={"password": "admin"})
       alarm_states = json.loads(r.text) 
    
       logger.info(alarm_states)

       if alarm_states['state'] == 1:
           self.alarmState = 'Armed' 
       else:
           self.alarmState = 'Disarmed' 
       
       self.alarmTriggerd = alarm_states['triggered']
       logger.info(self.alarmTriggerd )





def getInstance():
    """ Obtain a singleton instance of  """
    global singleton

    if singleton is None:
        singleton = SurveillanceSystem()

    return singleton