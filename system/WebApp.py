import os
import sys

sys.path.append("../database")
sys.path.append("./modules")
sys.path.append("./processMethods")


from flask import Flask, render_template, Response, redirect, url_for, request, jsonify, send_file, session, g
from flask_uploads import UploadSet, configure_uploads, IMAGES
import Camera
from flask_socketio import SocketIO, send, emit 
from flask_cors import CORS
import SurveillanceSystem
import json
import logging
from logging.handlers import RotatingFileHandler
import threading
import time
from random import random
import cv2
import psutil
import shutil

import AppEnv
import RootDB
import PeopleModule
import CameraModule
import AuthModule
import SysMonitorModule
import AlertModule
import SettingsModule
import SurveillanceLogModule

import CameraDAO
import ClassifierDAO

from MyUtils import MyEncoder
import ImageUtils

import FaceRecogniserEmbedding

LOG_FILE = 'logs/WebApp.log'
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


AppEnv.useWeb()

AppEnv.createGeneratedDirIfNotFound()
ClassifierDAO.createDummyClassifierIfNotFound()
AppEnv.printOutPaths()



# print("{}".format(os.listdir(AppEnv.trainingImagesDir)))
# initialize training images
if not os.listdir(AppEnv.trainingImagesDir):
    try:
        exampleDirectories = os.listdir(AppEnv.exampleTrainingImagesDir)
        for exampleKey in exampleDirectories:
            shutil.copytree(os.path.join(AppEnv.exampleTrainingImagesDir, exampleKey), os.path.join(AppEnv.trainingImagesDir, exampleKey))
    # Directories are the same
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not copied. Error: %s' % e)



# Initialises system variables, this object is the heart of the application
HomeSurveillance = SurveillanceSystem.getInstance()
HomeSurveillance.start_alert_threads()
HomeSurveillance.start_logger_process()

# Threads used to continuously push data to the client
# alarmStateThread = threading.Thread() 
# facesUpdateThread = threading.Thread() 
# monitoringThread = threading.Thread() 
# alarmStateThread.daemon = False
# facesUpdateThread.daemon = False
# monitoringThread.daemon = False
# Flask setup
app = Flask('SurveillanceWebServer')
app.config['SECRET_KEY'] = os.urandom(24) # Used for session management

# for Cors
cors = CORS(app, supports_credentials=True, origins=['http://localhost:3000', 'localhost:3000', 'http://localhost:5000', 'localhost:5000'])
# logging.getLogger('flask_cors').level = logging.INFO

socketio = SocketIO(app)
photos = UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST'] = os.path.join(AppEnv.uploadsDir, "imgs")
configure_uploads(app, photos)

RootDB.RootDB(True)
SysMonitorModule.system_info(app)
# print(cv2.getBuildInformation())

@app.before_request
def before_request():
    """Initialise session"""
    g.user = None
    if 'user' in session:
        g.user = session['user']


class ApiError(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['msg'] = self.message
        return rv

@app.errorhandler(ApiError)
def handle_api_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response




################################
#  API DEFINITION START BELOW
################################


@app.route('/', methods=['GET','POST'])
def login_page():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('index.html')


AuthModule.initRoutes(app)
# PeopleModule.initRoutes(app)
PeopleModule.init(app, socketio, HomeSurveillance)
CameraModule.init(app, socketio, HomeSurveillance)

# SysMonitorModule.init(app, socketio, HomeSurveillance)
AlertModule.init(app, socketio, HomeSurveillance)
SettingsModule.init(app, socketio, HomeSurveillance)
SurveillanceLogModule.init(app, socketio, HomeSurveillance)

@app.route('/old_home')
def old_home():
    if g.user:
        return render_template('index_old.html')
    return render_template('login.html')

@app.route('/old_login', methods=['GET','POST'])
def old_login_page():
    error = None
    if request.method == 'POST':
        session.pop('user',None) # Drops session everytime user tries to login
        if not AuthModule.checkLogin(request.form):
            error = 'Invalid username or password. Please try again'
        else:
            session['user'] = request.form['username']
            return redirect(url_for('old_home'))
    return render_template('login.html', error = error)

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    message = "Nothing happened"

    if request.method == 'POST' and 'photo' in request.files:

        form = request.get_json()
        if form is None:
            form = request.form

        try:
            filename = photos.save(request.files['photo'])
            image = request.files['photo']
            name = form.get('name')
            image = cv2.imread(os.path.join(AppEnv.uploadsDir, "imgs", filename))
            # height, width, _ = image.shape
            # if width > 1200 or height > 1200:
            image = ImageUtils.resize(image)
            wriitenToDir = HomeSurveillance.add_face(name, image, upload = True)
            message = "file uploaded successfully"
        except:
            message = "file upload NOT successful"

    return jsonify({ 'msg': 'OK {} {}'.format(message, time.time()) })
        # return render_template('index.html', message = message)
    # if g.user:
    #     return render_template('index.html')
    # else:
    #     return redirect(url_for('login'))

def gen(camera):
    """Can read processed frame or unprocessed frame.
    When streaming the processed frame with read_processed()
    and setting the drawing variable in the SurveillanceSystem 
    class, you can see all detection bounding boxes. This
    however slows down streaming and therefore read_jpg()
    is recommended"""
    prevFrame = None
    while True:
        frame = camera.read_processed()
        # frame = camera.read_jpg()

        if frame is prevFrame:
            time.sleep(0.05) # throttle a bit so will not busy load
            continue
        
        prevFrame = frame
            
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')  # Builds 'jpeg' data with header and payload
        
        time.sleep(0.05) # throttle a bit so will not busy load

@app.route('/video_streamer/<camNum>')
def video_streamer(camNum):
    """Used to stream frames to client, camNum represents the camera index in the cameras array"""
    cam = HomeSurveillance.cameras.get(str(camNum), None)
    if cam is not None:
        return Response(gen(HomeSurveillance.cameras[str(camNum)]),
                        mimetype='multipart/x-mixed-replace; boundary=frame') # A stream where each part replaces the previous part the multipart/x-mixed-replace content type must be used.
    else:
        resp = Response(Camera.PAUSE_IMAGE, mimetype="image/jpeg")
        resp.headers["Cache-Control"] = "max-age=0"
        return resp




def cameras_updated():

    dao = CameraDAO.CameraDAO()
    cameras = dao.getAll()

    camerasList = list(map(lambda item: {
        'camNum': item.get("id"),
        'url': item.get("url"),
        'cameraFunction': item.get("method"),
        # 'dlibDetection': camera.dlibDetection,
        'label': item.get("label"),
        'created_at': item.get("created_at"),
        'activated': HomeSurveillance.cameras.get(str(item.get("id")), None) is not None
    }, cameras))

    socketio.emit('cameras_updated', {
        'cameras': camerasList
    }, namespace='/surveillance')


@app.route('/add_camera', methods = ['GET','POST'])
def add_camera():
    """Adds camera new camera to SurveillanceSystem's cameras array"""
    if request.method == 'POST':
        print("Adding a new camera")

        camURL = None
        application = None
        detectionMethod = None
        label = None
        fpsTweak = None

        form = request.get_json()
        if form is None:
            camURL = request.form.get('camURL')
            application = request.form.get('application')
            # detectionMethod = request.form.get('detectionMethod')
            detectionMethod = False
            fpsTweak = request.form.get('fpstweak')
            label = ""
        else:
            camURL = form['camURL']
            application = form['application']
            detectionMethod = form['detectionMethod']
            label = form.get('label', "")
            fpsTweak = form['fpstweak']


        app.logger.info("Addding a new camera with url: ")
        app.logger.info(camURL)
        # app.logger.info(fpsTweak)

        # Add to DB
        dao = CameraDAO.CameraDAO()
        newCameraObj = dao.add({
            'label':label,
            'method': application,
            'url': camURL,
        })

        app.logger.info(newCameraObj)

        newCamId = newCameraObj.get("id")

        #This will "start" the camera and processing
        with HomeSurveillance.camerasLock :
            HomeSurveillance.add_camera(Camera.IPCamera(newCamId, camURL, application, detectionMethod, fpsTweak, label))

        data = {"camNum": newCamId}

        app.logger.info("Camera added, emitting events to clients")

        cameras_updated()

        return jsonify(data)


@app.route('/remove_camera', methods = ['GET','POST'])
def remove_camera():
    if request.method == 'POST':

        camID = None

        form = request.get_json()
        if form is None:
            camID = request.form.get('camID')
        else:
            camID = form['camID']

        print("Remove a camera {}".format(camID))
        app.logger.info("Removing camera: ")
        app.logger.info(camID)
        # data = {"camNum": len(HomeSurveillance.cameras) - 1}

        # stop the processing if camera activated
        with HomeSurveillance.camerasLock:
            camera = HomeSurveillance.cameras.get(str(camID))
            if camera is not None:
                HomeSurveillance.remove_camera(camID)

        # remove from DB
        dao = CameraDAO.CameraDAO()
        dao.remove(camID)

        # trigger an update to client
        cameras_updated()

        data = {"alert_status": "removed"}
        return jsonify(data)

@app.route('/start_camera', methods = ['GET','POST'])
def start_camera():

    form = request.get_json()

    camID = form.get("camID")

    # Add to DB
    dao = CameraDAO.CameraDAO()
    cam = dao.get(camID)

    if cam is not None:
        #This will "start" the camera and processing
        with HomeSurveillance.camerasLock :
            HomeSurveillance.add_camera(Camera.IPCamera(camID, cam.get("url"), cam.get("method"), True, False, cam.get("label")))

    data = {"camNum": camID}
    cameras_updated()
    return jsonify(data)

@app.route('/stop_camera', methods = ['GET','POST'])
def stop_camera():

    form = request.get_json()

    camID = form.get("camID")

    # Add to DB
    dao = CameraDAO.CameraDAO()
    cam = dao.get(camID)

    if cam is not None:
        #This will "start" the camera and processing
        with HomeSurveillance.camerasLock:
            HomeSurveillance.remove_camera(camID)

    data = {"camNum": camID}
    cameras_updated()
    return jsonify(data)

@app.route('/remove_face', methods = ['GET','POST'])
def remove_face():
    if request.method == 'POST':
        predicted_name = request.form.get('predicted_name')
        camNum = request.form.get('camera')

        with HomeSurveillance.cameras[str(camNum)].peopleDictLock:
            try:
                del HomeSurveillance.cameras[str(camNum)].people[predicted_name]
                app.logger.info("==== REMOVED: " + predicted_name + "===")
            except Exception as e:
                app.logger.error("ERROR could not remove Face {}".format(e))
                pass

        data = {"face_removed":  'true'}
        return jsonify(data)
    return render_template('index.html')

@app.route('/add_face', methods = ['GET','POST'])
def add_face():
    if request.method == 'POST':
        trust = request.form.get('trust')
        new_name = request.form.get('new_name')
        person_id = request.form.get('person_id')
        camNum = request.form.get('camera')
        img = None
        
        with HomeSurveillance.cameras[str(camNum)].peopleDictLock:  
            try:  
                img = HomeSurveillance.cameras[str(camNum)].people[person_id].face   # Gets face of person detected in cameras 
                predicted_name = HomeSurveillance.cameras[str(camNum)].people[person_id].identity
                del HomeSurveillance.cameras[str(camNum)].people[person_id]    # Removes face from people detected in all cameras 
            except Exception as e:
                app.logger.error("ERROR could not add Face {}".format(e))
 
        #print "trust " + str(trust)
        app.logger.info("trust " + str(trust))
        if str(trust) == "false":
            wriitenToDir = HomeSurveillance.add_face(new_name,img, upload = False)
        else:
            wriitenToDir = HomeSurveillance.add_face(predicted_name,img, upload = False)

            

        systemData = {'camNum': len(HomeSurveillance.cameras.keys()) , 'people': HomeSurveillance.peopleDB, 'onConnect': False}
        socketio.emit('system_data', json.dumps(systemData) ,namespace='/surveillance')
           
        data = {"face_added":  wriitenToDir}
        return jsonify(data)
    return render_template('index.html')

@app.route('/retrain_classifier', methods = ['GET','POST'])
def retrain_classifier():
    if request.method == 'POST':
        app.logger.info("retrain button pushed. clearing event in surveillance objt and calling trainingEvent")
        HomeSurveillance.trainingEvent.clear() # Block processing threads
        retrained = HomeSurveillance.trainClassifier()#calling the module in FaceRecogniser to start training
        HomeSurveillance.trainingEvent.set() # Release processing threads       
        data = {"finished":  retrained}
        app.logger.info("Finished re-training")
        return jsonify(data)
    return render_template('index.html')

@app.route('/retrain_encodings', methods = ['GET','POST'])
def retrain_encodings():
    if request.method == 'POST':
        app.logger.info("retrain button pushed. clearing event in surveillance objt and calling trainingEvent")
        HomeSurveillance.trainingEvent.clear() # Block processing threads
        recogniserEmbedding = FaceRecogniserEmbedding.FaceRecogniserEmbedding()
        recogniserEmbedding.trainClassifier()
        HomeSurveillance.trainingEvent.set() # Release processing threads       
        data = {"finished":  True}
        app.logger.info("Finished re-training")
        return jsonify(data)
    

@app.route('/get_faceimg/<name>')
def get_faceimg(name):  
    key,camNum = name.split("_")
    try:
        camera = HomeSurveillance.cameras[str(camNum)]
        img = camera.people[key].thumbnail 
    except Exception as e:
        app.logger.error("Error cannot get face {}".format(key))
        img = None

    if img is None:
        img = PeopleModule.UNKNOW_PERSON_IMG

    resp = Response(img, mimetype="image/jpeg")
    resp.headers["Cache-Control"] = "no-cache"
    return resp


@app.route('/get_all_faceimgs/<name>')
def get_faceimgs(name):  
    key, camNum, imgNum = name.split("_")
    try:
        with HomeSurveillance.cameras[str(camNum)].peopleDictLock:
            img = HomeSurveillance.cameras[str(camNum)].people[key].thumbnails[imgNum] 
    except Exception as e:
        app.logger.error("Error cannot get face " + key)
        img = ""

    if img == "":
        img = PeopleModule.UNKNOW_PERSON_IMG

    return  Response((b'--frame\r\n'
                     b'Content-Type: image/jpeg\r\n\r\n' + img + b'\r\n\r\n'),
                    mimetype='multipart/x-mixed-replace; boundary=frame') 



def update_faces_thread():
    while True:
        update_faces()
        time.sleep(4)

def update_faces():
    """Used to push all detected faces to client"""
    peopledata = []
    thumbnail = None
    now = time.time()
    with HomeSurveillance.camerasLock :
        for i, camera in HomeSurveillance.cameras.items():
            with camera.peopleDictLock:
                for key, person in camera.people.items():
                    if now - person.time < 600:
                        persondict = person.toApiDict()
                        persondict['camera'] = i
                        persondict['thumbnailNum'] = len(person.thumbnails)

                        # persondict = {
                        #     'identity': key,
                        #     'confidence': person.confidence,
                        #     'camera': i, 
                        #     'timeD': person.time,
                        #     'prediction': person.predictLabel,
                        #     'thumbnailNum': len(person.thumbnails)
                        # }
                        app.logger.info(persondict)
                        peopledata.append(persondict)

    # FIXME: only emit when changed?
    # if len(peopledata) > 0:
    socketio.emit('people_detected', json.dumps(peopledata, cls=MyEncoder) ,namespace='/surveillance')




@socketio.on('request_update_faces', namespace='/surveillance')
def request_update_faces():
    """ execute update_faces when client request to have updates """
    update_faces()


@socketio.on('connect', namespace='/surveillance') 
def connect(): 
    
    # Need visibility of global thread object                
    # global alarmStateThread
    # global facesUpdateThread 
    # global monitoringThread

    #print "\n\nclient connected\n\n"
    app.logger.info("client connected")

    # if not alarmStateThread.isAlive():
    #     #print "Starting alarmStateThread"
    #     app.logger.info("Starting alarmStateThread")
    #     alarmStateThread = threading.Thread(name='alarmstate_process_thread_',target= alarm_state, args=())
    #     alarmStateThread.start()
   
    # if not facesUpdateThread.isAlive():
    #     print("Starting facesUpdateThread")
    #     app.logger.info("Starting facesUpdateThread")
    #     facesUpdateThread = threading.Thread(name='websocket_process_thread_',target= update_faces_thread, args=())
    #     facesUpdateThread.start()

    # if not monitoringThread.isAlive():
    #     #print "Starting monitoringThread"
    #     app.logger.info("Starting monitoringThread")
    #     monitoringThread = threading.Thread(name='monitoring_process_thread_',target= system_monitoring, args=())
    #     monitoringThread.start()

    # cameraData = {}
    # cameras = []

    # with HomeSurveillance.camerasLock :
    #     for i, camera in HomeSurveillance.cameras.items():
    #         with camera.peopleDictLock:
    #             cameraData = {
    #                 'camNum': i,
    #                 'url': camera.camURL,
    #                 'cameraFunction': camera.cameraFunction,
    #                 'dlibDetection': camera.dlibDetection,
    #                 'label': camera.label,
    #                 'created_at': camera.created_at
    #             }
    #             #print cameraData
    #             app.logger.info(cameraData)
    #             cameras.append(cameraData)

    dao = CameraDAO.CameraDAO()
    cameras = dao.getAll()

    camerasList = list(map(lambda item: {
        'camNum': item.get("id"),
        'url': item.get("url"),
        'cameraFunction': item.get("method"),
        # 'dlibDetection': camera.dlibDetection,
        'label': item.get("label"),
        'created_at': item.get("created_at"),
        'activated': HomeSurveillance.cameras.get(str(item.get("id")), None) is not None
    }, cameras))

    alertData = {}
    alerts = []
    # for i, alert in enumerate(HomeSurveillance.alerts):
    #     with HomeSurveillance.alertsLock:
    #         alertData = {'alert_id': alert.id , 'alert_message':  "Alert if " + alert.alertString}
    #         #print alertData
    #         app.logger.info(alertData)
    #         alerts.append(alertData)
   
    systemData = {'camNum': len(camerasList) , 'people': HomeSurveillance.peopleDB, 'cameras': camerasList, 'alerts': alerts, 'onConnect': True}
    socketio.emit('system_data', json.dumps(systemData) ,namespace='/surveillance')

@socketio.on('disconnect', namespace='/surveillance')
def disconnect():
    #print('Client disconnected')
    app.logger.info("Client disconnected")


if __name__ == '__main__':

    # setup logging
    # formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    # handler = RotatingFileHandler(LOG_FILE, maxBytes=1000000, backupCount=10)
    # handler.setLevel(logging.DEBUG)
    # handler.setFormatter(formatter)
    # app.logger.addHandler(handler)

    app.logger.setLevel(logging.DEBUG)

    # Enable The below code for more debug logs about web server
    # log = logging.getLogger('werkzeug')
    # log.setLevel(logging.DEBUG)
    # log.setLevel(logging.WARNING)
    # log.addHandler(handler)

    logging.getLogger("werkzeug").setLevel(logging.WARNING)
    logging.getLogger("socketio").setLevel(logging.WARNING)
    logging.getLogger("engineio").setLevel(logging.WARNING)

    print("Starting server at port 5000 ...")
    # Starts server on default port 5000 and makes socket connection available to other hosts (host = '0.0.0.0')
    # start the server
    socketio.run(app, host='0.0.0.0', debug=False, use_reloader=False)
    #  socketio.run(app)