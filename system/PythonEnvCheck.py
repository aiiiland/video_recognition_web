import os
import psutil
import multiprocessing
import cv2
import dlib

print("CPU Count: " + str( multiprocessing.cpu_count() ) )
print("CPU Core Count: " + str( psutil.cpu_count(logical=True) ) )
print( cv2.getBuildInformation() )



print("")
print("")
print("========= DLIB =========")
print("dlib version: " + str( dlib.__version__) )
print("DLIB_USE_CUDA: " + str( dlib.DLIB_USE_CUDA) )