import os
import sys

# sys.path.append("../database")
# sys.path.append("./modules")
# sys.path.append("./processMethods")


import json
import logging
from logging.handlers import RotatingFileHandler
import threading
import time
from random import random
import cv2
import psutil
import dlib
import face_recognition

import AppEnv


logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)






def train():
    people = os.listdir(AppEnv.trainingImagesDir)

    resultPeopleSet = []
    resultEncodingSet = []

    for person in people:
        if person == ".DS_Store":
            continue

        logger.info("Finding encoding of {}".format(person))
        
        personDir = os.path.join(AppEnv.trainingImagesDir, person)
        personImages = os.listdir(personDir)

        for imgFname in personImages:
            image = face_recognition.load_image_file(os.path.join(personDir, imgFname))
            face_encodings = face_recognition.face_encodings(image)
            if face_encodings is not None and len(face_encodings) > 0:
                face_encoding = face_encodings[0]

                resultPeopleSet.append(person)
                resultEncodingSet.append(face_encoding)
                # break the loop
                break

    return (resultPeopleSet, resultEncodingSet)



def run_test_detailed(testingDatasetDir, knownData ):
    """ Run Classification test on a set of data, with details results """
    
    (knownPeopleSet, knwonwEncodingSet) = knownData

    # Test Result
    testResult = dict()

    # Test
    testDataset = os.listdir(testingDatasetDir)

    for testPersonLabel in testDataset:
        if testPersonLabel == ".DS_Store":
            continue
        # print(testPersonLabel)
        personDir = os.path.join(testingDatasetDir, testPersonLabel)
        personImages = os.listdir(personDir)

        resultDict = dict()

        for imgFname in personImages:
            if imgFname == ".DS_Store":
                continue

            print("Testing on {} ...".format(imgFname))
            imgPath = os.path.join(personDir, imgFname)

            image = face_recognition.load_image_file(imgPath)
            testFaceEncodings = face_recognition.face_encodings(image)
            if testFaceEncodings is not None and len(testFaceEncodings) > 0:
                faceDistances = face_recognition.face_distance(knwonwEncodingSet, testFaceEncodings[0])

                min_distance = 99
                min_i = -1
                for i, face_distance in enumerate(faceDistances):
                    # print("The test image has a distance of {:.2} from known image #{}".format(face_distance, i))
                    if face_distance < min_distance:
                        min_distance = face_distance
                        min_i = i
                    
                if min_distance < 0.6:
                    result = (testPersonLabel, imgFname, knownPeopleSet[min_i], min_distance)
                else:
                    result = (testPersonLabel, imgFname, None, None)
            else:
                result = (testPersonLabel, imgFname, None, None)

            resultDict[imgFname] = result

        testResult[testPersonLabel] = resultDict

    return testResult


def gen_result_report_detailed(testName, testResult):

    totalPhotos = 0
    overallSuccess = 0
    overallFalsePositive = 0
    overallFalsePositiveStrict = 0

    testResultPath = os.path.join(AppEnv.storageDir, "test-results")
    outputPath = os.path.join(testResultPath, "{}.csv".format(testName))

    with open(outputPath, "w") as textFileWriter:

        print("-----------------")
        print(testName)
        print("-----------------")
        print("{},{},{},{}".format("Label", "Image", "PredictLabel", "Confidence"))
        textFileWriter.write("{},{},{},{}\n".format("Label", "Image", "PredictLabel", "Confidence"))
        for person in testResult.keys():

            personResult = testResult[person]

            total = len(personResult.keys())
            
            numSuccess = 0
            numFalsePositive = 0
            numFalsePositiveStrict = 0

            for img in personResult.keys():
                (testPersonLabel, imageName, predictLabel, faceDistance) = personResult[img]

                if testPersonLabel == predictLabel:
                    numSuccess = numSuccess + 1

                if predictLabel is not None and testPersonLabel != predictLabel and faceDistance <= 0.6:
                    numFalsePositive = numFalsePositive + 1
                
                if predictLabel is not None and testPersonLabel != predictLabel and faceDistance <= 0.5:
                    numFalsePositiveStrict = numFalsePositiveStrict + 1
                
                print("{},{},{},{}".format(testPersonLabel, imageName, predictLabel, faceDistance))
                textFileWriter.write("{},{},{},{}\n".format(testPersonLabel, imageName, predictLabel, faceDistance))
            
            totalPhotos = totalPhotos + total
            overallSuccess = overallSuccess + numSuccess
            overallFalsePositive = overallFalsePositive + numFalsePositive
            overallFalsePositiveStrict = overallFalsePositiveStrict + numFalsePositiveStrict

            # percent = (numSuccess / total) * 100
            # print("{},{},{}".format(person, numSuccess, total))

        print("-----------------")
        print("Total accuracy : {}%".format( (overallSuccess/totalPhotos)*100 ))
        print("Total false positive (0.6 distance) : {}%".format( (overallFalsePositive/totalPhotos)*100 ))
        print("Total false positive (0.5 distance) : {}%".format( (overallFalsePositiveStrict/totalPhotos)*100 ))



def main():
    print("")
    print("##############################################################")
    print(" This is the console interface of the AI Surveillence System ")
    print("#############################################################")
    print("")

    logger.setLevel(logging.DEBUG)

    (knownPeopleSet, knwonwEncodingSet) = train()

    # result = run_test_detailed(AppEnv.trainingImagesDir, (knownPeopleSet, knwonwEncodingSet))
    # gen_result_report_detailed("FaceEncodingTraingDataTest", result)

    result = run_test_detailed(os.path.join(AppEnv.storageDir, "test-images"), (knownPeopleSet, knwonwEncodingSet))
    gen_result_report_detailed("FaceEncodingUnknownDataTest", result)





if __name__ == "__main__":
    main()

