#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os.path
import re
import sys
import cv2

import AppEnv

# Add internal libs
from libs.shape import Shape
from libs.labelFile import LabelFile, LabelFileError
from libs.pascal_voc_io import PascalVocReader
from libs.pascal_voc_io import XML_EXT
from libs.yolo_io import YoloReader
from libs.yolo_io import TXT_EXT

FORMAT_PASCALVOC='PscalVOC'
FORMAT_YOLO='YOLO'

class CreateAnnotation(object):

    def __init__(self, defaultLoadDir=AppEnv.alignedImagesDir, defaultSaveDir="yolo-aligned-txt", defaultFormat=FORMAT_YOLO):

        print("annotation init")
        # Save as Pascal voc xml
        self.defaultLoadDir = defaultLoadDir
        self.defaultSaveDir = defaultSaveDir
        if not os.path.exists(defaultSaveDir):
            os.makedirs(defaultSaveDir)
        self.usingPascalVocFormat = True
        self.usingYoloFormat = False

        # Application state.
        self.image = None
        self.filePath = defaultLoadDir
        self.imageShapes = []
        # Add Chris
        self.difficult = False
        self.labelHist = []
        self.labelFile = LabelFile()

        if defaultFormat == FORMAT_PASCALVOC:
            self.usingPascalVocFormat = True
            self.usingYoloFormat = False
            self.labelFile.suffix = XML_EXT

        elif defaultFormat == FORMAT_YOLO:
            self.usingPascalVocFormat = False
            self.usingYoloFormat = True
            self.labelFile.suffix = TXT_EXT


    ## process the annotation ##
    def startAutoAnnotation(self):
        
        print("annotation startauto")
        #do auto annotation to all the captured photos
        saveDir = self.defaultSaveDir
        for root, dirs, files in os.walk(self.defaultLoadDir):  
            #loop via all the available person directorys
            print("annotation os.walk")
            for person in dirs:
                print(person)
                #loop via all the available person photos
                for filename in os.listdir(os.path.join(root, person)):
                    path = os.path.join(root, person)
                    targetDir = os.path.join(saveDir, person)
                    if not os.path.exists(targetDir):
                        os.makedirs(targetDir)
                    if os.path.isfile(os.path.join(path, filename)):
                        print(filename)
                        self.filePath = os.path.join(path, filename)
                        self.image = cv2.imread(self.filePath)
                        
                        #set the dummy annotation to the full size of the photo
                        self.imageShapes = [] #one photo with one shape in this case
                        height, width, channels = self.image.shape
                        shape = Shape(label=person)
                        shape.addPoint((0, 0))
                        shape.addPoint((width, 0))
                        shape.addPoint((width, height))
                        shape.addPoint((0, height))
                        shape.difficult = self.difficult
                        shape.close()
                        self.imageShapes.append(shape)
                        if person not in self.labelHist:
                            self.labelHist.append(person)                        

                        module_name, ext = os.path.splitext(filename)
                        targetFilename = os.path.join(targetDir, module_name)

                        #create the annotatioFile
                        self.saveLabels(targetFilename)
        return len(self.labelHist)


    def saveLabels(self, annotationFilePath):

        print("annotation saveLabels")
        if self.labelFile is None:
            self.labelFile = LabelFile()
            self.verifyImg()

        #def format_shape(s):
        #    return dict(label=s.label,
        #                points=[(p.x(), p.y()) for p in s.points],
        #               # add chris
        #                difficult = s.difficult)

        #shapes = [format_shape(shape) for shape in self.imageShapes]
        shapes = self.imageShapes
        # Can add differrent annotation formats here
        try:
            if self.usingPascalVocFormat is True:
                if annotationFilePath[-4:] != ".xml":
                    annotationFilePath += XML_EXT
                print ('Img: ' + self.filePath + ' -> Its xml: ' + annotationFilePath)
                if not os.path.exists(annotationFilePath):
                    self.labelFile.savePascalVocFormat(annotationFilePath, shapes, self.filePath)
            elif self.usingYoloFormat is True:
                if annotationFilePath[-4:] != ".txt":
                    annotationFilePath += TXT_EXT
                print ('Img: ' + self.filePath + ' -> Its txt: ' + annotationFilePath)
                if not os.path.exists(annotationFilePath):
                    self.labelFile.saveYoloFormat(annotationFilePath, shapes, self.filePath, self.labelHist)
            #else:
            #    self.labelFile.save(annotationFilePath, shapes, self.filePath, self.imageData)
            return True
        except LabelFileError as e:
            print(u'Error saving label data', u'<b>%s</b>' % e)
            return False

    def verifyImg(self, _value=False):
        # check the file
         if self.filePath is not None and os.path.isfile(self.filePath):
            try:
                self.labelFile.toggleVerify()
            except AttributeError:
                # If the labelling file does not exist yet, create if and
                # re-save it with the verified attribute.
                self.labelFile.toggleVerify()

    def loadPascalXMLByFilename(self, xmlPath):
        if self.filePath is None:
            return
        if os.path.isfile(xmlPath) is False:
            return

        self.set_format(FORMAT_PASCALVOC)

        tVocParseReader = PascalVocReader(xmlPath)
        self.imageShapes = tVocParseReader.getShapes()

    def loadYOLOTXTByFilename(self, txtPath):
        if self.filePath is None:
            return
        if os.path.isfile(txtPath) is False:
            return

        self.set_format(FORMAT_YOLO)
        tYoloParseReader = YoloReader(txtPath, self.image)
        self.imageShapes = tYoloParseReader.getShapes()

    ## Support Functions ##
    def set_format(self, save_format):
        if save_format == FORMAT_PASCALVOC:
            self.usingPascalVocFormat = True
            self.usingYoloFormat = False
            self.labelFile.suffix = XML_EXT

        elif save_format == FORMAT_YOLO:
            self.usingPascalVocFormat = False
            self.usingYoloFormat = True
            self.labelFile.suffix = TXT_EXT

def main():
    """
    main
    """
    ##test
    print("annotation start")
    annotation = CreateAnnotation(defaultLoadDir=AppEnv.alignedImagesDir, defaultSaveDir="yolo-aligned-txt", defaultFormat=FORMAT_YOLO)
    annotation.startAutoAnnotation() 


if __name__ == "__main__":
    main()