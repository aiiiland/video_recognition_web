# Camera Class
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# import threading
import multiprocessing
from multiprocessing import Process, Queue, Pipe, Event, Lock, Manager

import time
import numpy as np
import cv2
import ImageUtils
import dlib
import openface
import os
import argparse
import logging
from logging.handlers import RotatingFileHandler
import SurveillanceSystem
import FaceDetector
# from queue import Queue
from app_utils import FPS, WebcamVideoStream
import sys
from datetime import datetime

#logging.basicConfig(level=logging.DEBUG,
#                    format='(%(threadName)-10s) %(message)s',
#                    )
logger = logging.getLogger(__name__)

# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
# LOG_FILE = 'logs/Camera.log'
# formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# handler = RotatingFileHandler(LOG_FILE, maxBytes=1000000, backupCount=10)
# handler.setLevel(logging.DEBUG)
# handler.setFormatter(formatter)
# logger.addHandler(handler)
logger.setLevel(logging.DEBUG)



CAPTURE_HZ = 30.0 # Determines frame rate at which frames are captured from IP camera

class VideoFetcher(object):
	""" The actual video fetcher """

	def __init__(self, camURL, queue, store, runningFlag):
		self.camURL = camURL
		self.queue = queue
		self.store = store
		self.runningFlag = runningFlag

		# self.video = cv2.VideoCapture(0)
		self.video = video = cv2.VideoCapture(camURL)
		# self.video = self.open_cam_rtsp(camURL, 1080, 720, 0)
		# self.video.set(cv2.CAP_PROP_FRAME_WIDTH, 480)
		# self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, 320)
		# video.set(cv2.CAP_PROP_FPS, 10)

	def open_cam_rtsp(self, uri, width, height, latency):
		gst_str = ("rtspsrc location={} latency={} ! rtph264depay ! h264parse ! omxh264dec ! "
				"nvvidconv ! video/x-raw, width=(int){}, height=(int){}, format=(string)BGRx ! "
				"videoconvert ! appsink").format(uri, latency, width, height)
		return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

	def open_cam_usb(self, dev, width, height):
		# We want to set width and height here, otherwise we could just do:
		#     return cv2.VideoCapture(dev)
		gst_str = ("v4l2src device=/dev/video{} ! "
				"video/x-raw, width=(int){}, height=(int){}, format=(string)RGB ! "
				"videoconvert ! appsink").format(dev, width, height)
		return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

	def open_cam_onboard(self, width, height):
		# On versions of L4T previous to L4T 28.1, flip-method=2
		# Use Jetson onboard camera
		gst_str = ("nvcamerasrc ! "
				"video/x-raw(memory:NVMM), width=(int)2592, height=(int)1458, format=(string)I420, framerate=(fraction)30/1 ! "
				"nvvidconv ! video/x-raw, width=(int){}, height=(int){}, format=(string)BGRx ! "
				"videoconvert ! appsink").format(width, height)
		return cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)

	def loop(self):
		""" Main loop of fetching frames """
		if not self.video.isOpened():
			if not self.__openVideoWithRetry():
				# when reach here, that means the program has retried 20 times
				return -1


		lastSucessTime = time.time()
		while True:
			try:

				# logger.info("Capture thread running")

				if self.runningFlag.value != 1:
					logger.info("Capture thread terminate ...")
					self.video.release()
					# break the loop
					return 0

				if not self.video.isOpened():
					logger.info("Connection broken during loop, retry from the beginning")
					self.video.release()
					# break the loop
					return -1

				if self.queue.full():
					# logger.debug('Queue fulled, drop one frame')
					self.queue.get()

				# logger.debug('Getting frame')

				# read the next frame from the file
				(success, frame) = self.video.read()

				success = True
				if success:
					# mark success
					lastSucessTime = time.time()

					if (np.shape(frame) == ()):
						continue

					# add the frame to the queue
					self.store['lastFrame'] = frame
					self.queue.put(frame)

					# throttle a bit so will not busy load
					# max fps is 25
					time.sleep(0.04)

				if time.time() - lastSucessTime > 5:
					logger.info("Video down for more than 5 seconds")
					# break the loop
					return -1
						
			except Exception as e:
				logger.error("Caught exception in capture process: {}".format(e))
				# throttle a bit so will not busy load
				time.sleep(0.04)
		

	def __openVideoWithRetry(self):
		failCount = 0
		while not self.video.isOpened():
			if failCount > 20:
				logger.error("Failed to open video too many times")
				return False

			logger.info("Opening the video ...")
			isOpened = self.video.open(self.camURL)
			if not isOpened:
				failCount = failCount+1
				time.sleep(1)
			else:
				break

		return True


def video_fetcher(camURL, queue, store, runningFlag):
	""" Video fetcher Process starter """

	while True:

		# The program will stay in loop() until it has a problem or terminated
		# if runningFlag is 1, the loop continues, exit otherwise
		if runningFlag.value != 1:
			break

		# Main code
		fetcher = VideoFetcher(camURL, queue, store, runningFlag)
		retval = fetcher.loop()

		if retval == -1:
			logger.info("Restarting in 30 seconds ...")
			time.sleep(30)
		if retval == 0:
			logger.info("Terminated")
			break

	return 0



_, jpeg = cv2.imencode('.jpg', cv2.imread("static/img/green.jpg"))
NOFRAME_IMAGE = jpeg.tostring()

_, jpeg = cv2.imencode('.jpg', cv2.imread("static/img/test_screen.jpg"))
TEST_IMAGE = jpeg.tostring()

_, jpeg = cv2.imencode('.jpg', cv2.imread("static/img/pause_screen.jpg"))
PAUSE_IMAGE = jpeg.tostring()



class IPCamera(object):
	"""The IPCamera object continually captures frames
	from a camera and makes these frames available for
	proccessing and streamimg to the web client. A 
	IPCamera can be processed using 5 different processing 
	functions detect_motion, detect_recognise, 
	motion_detect_recognise, segment_detect_recognise, 
	detect_recognise_track. These can be found in the 
	SureveillanceSystem object, within the process_frame function"""

	def __init__(self, camID, camURL, cameraFunction, dlibDetection, fpsTweak, label):
		logger.info("Loading Stream From IP Camera: " + camURL)

		self.camID = camID
		self.camURL = camURL
		self.cameraFunction = cameraFunction 
		self.dlibDetection = dlibDetection # Used to choose detection method for camera (dlib - True vs opencv - False)
		self.fpsTweak = fpsTweak # used to know if we should apply the FPS work around when you have many cameras
		self.label = label
		self.created_at = time.time()

		#self.tempFrame = None
		#self.captureFrame  = None
		self.streamingFPS = 0 # Streaming frame rate per second
		self.processingFPS = 0
		self.FPSstart = time.time()
		self.FPScount = 0
		self.motion = False # Used for alerts and transistion between system states i.e from motion detection to face detection


		self.processManager = SurveillanceSystem.getInstance().getManager()
		self.State = self.processManager.dict()

		self.people = self.processManager.dict() # Holds person ID and corresponding person object 
		self.trackers = [] # Holds all alive trackers
		
		# self.rgbFrame = None
		self.faceBoxes = None
		# self.captureEvent = Event()
		# self.captureEvent.set()
		self.peopleDictLock = Lock() # Used to block concurrent access to people dictionary
		
		self.personLabel = 'temp' + datetime.now().strftime('%Y%m%d%H%M')

		# self.video = None
		
		self.queueSize=32
		# initialize the queue used to store frames read from
		# the video file
		self.Q = Queue(maxsize=self.queueSize)


		
		# self.lastFrame = None
		# self.processing_frame = None
		self.initCameraProcess()

		# Start a thread to continuously capture frames.
		# The capture thread ensures the frames being processed are up to date and are not old
		self.captureLock = Lock() # Sometimes used to prevent concurrent access
		# self.captureThread = threading.Thread(name='video_captureThread',target=self.get_frame)
		# self.captureThread.daemon = True
		# self.captureThread.start()
		# self.captureThread.stop = False



	def __del__(self):
		# self.video.release()
		if self.process.is_alive():
			self.makeItStop()

	def makeItStop(self):
		# self.captureThread.stop = True
		self.runningFlag.value = 0
		# self.process.terminate()

	def initCameraProcess(self):
		logger.info("!!! initCameraProcess called !!!")
		
		self.State['lastFrame'] = None
		self.State['processing_frame'] = None
		
		self.runningFlag = multiprocessing.Value("i")
		self.runningFlag.value = 1

		self.process = Process(target=video_fetcher, args=(self.camURL, self.Q, self.State, self.runningFlag))
		self.process.daemon = True
		self.process.start()


	def read_jpg(self):
		"""We are using Motion JPEG, and OpenCV captures raw images,
		so we must encode it into JPEG in order to stream frames to
		the client. It is nessacery to make the image smaller to
		improve streaming performance"""

		try:

			frame = self.Q.get(True, 1.0)

			#frame = ImageUtils.resize_mjpeg(frame)
			ret, jpeg = cv2.imencode('.jpg', frame)
			return jpeg.tostring()

		except Exception as e:
			lastFrame = self.State['lastFrame']
			if lastFrame is not None:
				ret, jpeg = cv2.imencode('.jpg', lastFrame)
				return jpeg.tostring()
			else:
				return NOFRAME_IMAGE

	def read_processed(self):
		frame = self.State['processing_frame']
		if frame is None:
			return NOFRAME_IMAGE
		else:
			#frame = ImageUtils.resize_mjpeg(frame)
			ret, jpeg = cv2.imencode('.jpg', frame)
			return jpeg.tostring()


	def dump_video_info(self, video):
		logger.info("---------Dumping video feed info---------------------")
		logger.info("Position of the video file in milliseconds or video capture timestamp: ")
		logger.info(video.get(cv2.CAP_PROP_POS_MSEC))
		logger.info("0-based index of the frame to be decoded/captured next: ")
		logger.info(video.get(cv2.CAP_PROP_POS_FRAMES))
		logger.info("Relative position of the video file: 0 - start of the film, 1 - end of the film: ")
		logger.info(video.get(cv2.CAP_PROP_POS_AVI_RATIO))
		logger.info("Width of the frames in the video stream: ")
		logger.info(video.get(cv2.CAP_PROP_FRAME_WIDTH))
		logger.info("Height of the frames in the video stream: ")
		logger.info(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
		logger.info("Frame rate:")
		logger.info(video.get(cv2.CAP_PROP_FPS))
		logger.info("4-character code of codec.")
		logger.info(video.get(cv2.CAP_PROP_FOURCC))
		logger.info("Number of frames in the video file.")
		logger.info(video.get(cv2.CAP_PROP_FRAME_COUNT))
		logger.info("Format of the Mat objects returned by retrieve() .")
		logger.info(video.get(cv2.CAP_PROP_FORMAT))
		logger.info("Backend-specific value indicating the current capture mode.")
		logger.info(video.get(cv2.CAP_PROP_MODE))
		logger.info("Brightness of the image (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_BRIGHTNESS))
		logger.info("Contrast of the image (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_CONTRAST))
		logger.info("Saturation of the image (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_SATURATION))
		logger.info("Hue of the image (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_HUE))
		logger.info("Gain of the image (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_GAIN))
		logger.info("Exposure (only for cameras).")
		logger.info(video.get(cv2.CAP_PROP_EXPOSURE))
		logger.info("Boolean flags indicating whether images should be converted to RGB.")
		logger.info(video.get(cv2.CAP_PROP_CONVERT_RGB))
		logger.info("--------------------------End of video feed info---------------------")
