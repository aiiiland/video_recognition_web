#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys


class Shape(object):
    def __init__(self, label=None, difficult=False):
        self.label = label
        self.points = []
        self.difficult = difficult
        self._closed = False
    
    def distance(p):
        return sqrt(p.x() * p.x() + p.y() * p.y())

    def close(self):
        self._closed = True

    def reachMaxPoints(self):
        if len(self.points) >= 4:
            return True
        return False

    def addPoint(self, point):
        if not self.reachMaxPoints():
            self.points.append(point)

    def popPoint(self):
        if self.points:
            return self.points.pop()
        return None

    def isClosed(self):
        return self._closed

    def setOpen(self):
        self._closed = False

    def nearestVertex(self, point, epsilon):
        for i, p in enumerate(self.points):
            if distance(p - point) <= epsilon:
                return i
        return None

    def containsPoint(self, point):
        return self.makePath().contains(point)

#    def makePath(self):
#        path = Path(self.points[0])
#        for p in self.points[1:]:
#            path.lineTo(p)
#        return path
#
#    def boundingRect(self):
#        return self.makePath().boundingRect()

    def moveBy(self, offset):
        self.points = [p + offset for p in self.points]

    def moveVertexBy(self, i, offset):
        self.points[i] = self.points[i] + offset

    def copy(self):
        shape = Shape("%s" % self.label)
        shape.points = [p for p in self.points]
        shape._closed = self._closed
        shape.difficult = self.difficult
        return shape

    def __len__(self):
        return len(self.points)
