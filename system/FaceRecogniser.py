# FaceRecogniser.
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Code used in this project included opensource software (openface)
# developed by Brandon Amos
# Copyright 2015-2016 Carnegie Mellon University

import cv2
import numpy as np
import os
import glob
import dlib
import sys
import argparse
from PIL import Image
import pickle
import math
import datetime
import threading
import logging
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
# from sklearn.grid_search import GridSearchCV
from sklearn.manifold import TSNE
from sklearn.svm import SVC
from sklearn.mixture import GMM
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import time
from operator import itemgetter
from datetime import datetime, timedelta
from operator import itemgetter
from sklearn.preprocessing import LabelEncoder
import atexit
from subprocess import Popen, PIPE
import os.path
import numpy as np
import pandas as pd
import aligndlib
import openface
import face_recognition
from imutils import paths

import AppEnv

logger = logging.getLogger(__name__)

start = time.time()
np.set_printoptions(precision=2)

systemDir = AppEnv.systemDir
luaDir = AppEnv.luaDir
modelDir = AppEnv.modelDir
dlibModelDir = AppEnv.dlibModelDir
openfaceModelDir = AppEnv.openfaceModelDir





class FaceRecogniser(object):
    """This class implements face recognition using Openface's
    pretrained neural network and a Linear SVM classifier. Functions
    below allow a user to retrain the classifier and make predictions
    on detected faces"""

    def __init__(self):
        self.args = args = AppEnv.getArgs()
        self.net = openface.TorchNeuralNet(args.networkModel, imgDim=args.imgDim,cuda=args.cuda)
        self.align = openface.AlignDlib(args.dlibFacePredictor)
        self.neuralNetLock = threading.Lock()
        self.predictor = dlib.shape_predictor(args.dlibFacePredictor)

        logger.info("Opening classifier.pkl to load existing known faces db")
        if os.path.isfile(AppEnv.classifierPath):
            with open(AppEnv.classifierPath, 'rb') as f: # le = labels, clf = classifier
                (self.labels, self.classifier) = pickle.load(f, encoding='latin1') # Loads labels and classifier SVM or GMM

    def confirmPredition(self, rgbFrame, name):

        try:
            # double confirm by face_recognition to prevent wrong unknown detection
            detected_face_encoding = face_recognition.face_encodings(rgbFrame)[0]
        except Exception as e:
            return "unknown"

        face_path = os.path.join(AppEnv.trainingImagesDir, name)
        print(face_path)
        imagePaths = list(paths.list_images(face_path))
        # load the input image and convert it from RGB (OpenCV ordering)
        # to dlib ordering (RGB)
        image = cv2.imread(imagePaths[0])
        rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        face_encoding = None
        try:
            # compute the facial embedding for the face
            face_encoding = face_recognition.face_encodings(rgb)[0]
        except Exception as e:
            return "unknown"

        results = face_recognition.compare_faces([face_encoding], detected_face_encoding, tolerance=0.48)

        if results[0] == True:
            print("The face is confirmed")
            return name
        else:
            print("The face is not confirmed")
            return "unknown"

    def make_prediction(self,rgbFrame,bb):
        """The function uses the location of a face
        to detect facial landmarks and perform an affine transform
        to align the eyes and nose to the correct positiion.
        The aligned face is passed through the neural net which
        generates 128 measurements which uniquly identify that face.
        These measurements are known as an embedding, and are used
        by the classifier to predict the identity of the person"""

        landmarks = self.align.findLandmarks(rgbFrame, bb)
        if landmarks == None:
            logger.debug("///  FACE LANDMARKS COULD NOT BE FOUND  ///")
            return None
        alignedFace = self.align.align(self.args.imgDim, rgbFrame, bb,landmarks=landmarks,landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if alignedFace is None:
            logger.debug("///  FACE COULD NOT BE ALIGNED  ///")
            return None

        logger.debug("////  FACE ALIGNED  // ")
        with self.neuralNetLock :
            persondict = self.recognize_face(alignedFace)

        if persondict is None:
            logger.debug("/////  FACE COULD NOT BE RECOGNIZED  //")
            return persondict, alignedFace
        else:
            logger.debug("/////  FACE RECOGNIZED  /// ")
            return persondict, alignedFace

    def recognize_face(self, alignedFaceImg):
        """ Main flow for tagging a image with a predicated name """

        if self.getRep(alignedFaceImg) is None:
            return None
        rep1 = self.getRep(alignedFaceImg) # Gets embedding representation of image
        logger.debug("Embedding returned. Reshaping the image and flatting it out in a 1 dimension array.")
        rep = rep1.reshape(1, -1)   #take the image and  reshape the image array to a single line instead of 2 dimensionals
        start = time.time()
        logger.debug("Submitting array for prediction.")
        predictions = self.classifier.predict_proba(rep).ravel() # Computes probabilities of possible outcomes for samples in classifier(clf).
        #logger.info("We need to dig here to know why the probability are not right.")
        maxI = np.argmax(predictions)
        person1 = self.labels.inverse_transform(maxI)
        #person1 = person1.decode('utf-8') #only when python3 opening a python2 object
        confidence1 = int(math.ceil(predictions[maxI]*100))

        logger.info("Recognition took {} seconds.".format(time.time() - start))
        # logger.info("Recognized {} with {:.2f} confidence.".format(person1, confidence1))

        person1 = self.confirmPredition(alignedFaceImg, person1)

        persondict = {'name': person1, 'confidence': confidence1, 'rep':rep1}
        print ("Recognized P: " + str(person1) + " C: " + str(confidence1))
        return persondict

    def getRep(self,alignedFace):
        bgrImg = alignedFace
        if bgrImg is None:
            logger.error("unable to load image")
            return None

        logger.debug("Tweaking the face color ")
        alignedFace = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
        start = time.time()
        logger.debug("Getting embedding for the face")
        rep = self.net.forward(alignedFace) # Gets embedding - 128 measurements
        return rep

    def getBBs(self,bgrImg):
        if bgrImg is None:
            raise Exception("Unable to load image/frame")

        rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
        start = time.time()

        # Get the largest face bounding box
        # bb = align.getLargestFaceBoundingBox(rgbImg) #Bounding box

        # Get all bounding boxes
        bb = self.align.getAllFaceBoundingBoxes(rgbImg)

        if bb is None:
            # raise Exception("Unable to find a face: {}".format(imgPath))
            return None
        logger.info("Faces detection took {} seconds.".format(time.time() - start))

        return bb

    def reloadClassifier(self):
        with open(AppEnv.classifierPath, 'rb') as f: # Reloads character stream from pickle file
            (self.labels, self.classifier) = pickle.load(f, encoding='latin1') # Loads labels and classifier SVM or GMM
        logger.info("reloadClassifier called")
        return True

    def trainClassifier(self):
        """Trainng the classifier begins by aligning any images in the
        training-images directory and putting them into the aligned images
        directory. Each of the aligned face images are passed through the
        neural net and the resultant embeddings along with their
        labels (names of the people) are used to train the classifier
        which is saved to a pickle file as a character stream"""

        logger.info("trainClassifier called")

        cachePath = AppEnv.pathJoin(AppEnv.alignedImagesDir, "cache.t7")
        try:
            os.remove(cachePath) # Remove cache from aligned images folder
        except:
            logger.info("Failed to remove cache.t7. Could be that it did not existed in the first place.")
            pass

        start = time.time()
        aligndlib.alignMain(AppEnv.trainingImagesDir, AppEnv.alignedImagesDir, "outerEyesAndNose", self.args.dlibFacePredictor, self.args.imgDim)
        logger.info("Aligning images for training took {} seconds.".format(time.time() - start))
        done = False
        start = time.time()

        done = self.generate_representation()

        if done is True:
            logger.info("Representation Generation (Classification Model) took {} seconds.".format(time.time() - start))
            start = time.time()
            # Train Model
            self.train(AppEnv.generatedEmbeddingsDir,"LinearSvm",-1)
            logger.info("Training took {} seconds.".format(time.time() - start))
        else:
            logger.info("Generate representation did not return True")


    def generate_representation(self):
        """ Generate representation data and output to "generated-embeddings" folder """

        logger.info("lua Directory:    " + luaDir)
        self.cmd = ['/usr/bin/env', 'th', os.path.join(luaDir, 'main.lua'),'-outDir',  AppEnv.generatedEmbeddingsDir , '-data', AppEnv.alignedImagesDir]
        logger.info("lua command:    " + str(self.cmd))
        if self.args.cuda:
            self.cmd.append('-cuda')
            logger.info("using -cuda")
        self.p = Popen(self.cmd, stdin=PIPE, stdout=PIPE, bufsize=0)
        #our issue is here, torch probably crashes without giving much explanation.
        outs, errs = self.p.communicate() # Wait for process to exit - wait for subprocess to finish writing to files: labels.csv & reps.csv
        logger.info("Waiting for process to exit to finish writing labels and reps.csv" + str(outs) + " - " + str(errs))

        def exitHandler():
            if self.p.poll() is None:
                logger.info("<=Something went Wrong===>")
                self.p.kill()
                return False
        atexit.register(exitHandler)

        return True


    def train(self,workDir,classifier,ldaDim):
        """ Use representation and labels csv, output the classifier.pkl  """

        # define the paths
        labelsCsvPath = AppEnv.labelsCsvPath
        repsCsvPath = AppEnv.repsCsvPath
        classifierPath = AppEnv.classifierPath


        fname = labelsCsvPath #labels of faces
        logger.info("Loading labels " + fname + " csv size: " +  str(os.path.getsize(repsCsvPath)))
        if os.path.getsize(fname) > 0:
            logger.info(fname + " file is not empty")
            labels = pd.read_csv(fname, header=None).as_matrix()[:, 1]
            logger.info(labels)
        else:
            logger.info(fname + " file is empty")
            labels = "1:aligned-images/dummy/1.png"  #creating a dummy string to start the process
        logger.debug(list(map(os.path.dirname, labels)))
        logger.debug(list(map(os.path.split,list(map(os.path.dirname, labels)))))
        logger.debug(list(map(itemgetter(1),list(map(os.path.split,list(map(os.path.dirname, labels)))))))
        labels = list(map(itemgetter(1),list(map(os.path.split,list(map(os.path.dirname, labels))))))

        fname = repsCsvPath # Representations of faces
        # fnametest = format(workDir) + "reps.csv"
        logger.info("Loading embedding " + fname + " csv size: " + str(os.path.getsize(fname)))
        if os.path.getsize(fname) > 0:
            logger.info(fname + " file is not empty")
            embeddings = pd.read_csv(fname, header=None).as_matrix() # Get embeddings as a matrix from reps.csv
        else:
            logger.info(fname + " file is empty")
            embeddings = np.zeros((2,150)) #creating an empty array since csv is empty

        self.labels = LabelEncoder().fit(labels) # LabelEncoder is a utility class to help normalize labels such that they contain only values between 0 and n_classes-1
        # Fits labels to model
        labelsNum = self.labels.transform(labels)
        nClasses = len(self.labels.classes_)
        logger.info("Training for {} classes.".format(nClasses))

        if classifier == 'LinearSvm':
            self.classifier = SVC(C=1, kernel='linear', probability=True)
        elif classifier == 'GMM':
            self.classifier = GMM(n_components=nClasses)

        if ldaDim > 0:
            clf_final =  self.classifier
            self.classifier = Pipeline([('lda', LDA(n_components=ldaDim)),
                ('clf', clf_final)])

        self.classifier.fit(embeddings, labelsNum) #link embeddings to labels

        fName = classifierPath
        logger.info("Saving classifier to '{}'".format(fName))
        with open(fName, 'wb') as f:
            pickle.dump((self.labels,  self.classifier), f) # Creates character stream and writes to file to use for recognition


    def getSquaredl2Distance(self,rep1,rep2):
        """Returns number between 0-4, Openface calculated the mean between
        similar faces is 0.99 i.e. returns less than 0.99 if reps both belong
        to the same person"""
        
        d = rep1 - rep2
        #print("Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))        
        return float(np.dot(d, d))