import cv2
import numpy as np
import os
import glob
import dlib
import sys
import argparse
from PIL import Image
import pickle
import math
import datetime
import threading
import logging
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
# from sklearn.grid_search import GridSearchCV
from sklearn.manifold import TSNE
from sklearn.svm import SVC
from sklearn.mixture import GMM
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import time
from operator import itemgetter
from datetime import datetime, timedelta
from operator import itemgetter
from sklearn.preprocessing import LabelEncoder
import atexit
from subprocess import Popen, PIPE
import os.path
import numpy as np
import pandas as pd
import aligndlib
import openface
import face_recognition
from imutils import paths

import AppEnv

from PickBestPhoto import pickBestPhotoIn, getPos



logger = logging.getLogger(__name__)

start = time.time()
np.set_printoptions(precision=2)

systemDir = AppEnv.systemDir
luaDir = AppEnv.luaDir
modelDir = AppEnv.modelDir
dlibModelDir = AppEnv.dlibModelDir
openfaceModelDir = AppEnv.openfaceModelDir


encodingPath = os.path.join(AppEnv.generatedEmbeddingsDir, "encodings.pkl")


class FaceRecogniserBestPhotoSimilarity(object):
    """ This recogniser choose a best photo from a person, 
    and encode the face for comparison during runtime """

    def __init__(self):
        self.args = args = AppEnv.getArgs()


        logger.info("Opening classifier.pkl to load existing known faces db")
        self.reloadClassifier()


    def reloadClassifier(self):
        logger.info("reloadClassifier called")
        if os.path.isfile(encodingPath):
            with open(encodingPath, 'rb') as f:
                (self.knownPeople, self.knownEncodingSet, self.knownPosition) = pickle.load(f, encoding='latin1') # Loads labels and classifier SVM or GMM
        return True


    def make_prediction(self, rgbFrame, face_locations):

        # logger.debug(f"make_prediction, bb = {bb}")

        # (bbs, scores, idx) = face_locations

        testFaceEncodings = face_recognition.face_encodings(rgbFrame, face_locations)

        # No face found
        if testFaceEncodings is None or len(testFaceEncodings) == 0:
            return None, None

        # TODO more than 1 face?
        predictions = []
        for k, face in enumerate(testFaceEncodings):

            if face is None:
                continue

            # one face
            faceDistances = face_recognition.face_distance(self.knownEncodingSet, face)

            min_distance = 99
            min_i = -1
            for i, face_distance in enumerate(faceDistances):
                # print("The test image has a distance of {:.2} from known image #{}".format(face_distance, i))
                if face_distance < min_distance:
                    min_distance = face_distance
                    min_i = i

            detectedLabel = self.knownPeople[min_i]
            confidence = int(math.ceil((1 - min_distance)*100))

            # persondict = {'name': detectedLabel, 'confidence': confidence, 'rep': testFaceEncodings[0]}
            logger.debug("/////  FACE RECOGNIZED  /// ")
            print ("Recognized P: " + str(detectedLabel) + " C: " + str(confidence))

            bb = face_locations[k]

            predictions.append( ( detectedLabel, confidence, bb, testFaceEncodings[0] ) )

        return predictions

        # if min_distance < 0.6:
        #     detectedLabel = self.knownPeople[min_i]
        #     confidence = (1 - min_distance)*100

        #     persondict = {'name': detectedLabel, 'confidence': confidence, 'rep': testFaceEncodings[0]}
        #     return persondict, rgbFrame

        # else:
        #     detectedLabel = self.knownPeople[min_i]
        #     confidence = (1 - min_distance)*100
        #     persondict = {'name': "unknown", 'confidence': 0, 'rep': testFaceEncodings[0]}
        #     return persondict, rgbFrame





    def trainClassifier(self):

        people = os.listdir(AppEnv.trainingImagesDir)
        people = list(filter(lambda item : item != ".DS_Store", people))


        knownPeople = []
        knownEncodingSet = []
        knownPosition = []
        postions = ["Left", "Right", "Center"]

        for position in postions:
            for person in people:

                logger.info("Finding encoding of {}".format(person))
                
                personDir = os.path.join(AppEnv.trainingImagesDir, person)
                (imagePath, avgDistance, encoding) = pickBestPhotoIn(personDir, position)


                logger.info(f"Best Photo of {person} is {imagePath} with avg distance of {avgDistance}")

                if encoding is None:
                    # Skip the person if no photo
                    continue
                
                knownPeople.append(person)
                knownEncodingSet.append(encoding)
                knownPosition.append(position)

        self.knownPeople = knownPeople
        self.knownEncodingSet = knownEncodingSet
        self.knownPosition = knownPosition

        fName = encodingPath
        logger.info("Saving classifier to '{}'".format(fName))
        with open(fName, 'wb') as f:
            pickle.dump((self.knownPeople,  self.knownEncodingSet, self.knownPosition), f) # Creates character stream and writes to file to use for recognition

        return (knownPeople, knownEncodingSet)
