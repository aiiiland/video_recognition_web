# FaceDetector.
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import numpy as np
import os
import glob
import dlib
import sys
import argparse
from PIL import Image
import math
import datetime
import threading
import logging
import ImageUtils


try:
    import face_recognition_models
except Exception:
    print("Please install `face_recognition_models` with this command before using `face_recognition`:\n")
    print("pip install git+https://github.com/ageitgey/face_recognition_models")
    quit()

cnn_face_detection_model = face_recognition_models.cnn_face_detector_model_location()
cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detection_model)


front_face_detector = dlib.get_frontal_face_detector()


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


SCORE_THRESHOLD = 0.1

class CNNFaceDetector(object):
    """This class implements Dlib's CNN based face detector"""

    def __init__(self):
        self.detector = cnn_face_detector


    def detect_faces(self, image, dummy):
        #  logger.info("dlibDetector = {}".format(dlibDetector))
        dets = self.detector(image, 1)

        # return dets.rect

        bbs = []
        for i, d in enumerate(dets):
            bbs.append(d.rect)

        return bbs


    def is_front_face(self, image):
        dets, scores, idx = front_face_detector.run(image, 1, SCORE_THRESHOLD)

        return len(dets) > 0