import multiprocessing
from multiprocessing import Process, Queue, Pipe, Event, Lock, Manager

import time
import numpy as np
import cv2
import ImageUtils
import dlib
import openface
import os
import logging
import SurveillanceSystem

logger = logging.getLogger(__name__)



singleton = None



class PeopleRuntimeDB(object):
    """ An In-memory DB for storing people detected by cameras """

    def __init__(self):
        global singleton
        if singleton is not None:
            print("Do not create more than one instance of PeopleRuntimeDB")
            logger.error("Do not create more than one instance of PeopleRuntimeDB")
            exit()

        self.manager = SurveillanceSystem.getInstance().getManager()
        self.people = self.manager.dict()





def getInstance():
    """ Obtain a singleton instance of  """
    global singleton

    if singleton is None:
        singleton = PeopleRuntimeDB()

    return singleton