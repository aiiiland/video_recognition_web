# FaceDetector.
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import numpy as np
import os
import glob
import dlib
from mtcnn_detect import MTCNNDetect
from tf_graph import FaceRecGraph
import sys
import argparse
from PIL import Image
import math
import datetime
import threading
import logging
import ImageUtils

import AppEnv


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

SCORE_THRESHOLD = 0.1


class MTCNNFaceDetector(object):
    """This class implements Dlib's CNN based face detector"""

    def __init__(self):
        FRGraph = FaceRecGraph()
        mtcnn_face_detector = MTCNNDetect(FRGraph, model_path = AppEnv.mtcnnModelDir ,scale_factor=1); #scale_factor, rescales image for faster detection
        self.front_face_detector = dlib.get_frontal_face_detector()
        self.detector = mtcnn_face_detector
        self.landmarks = None


    def detect_faces(self, image, dummy):
        #  logger.info("dlibDetector = {}".format(dlibDetector))
        rects, landmarks = self.detector.detect_face(image,40);#min face size is set to 40x40
        # logger.info("MTCNN detects faces = {}".format(len(rects)))
        self.landmarks = landmarks

        #change CV rect to dib rect
        lit=[]
        try:
            for (x, y, w, h) in rects:
                lit.append(dlib.rectangle(int(x),int(y),int(x+w),int(y+h)))
            return lit, landmarks

        except Exception as e:
            print("Warning: {}".format(e))
            # In rare cases, exceptions are thrown.
            return [], landmarks

    def getPos(self, points):
        if abs(points[0] - points[2]) / abs(points[1] - points[2]) > 2:
            return "Right"
        elif abs(points[1] - points[2]) / abs(points[0] - points[2]) > 2:
            return "Left"
        return "Center"

    def is_front_face(self, image):        
        dets, scores, idx = self.front_face_detector.run(image, 1, SCORE_THRESHOLD)

        return len(dets) > 0