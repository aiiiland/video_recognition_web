import os
import sys

sys.path.append("../database")
sys.path.append("./modules")
sys.path.append("./processMethods")


import json
import logging
from logging.handlers import RotatingFileHandler
import threading
import time
from random import random
import cv2
import psutil
import dlib

# import Camera
import SurveillanceSystem
import FaceDetector
# from FaceDetector import DLIB_DETECTOR, OPENCV_DETECTOR
import ImageUtils
import FaceRecogniser

import AppEnv
# import RootDB
# import PeopleModule
# import CameraModule
# import AuthModule
# import SysMonitorModule
# import AlertModule
# import SettingsModule
# import CameraDAO
# from MyUtils import MyEncoder

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


AppEnv.useConsole()

def retrain_classifier():
    logger.info("Training started")
    recogniser = FaceRecogniser.FaceRecogniser()
    recogniser.trainClassifier()#calling the module in FaceRecogniser to start training
    logger.info("Training finished")
    # logger.info("Labels = {}".format(recogniser.labels))


# def command_detect_image(args, faceRecogniser):
#     imagePath = args.detectImage
#     frame = cv2.imread(imagePath)
#     # _, jpeg = cv2.imencode('.jpg', img)
#     # self.NOFRAME_IMAGE = jpeg.tostring()
#     predict_image(frame)


def predict_image(frame, faceDetector, faceRecogniser):
    if frame is None:
        print("Frame is none")
        exit()

    # detect
    faceBoxes = faceDetector.detect_faces(frame, FaceDetector.DLIB_DETECTOR)
    # draw
    # frame = ImageUtils.draw_boxes(frame, faceBoxes, FaceDetector.DLIB_DETECTOR)

    predictions = []
    for face_bb in faceBoxes:

        # returns a dictionary that contains name, confidence and representation and an alignedFace (numpy array)
        prediction, alignedFace = faceRecogniser.make_prediction(frame, face_bb)

        # logger.info("Predictions = {}".format(prediction))

        predictions.append(prediction)

    return predictions




def run_test(testingDatasetDir):
    """ Run Classification test on the Training data """

    # prepare
    faceDetector = FaceDetector.FaceDetector()
    faceRecogniser = FaceRecogniser.FaceRecogniser()

    # Test Result
    testResult = dict()

    # Test
    people = os.listdir(testingDatasetDir)

    for person in people:
        if person == ".DS_Store":
            continue
        # print(person)
        personDir = os.path.join(testingDatasetDir, person)
        personImages = os.listdir(personDir)

        resultDict = dict()

        for imgFname in personImages:
            if imgFname == ".DS_Store":
                continue

            print("Testing on {} ...".format(imgFname))
            imgPath = os.path.join(personDir, imgFname)
            frame = cv2.imread(imgPath)

            predictions = predict_image(frame, faceDetector, faceRecogniser)

            success = False
            if predictions is not None:
                if len(predictions) > 0:
                    predict = predictions[0]
                    if predict['name'] == person:
                        success = True

            resultDict[imgFname] = success

        testResult[person] = resultDict

    return testResult


def gen_result_report(testName, testResult):

    totalPhotos = 0
    overallSuccess = 0

    print("-----------------")
    print(testName)
    print("-----------------")
    print("{},{},{}".format("Label", "Correct", "Total"))
    for person in testResult.keys():

        personResult = testResult[person]
        total = len(personResult.keys())
        numSuccess = 0
        for img in personResult.keys():
            if personResult[img] == True:
                numSuccess = numSuccess + 1
        
        totalPhotos = totalPhotos + total
        overallSuccess = overallSuccess + numSuccess

        # percent = (numSuccess / total) * 100
        print("{},{},{}".format(person, numSuccess, total))

    print("-----------------")
    print("Total accuracy : {}%".format( (overallSuccess/totalPhotos)*100 ))





def run_test_detailed(testingDatasetDir):
    """ Run Classification test on a set of data, with details results """

    # prepare
    faceDetector = FaceDetector.FaceDetector()
    faceRecogniser = FaceRecogniser.FaceRecogniser()

    # Test Result
    testResult = dict()

    # Test
    testDataset = os.listdir(testingDatasetDir)

    for testPersonLabel in testDataset:
        if testPersonLabel == ".DS_Store":
            continue
        # print(testPersonLabel)
        personDir = os.path.join(testingDatasetDir, testPersonLabel)
        personImages = os.listdir(personDir)

        resultDict = dict()

        for imgFname in personImages:
            if imgFname == ".DS_Store":
                continue

            print("Testing on {} ...".format(imgFname))
            imgPath = os.path.join(personDir, imgFname)
            frame = cv2.imread(imgPath)

            predictions = predict_image(frame, faceDetector, faceRecogniser)

            result = None
            if predictions is not None and len(predictions) > 0:
                predict = predictions[0]
                result = (testPersonLabel, imgFname, predict['name'], predict['confidence'])
            else:
                result = (testPersonLabel, imgFname, None, None)

            resultDict[imgFname] = result

        testResult[testPersonLabel] = resultDict

    return testResult


def gen_result_report_detailed(testName, testResult):

    totalPhotos = 0
    overallSuccess = 0
    overallFalsePositive = 0

    testResultPath = os.path.join(AppEnv.storageDir, "test-results")
    outputPath = os.path.join(testResultPath, "{}.csv".format(testName))

    with open(outputPath, "w") as textFileWriter:

        print("-----------------")
        print(testName)
        print("-----------------")
        print("{},{},{},{}".format("Label", "Image", "PredictLabel", "Confidence"))
        textFileWriter.write("{},{},{},{}\n".format("Label", "Image", "PredictLabel", "Confidence"))
        for person in testResult.keys():

            personResult = testResult[person]

            total = len(personResult.keys())
            
            numSuccess = 0
            numFalsePositive = 0

            for img in personResult.keys():
                (testPersonLabel, imageName, predictLabel, predictConfidence) = personResult[img]

                if testPersonLabel == predictLabel:
                    numSuccess = numSuccess + 1

                if predictLabel is not None and testPersonLabel != predictLabel and predictConfidence >= 50:
                    numFalsePositive = numFalsePositive + 1
                
                print("{},{},{},{}".format(testPersonLabel, imageName, predictLabel, predictConfidence))
                textFileWriter.write("{},{},{},{}\n".format(testPersonLabel, imageName, predictLabel, predictConfidence))
            
            totalPhotos = totalPhotos + total
            overallSuccess = overallSuccess + numSuccess
            overallFalsePositive = overallFalsePositive + numFalsePositive

            # percent = (numSuccess / total) * 100
            # print("{},{},{}".format(person, numSuccess, total))

        print("-----------------")
        print("Total accuracy : {}%".format( (overallSuccess/totalPhotos)*100 ))
        print("Total false positive (50% confidence) : {}%".format( (overallFalsePositive/totalPhotos)*100 ))


def main():
    print("")
    print("##############################################################")
    print(" This is the console interface of the AI Surveillence System ")
    print("#############################################################")
    print("")

    logger.setLevel(logging.DEBUG)


    args = AppEnv.getArgs()

    if args.cuda:
        print("CUDA: True")


    # if args.detectImage is not None:
    #     print("You want to detect an image : {}".format(args.detectImage) )
    #     HomeSurveillance = SurveillanceSystem.SurveillanceSystem()
    #     command_detect_image(args, HomeSurveillance.recogniser)

    # HomeSurveillance = SurveillanceSystem.SurveillanceSystem()
    retrain_classifier()

    # testResult = run_test(AppEnv.trainingImagesDir)
    # gen_result_report("TrainingDataTest", testResult)

    # testResult = run_test_detailed(os.path.join(AppEnv.storageDir, "training-images"))
    # gen_result_report_detailed("TrainingDataTest", testResult)

    testResult = run_test_detailed(os.path.join(AppEnv.storageDir, "test-images"))
    gen_result_report_detailed("UnknownDataTest", testResult)


    


    

if __name__ == "__main__":
    main()

