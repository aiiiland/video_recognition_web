# FaceDetector.
# Brandon Joffe
# 2016
#
# Copyright 2016, Brandon Joffe, All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import numpy as np
import os
import glob
import dlib
import mxnet as mx
from mtcnn_detector import MtcnnDetector
import sys
import argparse
from PIL import Image
import math
import datetime
import threading
import logging
import ImageUtils

import AppEnv



logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

SCORE_THRESHOLD = 0.1


class MTCNN2FaceDetector(object):
    """This class implements Dlib's CNN based face detector"""

    def __init__(self):
        mtcnn2_face_detector = MtcnnDetector(model_folder=AppEnv.mtcnn2ModelDir, ctx=mx.cpu(0), num_worker = 4 , accurate_landmark = False)
        self.front_face_detector = dlib.get_frontal_face_detector()
        self.detector = mtcnn2_face_detector
        self.landmarks = None


    def detect_faces(self, image, dummy):
        #  logger.info("dlibDetector = {}".format(dlibDetector))
        results = self.detector.detect_face(image)
        if results is None:
            return [], None

        total_boxes = results[0]
        points = results[1]
        # logger.info("MTCNN detects faces = {}".format(len(total_boxes)))
        self.landmarks = points

        #change CV rect to dib rect
        lit=[]
        try:
            for b in total_boxes:
                lit.append(dlib.rectangle(int(b[0]),int(b[1]),int(b[2]),int(b[3])))
            return lit, self.landmarks

        except Exception as e:
            print("Warning: {}".format(e))
            # In rare cases, exceptions are thrown.
            return [], self.landmarks

    def getPos(self, points):
        if abs(points[0] - points[2]) / abs(points[1] - points[2]) > 2:
            return "Right"
        elif abs(points[1] - points[2]) / abs(points[0] - points[2]) > 2:
            return "Left"
        return "Center"

    def is_front_face(self, image):
        dets, scores, idx = self.front_face_detector.run(image, 1, SCORE_THRESHOLD)

        return len(dets) > 0