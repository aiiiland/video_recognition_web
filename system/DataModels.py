import cv2
import time
from datetime import datetime, timedelta
import collections

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
class Person(object):
    """Person object simply holds all the
    person's information for other processes
    """
    person_count = 0

    def __init__(self, rep, predictLabel, predictConfidence, face = None, identity = None, uniqueKey = None):

        if identity is None:
            self.identity = str(predictLabel)
        else:
            self.identity = str(identity)

        if uniqueKey is None:
            self.uniqueKey = str(predictLabel)
        else:
            self.uniqueKey = str(uniqueKey)

        # if "unknown" not in str(identity): # Used to include unknown-N from Database
        #     self.identity = str(identity)
        # else:
        #     self.identity = "unknown"
        # self.identity = str(identity)
       
        self.count = Person.person_count

        self.predictLabel = str(predictLabel)
        self.confidence = predictConfidence  
        self.thumbnails = collections.deque(maxlen=5)
        self.face = face
        self.rep = rep # Face representation
        if face is not None:
            ret, jpeg = cv2.imencode('.jpg', face) # Convert to jpg to be viewed by client
            self.thumbnail = jpeg.tostring()
        self.thumbnails.append(self.thumbnail) 
        Person.person_count += 1 
        # now = datetime.now() + timedelta(hours=2)
        # self.time = now.strftime("%A %d %B %Y %I:%M:%S%p")
        self.time = time.time()
        self.istracked = False
   
    def set_rep(self, rep):
        self.rep = rep

    def set_identity(self, identity):
        self.identity = identity

    def set_time(self): # Update time when person was detected
        # now = datetime.now() + timedelta(hours=2)
        # self.time = now.strftime("%A %d %B %Y %I:%M:%S%p")
        self.time = time.time()

    def set_thumbnail(self, face):
        self.face = face
        _, jpeg = cv2.imencode('.jpg', face) # Convert to jpg to be viewed by client
        self.thumbnail = jpeg.tostring()

    def add_to_thumbnails(self, face):
        _, jpeg = cv2.imencode('.jpg', face) # Convert to jpg to be viewed by client
        self.thumbnails.append(jpeg.tostring())

    def toApiDict(self):
        bean = {
            "uniqueKey": self.uniqueKey,
            "predictLabel": self.predictLabel,
            "confidence": self.confidence,
            "identity": self.identity,
            "time": self.time,
            # "thumbnail": self.thumbnail
        }
        return bean

    def toDict(self):
        bean = {
            "uniqueKey": self.uniqueKey,
            "predictLabel": self.predictLabel,
            "confidence": self.confidence,
            "identity": self.identity,
            "time": self.time,
            "thumbnail": self.thumbnail
        }
        return bean


class Tracker:
    """Keeps track of person position"""

    tracker_count = 0

    def __init__(self, img, bb, person, id):
        self.id = id 
        self.person = person
        self.bb = bb
        self.pings = 0
        self.facepings = 0

    def reset_pinger(self):
        self.pings = 0

    def reset_face_pinger(self):
        self.facepings = 0

    def update_tracker(self,bb):
        self.bb  = bb 
        
    def overlap(self, bb):
        p = float(self.bb.intersect(bb).area()) / float(self.bb.area())
        return p > 0.2

    def ping(self):
        self.pings += 1

    def faceping(self):
        self.facepings += 1

class Alert(object): 
    """Holds all the alert details and is continually checked by 
    the alert monitoring thread"""

    alert_count = 1

    def __init__(self,alarmState,camera, event, person, actions, emailAddress, confidence):   
        # logger.info( "alert_"+str(Alert.alert_count)+ " created")
       

        if  event == 'Motion':
            self.alertString = "Motion detected in camera {}".format(camera)
        else:
            if camera == "All":
                self.alertString = "{} was recognised in camera with a confidence greater than {}".format(person, str(confidence))
            else:
                self.alertString = "{} was recognised in camera {} with a confidence greater than {}".format(person, camera, str(confidence))

        self.id = "alert_" + str(Alert.alert_count)
        self.event_occurred = False
        self.action_taken = False
        self.camera = camera
        self.alarmState = alarmState
        self.event = event
        self.person = person
        self.confidence = confidence
        self.actions = actions
        if emailAddress == None:
            self.emailAddress = "max.liu@aiiiland.com"
        else:
            self.emailAddress = emailAddress

        self.eventTime = 0

        Alert.alert_count += 1

    def reinitialise(self):
        self.event_occurred = False
        self.action_taken = False

    def set_custom_alertmessage(self,message):
        self.alertString = message

