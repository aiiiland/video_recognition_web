# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import numpy as np
import os
import glob
import dlib
import sys
import argparse
from PIL import Image
import math
import datetime
import threading
import logging
import ImageUtils

import AppEnv

front_face_detector = dlib.get_frontal_face_detector()


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


SCORE_THRESHOLD = 0.1

class DNNFaceDetector(object):
    """This class implements Dlib's CNN based face detector"""

    def __init__(self):
        # load our serialized model from disk
        logger.info("[INFO] loading model...")
        self.net = cv2.dnn.readNetFromCaffe(AppEnv.dnnPrototxtPath, AppEnv.dnnModelPath)
        # self.ct = CentroidTracker()


    def detect_faces(self, image, dummy):

        (H, W) = image.shape[:2]

        # construct a blob from the frame, pass it through the network,
        # obtain our output predictions, and initialize the list of
        # bounding box rectangles
        blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
            (104.0, 177.0, 123.0))
        self.net.setInput(blob)
        detections = self.net.forward()
        rects = []

        # loop over the detections
        for i in range(0, detections.shape[2]):
            # filter out weak detections by ensuring the predicted
            # probability is greater than a minimum threshold
            if detections[0, 0, i, 2] > 0.5:
                # compute the (x, y)-coordinates of the bounding box for
                # the object, then update the bounding box rectangles list
                box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])

                # reform box
                box1 = dlib.rectangle(int(box[0]),int(box[1]),int(box[2]),int(box[3]))
                rects.append(box1)

        #logger.info("DNN detects faces = {}".format(len(rects)))

        return rects


    def is_front_face(self, image):
        dets, scores, idx = front_face_detector.run(image, 1, SCORE_THRESHOLD)

        return len(dets) > 0