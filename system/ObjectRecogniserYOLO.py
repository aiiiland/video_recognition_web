# objectRecogniser.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# import the necessary packages
import cv2
import os
import darknet as dn
import CreateAnnotation
from CreateAnnotation import FORMAT_PASCALVOC, FORMAT_YOLO
import logging
import time
from subprocess import Popen, PIPE
import atexit
from shutil import copyfile
import argparse

import AppEnv

logger = logging.getLogger(__name__)

start = time.time()

class ObjectRecogniserYOLO(object):

    def __init__(self):

        logger.info("Opening dataset to load existing known objects db")
        self.args = args = AppEnv.getArgs()
        # load the known objects and embeddings
        if args.cuda:
            libfilepath = AppEnv.darknet_GPULibPath
        else:
            libfilepath = AppEnv.darknetLibPath
        cfgfilepath = AppEnv.darknetCfgPath
        datafilepath = AppEnv.darknetDataPath
        weightsfilepath = AppEnv.darknetWeightPath

        self.darknet = dn.Darknet(libfilepath=libfilepath,
                      cfgfilepath=cfgfilepath.encode(),
                      weightsfilepath=weightsfilepath.encode(),
                      datafilepath=datafilepath.encode())

        self.darknet.load_conf()

    def reloadClassifier(self):
        logger.info("reloadClassifier called")
        self.darknet.load_conf()
        return True

    def make_prediction(self,bgrImg):

        logger.info("YOLO prediction")
        if bgrImg is None:
            raise Exception("Unable to load image/frame")

        success, imagefile = cv2.imencode(".jpg", bgrImg)
        start = time.time()

        if not success:
            logger.info("image file has problems")
            return None

        # Get all bounding boxes
        res = self.darknet.detectImage(imagefile)
        logger.info("Objects detection took {} seconds.".format(time.time() - start))
        logger.info(res)

        if res is None:
            logger.info("///  OBJECT COULD NOT BE DETECT  ///")
            return None

        return res #[name, confidence, box]  

    def trainClassifier(self):

        logger.info("Yolo not support training yet")
        return

        logger.info("trainClassifier called")
        start = time.time()

        logger.info("annotation start")

        annotation = CreateAnnotation.CreateAnnotation(defaultLoadDir=AppEnv.alignedImagesDir, defaultSaveDir="yolo-aligned-txt", defaultFormat=FORMAT_YOLO)
        totalClasses = annotation.startAutoAnnotation()

        if not os.path.exists(AppEnv.darknetAlignedTxtDir):
            try:
                logger.info( "Creating New Face Dircectory: " + AppEnv.darknetAlignedTxtDir)
                os.makedirs(AppEnv.darknetAlignedTxtDir)
            except OSError:
                logger.info( OSError)
                return False
                pass

        #copy the classes file
        voc_names_filepath = AppEnv.darknetClassNamesPath
        voc_names_source = AppEnv.darknetAlignedClassesPath
        try:
            os.remove(voc_names_filepath) # Remove cache from aligned images folder
        except:
            logger.info("Failed to remove voc.names. Could be that it did not existed in the first place.")
            pass
        
        logger.info("copy classes file")
        copyfile(voc_names_source, voc_names_filepath)

        logger.info("prepare voc.data file")
        voc_data_filepath = AppEnv.darknetDataPath
        voc_data_file = open(voc_data_filepath, "w")
        voc_data_file.write("classes = " + str(totalClasses) + "\n")
        voc_data_file.write("train = " + AppEnv.darknetAlignedImageListTrainPath + "\n")
        voc_data_file.write("valid = " + AppEnv.darknetAlignedImageListValidPath + "\n")
        voc_data_file.write("names = " + AppEnv.darknetClassNamesPath + "\n")
        voc_data_file.write("backup = " + AppEnv.darknetBackupDir + "\n")
        voc_data_file.close()

        #update the cfg file with the classes number
        new_classes = "classes=" + str(totalClasses)
        f1 = open(darknetCfgPath, "r")
        f2 = open(darknetCfg1Path, "w")
        for line in f1:
            f2.write(line.replace("classes=20", new_classes))
        f1.close()
        f2.close()        

        logger.info("start training")
        self.cmd = [darknetDir, "detector", "train", darknetDataPath, darknetCfgPath, darknetTrainWeightPath]
        #self.cmd = ["../darknet/darknet", "detector", "train", "../darknet/cfg/voc.data", "../darknet/cfg/yolov3-voc1.cfg"]
        print("run command:    " + str(self.cmd))
        self.p = Popen(self.cmd, stdin=PIPE, stdout=PIPE, bufsize=0)
        outs, errs = self.p.communicate() # Wait for process to exit - wait for subprocess to finish
        logger.info("Waiting for process to exit to finish" + str(outs) + " - " + str(errs))

        def exitHandler():
            if self.p.poll() is None:
                logger.info("<=Something went Wrong===>")
                self.p.kill()
                return False
        atexit.register(exitHandler)
        logger.info("Training took {} seconds.".format(time.time() - start))
        logger.info("YOLO training finished")

def main():
    """
    main
    """
    ##test
    print("train classifier")    
    objectRecogniser = ObjectRecogniserYOLO()
    objectRecogniser.trainClassifier()#calling the module in objectRecogniser to start training


if __name__ == "__main__":
    main()
