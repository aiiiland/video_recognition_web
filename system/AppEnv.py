import os
import argparse


# Dirs
systemDir = os.path.dirname(os.path.realpath(__file__))
projectRootDir = os.path.normpath(os.path.join(systemDir, '..'))

# storageDir = systemDir
storageDir = os.path.join(projectRootDir, 'storage')

luaDir = os.path.join(projectRootDir, 'batch-represent')
modelDir = os.path.join(projectRootDir, 'models')
dlibModelDir = os.path.normpath(os.path.join(modelDir, 'dlib'))
openfaceModelDir = os.path.normpath(os.path.join(modelDir, 'openface'))
kerasopenfaceModelDir = os.path.normpath(os.path.join(modelDir, 'keras-openface'))
mtcnnModelDir = os.path.normpath(os.path.join(modelDir, 'mtcnn'))
mtcnn2ModelDir = os.path.normpath(os.path.join(modelDir, 'mtcnn2'))
outputDir = os.path.normpath(os.path.join(modelDir, 'output'))
# DNN path
dnnModelDir = os.path.normpath(os.path.join(modelDir, 'dnn'))
dnnModelPath = os.path.normpath(os.path.join(dnnModelDir , 'res10_300x300_ssd_iter_140000.caffemodel'))
dnnPrototxtPath = os.path.normpath(os.path.join(dnnModelDir , 'deploy.prototxt'))


exampleTrainingImagesDir = os.path.join(systemDir, 'example-training-images')

# Generated data paths
trainingImagesDir = os.path.join(storageDir, 'training-images')
alignedImagesDir = os.path.join(storageDir, 'aligned-images')
generatedEmbeddingsDir = os.path.join(storageDir, 'generated-embeddings')
uploadsDir = os.path.join(storageDir, 'uploads')
capturedImagesDir = os.path.join(storageDir, "captured-images")
notificationDir = os.path.join(storageDir, "notification")
logImagesDir = os.path.join(storageDir, "log-images")


labelsCsvPath = os.path.join(generatedEmbeddingsDir, "labels.csv")
repsCsvPath = os.path.join(generatedEmbeddingsDir, "reps.csv")
classifierPath = os.path.join(generatedEmbeddingsDir, "classifier.pkl")
classifierEmbeddingPath = os.path.join(generatedEmbeddingsDir, "classifierEmbedding.pkl")
classifierKerasPath = os.path.join(generatedEmbeddingsDir, "classifierKeras.pkl")

# darknet paths
darknetDir = os.path.join(projectRootDir, "darknet")
darknetLibPath = os.path.join(darknetDir, "libdarknet.so")
darknet_GPULibPath = os.path.join(darknetDir, "libdarknet_gpu.so")
darknetCfgDir = os.path.join(darknetDir, "cfg")
#darknetCfgPath = os.path.join(darknetCfgDir, "yolov3-voc.cfg")
darknetCfgPath = os.path.join(darknetCfgDir, "yolov3-tiny.cfg")
darknetCfg1Path = os.path.join(darknetCfgDir, "yolov3-voc1.cfg")
#darknetDataPath = os.path.join(darknetCfgDir, "voc.data")
darknetDataPath = os.path.join(darknetCfgDir, "coco.data")
darknetBackupDir = os.path.join(darknetDir, "backup")
#darknetWeightPath = os.path.join(darknetBackupDir, "yolov3-voc_900.weights")
darknetWeightPath = os.path.join(darknetBackupDir, "yolov3-tiny.weights")

darknetClassNamesDir = os.path.join(darknetDir, "data")
#darknetClassNamesPath = os.path.join(darknetClassNamesDir, "voc.names")
darknetClassNamesPath = os.path.join(darknetClassNamesDir, "coco.names")
darknetAlignedTxtDir = os.path.join(storageDir, 'yolo-aligned-txt')
darknetAlignedClassesPath = os.path.join(darknetAlignedTxtDir, 'classes.txt')
darknetAlignedImageListTrainPath = os.path.join(darknetAlignedTxtDir, 'imageslist.txt')
darknetAlignedImageListValidPath = os.path.join(darknetAlignedTxtDir, 'imageslist_valid.txt')

darknetTrainWeightPath = os.path.join(darknetDir, "darknet53.conv.74")

def pathJoin(path1, path2):
    return os.path.join(path1, path2)


def createGeneratedDirIfNotFound():
    dirs = [storageDir, trainingImagesDir, alignedImagesDir, generatedEmbeddingsDir, uploadsDir, capturedImagesDir, notificationDir, logImagesDir]

    for dir in dirs:
        if not os.path.exists(dir):
            os.makedirs(dir)

def printOutPaths():
    """ Print out the paths for debugging purpose """
    print("systemDir = " + systemDir)
    print("projectRootDir = " + projectRootDir)
    print("luaDir = " + luaDir)
    print("modelDir = " + modelDir)
    print("dlibModelDir = " + dlibModelDir)
    print("openfaceModelDir = " + openfaceModelDir)


# Args
args = None # default value


def getArgs():
    """ Get the arguments passed when starting this Python app """
    if args is None:
        useWeb() # assume use web

    return args

def useWeb():
    """ Used when starting WebApp """
    global args
    parser = argparse.ArgumentParser()

    parser.add_argument('--confidenceThreshold', type=int, help="confidenceThreshold", default=53)                    

    # select the detector
    parser.add_argument("--detector", type=str, default="DNN",help="recogniser model to use: either `dlib` or `CNN` or `MTCNN` or `MTCNN2` or `DNN`")
    # select the recogniser
    parser.add_argument("--recogniser", type=str, default="face_recognition_compare",help="recogniser model to use: either `openface` or `face_recognition` or `keras` or `face_recognition_compare`")
    # select the object recogniser
    parser.add_argument("--objectRecogniser", type=str, default="object_recognition_yolo",help="object recogniser model to use: either `object_recognition_yolo`")

    parser.add_argument('--dlibFacePredictor', 
                        type=str, help="Path to dlib's face predictor.", 
                        default=os.path.join(dlibModelDir , "shape_predictor_68_face_landmarks.dat"))                  
    parser.add_argument('--networkModel', 
                    type=str, help="Path to Torch network model.", 
                    default=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'))                   
    parser.add_argument('--imgDim', type=int, help="Default image dimension.", default=96)                    
    parser.add_argument('--cuda', action='store_true')
    parser.add_argument('--unknown', type=bool, default=False, help='Try to predict unknown people')                  

    #for embedding cnn algor
    parser.add_argument("--detection_method", type=str, default="cnn",help="face detection model to use: either `hog` or `cnn`")

    #for tensorflow keras
    parser.add_argument('--networkModelTF', 
                    type=str, help="Path to TensorFlow network model.", 
                    default=os.path.join(kerasopenfaceModelDir, 'nn4.small2.v1.h5'))                   

    args = parser.parse_args()
    return args


def useConsole():
    """ Used when starting ConsoleApp """
    global args

    parser = argparse.ArgumentParser()

    parser.add_argument('--confidenceThreshold', type=int, help="confidenceThreshold", default=53)                    

    # select the detector
    parser.add_argument("--detector", type=str, default="DNN",help="recogniser model to use: either `dlib` or `CNN` or `MTCNN` or `MTCNN2` or `DNN`")
    # select the recogniser
    parser.add_argument("--recogniser", type=str, default="face_recognition_compare",help="recogniser model to use: either `openface` or `face_recognition` or `keras` or `face_recognition_compare`")
    # select the object recogniser
    parser.add_argument("--objectRecogniser", type=str, default="object_recognition_yolo",help="object recogniser model to use: either `object_recognition_yolo`")

    # Main features
    parser.add_argument('--detectImage', type=str, help="Detect an image by path")

    # include for compatibility
    parser.add_argument('--dlibFacePredictor', 
                        type=str, help="Path to dlib's face predictor.", 
                        default=os.path.join(dlibModelDir , "shape_predictor_68_face_landmarks.dat"))                  
    parser.add_argument('--networkModel', 
                    type=str, help="Path to Torch network model.", 
                    default=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'))                   
    parser.add_argument('--imgDim', type=int, help="Default image dimension.", default=96)                    
    parser.add_argument('--cuda', action='store_true')
    parser.add_argument('--unknown', type=bool, default=False, help='Try to predict unknown people')                  

    #for embedding cnn algor
    parser.add_argument("--detection_method", type=str, default="cnn",help="face detection model to use: either `hog` or `cnn`")

    #for tensorflow keras
    parser.add_argument('--networkModelTF', 
                    type=str, help="Path to TensorFlow network model.", 
                    default=os.path.join(kerasopenfaceModelDir, 'nn4.small2.v1.h5'))                   

    args = parser.parse_args()
    return args
