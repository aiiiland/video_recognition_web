FROM aiiiland/video_recognition_web:base
# FROM aiiiland-cuda-base

# add files
ADD ./requirements.txt /host/requirements.txt
COPY docker-entrypoint.sh /

# App dependecies
RUN pip3 install -r /host/requirements.txt

# Install opencv
RUN pip3 install opencv-python-headless opencv-contrib-python-headless

# Install SQLite and Pyrika
RUN pip3 install pypika
RUN pip3 install imutils
RUN pip3 install --no-dependencies face_recognition face_recognition_models
# For Websocket
# RUN pip3 install gevent-websocket
RUN pip3 install lxml

VOLUME ["/host/storage"]
EXPOSE 5000

WORKDIR /host/system
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["python3", "WebApp.py"]